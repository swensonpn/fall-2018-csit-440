# Class Workspace
This public workspace has been made available for you to use as a reference in 
recalling material discussed in class.  All content for this course can be found
here. 

## Course Content Organization
Code and documents are stored in separate folders.  This is done to reduce 
clutter in the code folders, and make notes easier to find.  Folder names for
the course modules are the same to make referencing notes to code should be 
simple.  This is how the material is organized:

**fall-2018-csit-440**

* **.c9** - Contains project configuration for the workspace.

* **.git** - Contains git change tracking information.

* **code** - Example code. Each sub-folder can be thought of as a stand alone project.

    * **1-n_section_name** - Lecture Code and Examples
    
* **docs** - All course notes and supporting materials

    * **0_syllabus** - Syllabus, Schedule
    
    * **1-n_section_name** - Lecture Notes, Activities, and Homework
    
* **.gitignore** - Tells GIT which files should not be tracked

* **README.md** - Entry point for lecture notes

## Table of Contents
1. [Syllabus](./docs/1_syllabus/README.md)
2. [Getting Started](./docs/2_getting_started/README.md)
3. [JavaScript](./docs/3_javascript/README.md)
4. [Object Oriented Programming](./docs/4_object_oriented/README.md)
5. [Progressive Web Applications](./docs/5_progressive/README.md)
6. [Serving Web Content](./docs/6_web_content/README.md)
7. [Project](./docs/7_project/README.md)