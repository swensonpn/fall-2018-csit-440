const path = require("path"),
      bodyParser = require('body-parser'),
      cardRoutes = require("./routes/cards"),
      mongoose = require("mongoose");

const express = require('express');
const app = express();
const port = process.env.PORT || 5000;//cloud9 port is env variable
const connectString = "mongodb://localhost:27017/test";

//connect to the database on server start
mongoose
  .connect(connectString, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch((err) => {console.log('Mongo Error:'); console.log(err);});

//disconnect from the database on server stop
process.on('SIGINT',function(){
  mongoose.connection.close(function(){
    console.log('MongoDB disconnected due to Application Exit');
    process.exit(0);
  });
});

// Parse json request body
app.use(bodyParser.json())

// Serve any static files from react
app.use(express.static(path.join(__dirname, 'client/build')));

// Assign api routes
app.get("/api", cardRoutes.index);
app.post("/api", cardRoutes.save);
app.delete("/api", cardRoutes.delete);

// Assign client app routes (everything else)
app.get('*', function(req, res) {
 res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});  

app.listen(port, () => console.log(`Example app listening on port ${port}!`));