const mongoose = require("mongoose"),
      Schema = mongoose.Schema;
 
//create a schema      
const cardSchema = new Schema({
  type: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  due: {
    type: Date,
    required: true
  },
  complete: {
    type: Date
  }
},
{
  collection: "cards"
});

//compile schema into a model
module.exports = mongoose.model('Card', cardSchema);