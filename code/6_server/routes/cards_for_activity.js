const cards = [//an array can stand in for the database for now
    //card objects
];



exports.index = function(request, response) {                                   
    console.log("index");
    response.send(JSON.stringify(cards));
};

exports.save = function(request, response) {                                    
    console.log("save")
    const id = request.body.id;
    
    let card = cards.find(function(card) {
        return card.id === id;
    })
    
    if(card) {
        card.type = request.body.type;
        card.description = request.body.description;
        card.due = request.body.due;
        if(card.type !== "complete" && request.body.type === "complete") {
            card.complete = new Date();
        } 
        response.writeHead(200);
        response.end(JSON.stringify(card));
    } else {
        card = {
          id:request.body.id,
          type: request.body.type,
          description: request.body.description,
          due: request.body.due
        };
        
        cards.push(card);
        response.writeHead(201);
        response.end(JSON.stringify(card));
    }
};    

exports.delete = function(request, response) {                                  
    console.log("delete");
    const id = request.body.id;
    
    if(id) {
        for(let i = 0; i < cards.length; i++) {
            if(cards[i].id === id) {
                let card = cards.splice(i,1)[0];
                response.writeHead(202);
                response.end(JSON.stringify(card));
                return;
            }
        }
    }
    
    response.writeHead(404);
    response.end("not found");
};