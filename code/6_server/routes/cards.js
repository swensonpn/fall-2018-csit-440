const cards = [//an array can stand in for the database for now
    //card objects
];

const CardModel = require("../models/card");


exports.index = function(request, response) {                                   
    console.log("index");
    CardModel
        .find({type: {$ne: 'complete'}})
        .exec(function(err, cards) {
             if (err) {
                 console.error(err);
                 response.writeHead(500);
                 response.end("error");
            } else {
                response.send(JSON.stringify(cards));
             }
         });
};

exports.save = function(request, response) {                                    
    console.log("save")
    const id = request.body._id;
    
    if(id) {
        //1. Get the card from the database
        CardModel.findById(id, function(err, card){
            if(err || card == null) {//don't pass an id till you have one
                console.error(err);
                response.writeHead(404);
                response.end("not found");
            } else {
                //2. Update properties from the request
                card.type = request.body.type;
                card.description = request.body.description;
                card.due = request.body.due;
                if(request.body.type === "complete") {
                    card.complete = new Date();
                }
                
                //3. Save the updated card
                card.save(function() {
                    response.writeHead(200);
                    response.end(JSON.stringify(card));
                });
            }
        });
    } else {
        CardModel.create({
          type: request.body.type,
          description: request.body.description,
          due: request.body.due
        }, function(err, card) {
            if(err) {
                console.error(err);
                response.writeHead(500);
                response.end("error");
            } else {
                response.writeHead(201);
                response.end(JSON.stringify(card));
            }
        });
    }
};    

exports.delete = function(request, response) {                                  
    console.log("delete");
    const id = request.body._id;
    
    CardModel.deleteOne({_id:id},function(err) {
        if(err) {
            console.error(err);
            response.writeHead(500);
            response.end("error");
        } else {
            response.writeHead(202);
            response.end(JSON.stringify({}));
        }
    });
};