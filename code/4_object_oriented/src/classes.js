class Player {
    
    constructor(firstName = "", lastName = "") {// constructor is optional
        this._firstName = firstName;
        this._lastName = lastName;
    }
    
    get firstName() {
        return this._firstName;
    }
    
    get lastName() {
        return this._lastName;
    }

    get name() {
        return this._firstName + " " + this._lastName;
    }
    
    set firstName(firstName = "") {
        this._firstName = firstName;
    }
    
    set lastName(lastName = "") {
        this._lastName = lastName;
    }
    
    set name(name = "") {
        const names = name.split(" ");
        this._firstName = names[0];
        this._lastName = (names[1] !== undefined) ? names[1] : "";
    }
    
    static speak(message) {
        console.log(message);
    }
}
const player1 = new Player("Jane", "Doe");
console.log(player1.name);
player1.name = "John Doe";
console.log(player1.name);

const player2 = new Player("Steve", "Smith");
Player.speak(`Hi my name is ${player2.name}`);


class Deck {
    shuffle() {}
}

class Dealer extends Player {
    constructor(firstName, lastName, deck) {
        super(firstName, lastName);//call the parent's constructor
        this._deck = deck;
    }
    
    get deck() {
        return this._deck;
    }
    
    deal(numCards = 0, numPlayers = 0) {
        let hands = [];
        
        this._deck.shuffle();
        for(let i = 0; i < numCards; i++) {
            if(! hands[i] instanceof Array) {
                hands[i] = [];
            }
            
            for(let j = 0; j < numPlayers; j++) {
                hands[i].push(this._deck.takeCard());
            }
        }
    }
}


const dealer = new Dealer("Kenny", "Rogers", new Deck());
console.log(dealer instanceof Player);
console.log(dealer instanceof Dealer);