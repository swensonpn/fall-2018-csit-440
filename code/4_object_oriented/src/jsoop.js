//view in Firefox developer toolbar
console.log(new String("Hello"));
console.log(new Array("zero","one","two"));
console.log(new Object());

function Card(suit, value) {
    //validate suit
    suit = parseInt(suit, 10);
    if(suit < 0 || suit > 3) throw new Error("Suit must be an integer between 0 and 3");
    
    //validate value
    value = parseInt(value, 10);
    if(value < 1 || value > 13) throw new Error("Value must be an integer between 1 and 13");
    
    this.suit = suit;//this keyword makes suit a puplic property of the Card instance.
    this.value = value;
}

Card.prototype.CLUB = 0;
Card.prototype.DIAMOND = 1;
Card.prototype.HEART = 2;
Card.prototype.SPADE = 3;

Card.prototype.getSuitName = function() {
   const names = ["club", "diamond", "heart", "spade"];
   
   return names[this.suit];
};
Card.prototype.getValueDescription = function() {
    switch(this.value) {
        case 1:
            return "ace";
        case 11:
            return "jack";
        case 12:
            return "queen";
        case 13:
            return "king";
        default:
            return this.value;
    }
};
Card.prototype.toString = function() {
    return `${this.getValueDescription()} of ${this.getSuitName()}s`;
};

let deck = [];

for(let i = 0; i < 4; i++) {//loop through four suits
    for(let j = 1; j < 14; j++) {
        const card = new Card(i, j);//created a new card instance
        //console.log(card.toString());//use the toString method
        deck.push(card);
    }
}
console.log(deck);

//the dealer is dealt these cards
const hand = [
    new Card(0, 10),
    new Card(2, 6)
];
console.log(hand[0].toString() + " and " + hand[1].toString());
//the dealer cheats
hand[1].value = 1;
console.log(hand[0].toString() + " and " + hand[1].toString() + " blackjack!!!");

function EncapsulatedCard(suit, value) {
    //validate suit
    suit = parseInt(suit, 10);
    if(suit < 0 || suit > 3) throw new Error("Suit must be an integer between 0 and 3");
    
    //validate value
    value = parseInt(value, 10);
    if(value < 1 || value > 13) throw new Error("Value must be an integer between 1 and 13");
    
    //suit is private, but we can get to it using the this.getSuit() method
    this.getSuit = function(){
        return suit;
    }
    
    //value is private, but we can get to it using the this.getValue() method
    this.getValue = function() {
        return value;
    }
}

EncapsulatedCard.prototype.getSuitName = function() {
    const names = ["club", "diamond", "heart", "spade"];
    
    return names[this.getSuit()];
};

EncapsulatedCard.prototype.getValueDescription = function() {
    switch(this.getValue()) {
        case 1:
            return "ace";
        case 11:
            return "jack";
        case 12:
            return "queen";
        case 13:
            return "king";
        default:
            return this.getValue();
    }
};

EncapsulatedCard.prototype.toString = function() {
    return `${this.getValueDescription()} of ${this.getSuitName()}s`;
};

//the dealer is dealt these cards
const encapsulatedHand = [
    new EncapsulatedCard(0, 10),
    new EncapsulatedCard(2, 6)
];
console.log(encapsulatedHand[0].toString() + " and " + encapsulatedHand[1].toString());
//the dealer trys to cheat
encapsulatedHand[1].value = 1;
console.log(encapsulatedHand[0].toString() + " and " + encapsulatedHand[1].toString() + " busted!!!");

function Hand(cards = []) {
    if(!cards instanceof Array) throw new Error("argument zero must be an array");
    
    this.getCards = function() {
        return cards.slice();
    };
    
    this.playCard = function(card) {
        for(let i = 0; i < cards.length; i++) {
            if(card === cards[i]) {
                return cards.splice(i, 1)[0];
            }
        }
    };
    
    this.takeCard = function(card) {
        if(card instanceof Card) cards.push(card);
    }
}

Hand.prototype.score = function() {
  return 0;  
};

const genericHand = new Hand([
        new Card(0, 2),
        new Card(1, 2)
    ]);

function BlackjackHand(cards = []) {
    Hand.call(this, cards);//inherit Hand
    if(this.getCards().length !== 2) throw new Error("Blackjack hands must start with two cards");
}

BlackjackHand.prototype = Object.create(Hand.prototype);
BlackjackHand.prototype.constructor = BlackjackHand;

BlackjackHand.prototype.score = function() {
    const cards = this.getCards();
 
    let totalScore = 0;
    let numAces = 0;
    
    for(let i = 0; i < cards.length; i++) {
        console.log(i);
         console.log(cards[i]);
         if(cards[i].getValue() > 10) {
            totalScore += 10;
        } else if(cards[i].getValue() === 1) {
        
            numAces += 1;
            totalScore += 11;
        } else {
            totalScore += cards[i].getValue();
        }
    }
    
    for(let i = 0; i < numAces.length; i++) {
        if(totalScore > 21) {
            totalScore -= 10;
        }
    }
    
    return totalScore;
};


const blackjackHand = new BlackjackHand([
        new Card(0, 7),
        new Card(1, 13)
    ]);

//show genericHand is instanceof Hand but not BlackjackHand
console.log(genericHand instanceof Hand);
console.log(genericHand instanceof BlackjackHand);
//show blackjackHand is an instanceof Hand & BlackjackHand
console.log(blackjackHand instanceof Hand);
console.log(blackjackHand instanceof BlackjackHand);
//show genericHand has an empty score and blackjackHand scores the hand
console.log(genericHand.score());
console.log(blackjackHand.score());