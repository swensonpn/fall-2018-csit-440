//importing a named value from an object
//import { MyClass, myFunction, myVariable } from "./modules/es_module/module1";

//importing a default value from and object
//import anyVariableName, { someFunction } from "./modules/es_module/module2";

import { MyClass, myFunction, myVariable, someFunction  } from "./modules/es_module";

console.log(MyClass);
console.log(myFunction);
console.log(myVariable);

//console.log(anyVariableName);
console.log(someFunction);
