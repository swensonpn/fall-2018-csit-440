// import the file system module for use in this module
let fs = require("fs");

//everything declared in a module is scoped to that module
let tempData = [
  {
    propertyOne:"valueOne"
  }
];

function serialize(object = {}) {
    if(typeof object !== "object") throw new Error("argument zero must be an object");
        return JSON.stringify(object);
}

function deserialize(string = "{}") {
    if(typeof string !== "string") throw new Error("argument zero must be a string");
    return JSON.parse(string);    
}

//expose a public method
module.exports.getFile = function(filepath = "", onfileRetrieved) {
    fs.readFile(filepath, "UTF-8", (err, data) => {
        if(err) {
            onfileRetrieved(err);
        } else {
            try {
                onfileRetrieved(null, deserialize(data));
            } catch(err) {
                onfileRetrieved(err);   
            }
        }
    });
};

//module is optional, because it is the global scope
exports.setFile = function(filepath = "", fileData = "", onFileSaved) {
    fs.writeFile(filepath, serialize(fileData), "UTF-8", onFileSaved);
};