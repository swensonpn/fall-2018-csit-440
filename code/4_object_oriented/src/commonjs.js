

// import module
// file and extension are optional if file is named index.js if filename is not index use filename without .js
var fileModule = require("./modules/commonjs");

//use import
fileModule.getFile("cars.json", function(err, data) {
    if(!err) {
        data.push({
            car:"Ford Model T"
        });
 
        fileModule.setFile("cars.json", data, function(err) {
            if(!err) {
                console.log("operation successful"); 
            } else {
                console.log(err);
            }
        });
    } else {
        console.log(err);
    }
});