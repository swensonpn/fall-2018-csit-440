//variables declared without a preceding keyword are global
myGlobalVariableOne = "I am global";

//variables directly assigned to the window are global.
/* window.myGlobalVariableTwo = "I am also global"; */

//variables declared with a keyword not in a closure are global
var myGlobalVariableThree = "Also global";


//example of block scoped variable
function blockExample(value) {
  if(value === 5) {//block
      let newValue = value + 5;
      console.log(newValue);
  }
  //console.log(newValue);//always undefined
}
blockExample(10);

//example of hoisting
function hoistingExample(value) {//closure
  if(value === 5) {//block
      var newValue = value + 5;
      console.log(newValue);
  }  
  console.log(newValue);
}
hoistingExample(10);

/* 
  constants declared in all caps are typically meant to indicate a variables
  that should be re-used.
*/
const ERROR_MESSAGE = "Oops.  Something went terribly wrong";

//they can also be used to make code less brittle.
const floor = Math.floor(3.14);
try {
    floor = 5;
} catch(err) {
    console.error(err);
}