let myBoolean = false;
console.log(typeof myBoolean === "boolean");
myBoolean = true;
console.log(typeof myBoolean === "boolean");

const myInteger = 5;
console.log(typeof myInteger === "number");

const myFloat = 3.14;
console.log(typeof myFloat === "number");

//special number values
const infinity = Infinity;//number too big to represent
const notANumber = NaN;//value is not a number

const myDoubleQuoteString = "Hello World. I'm a string";
console.log(typeof myDoubleQuoteString === "string");

const mySingleQuoteString = 'Quote "Hello World"';
console.log(typeof mySingleQuoteString === "string");

//strings enclosed in backticks can be expressed on multiple lines.
const myBacktickString = `
  <div>
    some content
  </div>
`;
console.log(typeof myBacktickString === "string");


let myUndefinedValue;
console.log(myUndefinedValue);
myUndefinedValue = 12;
console.log(myUndefinedValue);

let myNullValue = 12;
console.log(myNullValue);
myNullValue = null;
console.log(myNullValue);


const myParsedInteger = parseInt("19.2 inch");
console.log(myParsedInteger);//returns 19

const myParsedFloat = parseFloat("19.2 inch");
console.log(myParsedFloat);//returns 19.2

const myParsedNotANumber = parseInt("inch 19.2");
console.log(myParsedNotANumber);//returns NaN