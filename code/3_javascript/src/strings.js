//declare as a primitive
let stringTwo = "Hello String Two";
let stringThree = 'Hello String Three';
let stringFour = `Hello String Four`;

//create with the new keyword
let stringOne = new String("Hello String One");

//fun with string literals
let nameArr = ["Christian", "Hannah", "Jerry","Edward", "yuntao"];

function sayHi(name) {
    console.log(`
        <li>${name}</li>
    `);
}

nameArr.forEach(sayHi);

const stringOneLength = stringOne.length;
console.log(stringOneLength);

//upper case and lower case
const upperCase = stringOne.toUpperCase();
console.log(upperCase);//prints HELLO STRING ONE
const lowerCase = stringOne.toLowerCase();
console.log(lowerCase);//prints hello string one

//String.indexOf(searchString)
//returns the position of the first occurrence on searchString or -1 if not found
const stringHasString = stringOne.indexOf("One");
console.log("position: " + stringHasString);//prints position: 13
const stringDoesNotHaveString = stringOne.indexOf("Two");
console.log("position: " + stringDoesNotHaveString);//prints position: -1
//test if subject string contains needle
if("subject string".indexOf("string") > -1) {
    console.log("subject string contains search string");
} else {
    console.log("subject string does not contain search string");
}

//String.lastIndexOf(searchString)
//returns the position of the last occurance of searchString or -1 if not found
let url = "https://www.unk.edu/departments/csit/contact.html";
let fileNameStarts = url.lastIndexOf("/");
console.log(url.substr(fileNameStarts + 1));//prints "contact.html"

//String.includes(searchString)
//returns true if search string is contained in the subject or false if it is not
const includesSubjectString = "Hello World";
console.log("Includes: " + stringOne.includes("Hello"));//prints true

//String.startsWith(searchString)
//returns true if string starts with searchString
const startString = "Hello World";
console.log(startString.startsWith("He"));//prints true

//String.endsWith(searchString)
//returns true if string ends with searchString
const endString = "Hello World";
console.log(endString.endsWith("Wor"));//prints false

//String.replace(findCharSequence, replaceCharSequence)
//finds occurances of findCharSequence and replaces them with replaceCharSequence
let originalReplaceString = "Bobby pulls his dog red around in a red wagon";
console.log(originalReplaceString.replace(/red/g,"blue"));//"Bobby pulls his dog blue around in a blue wagon"
console.log(originalReplaceString);//original string did not change

//String.trim()
//removes whitespace from either end of a string
const spacePaddedString = "  ART";
console.log("|" + spacePaddedString + "|");//prints |  ART| 
const spacePaddedRemovedString = spacePaddedString.trim();
console.log("|" + spacePaddedRemovedString + "|");//prints |ART|
//there is no option to trim non-white space characters
const zeroPaddedString = "000252";
console.log(zeroPaddedString.replace(/^0*/g, ''));

//there is no method for padding a string either
const spacePaddingReplaced = " ".repeat(1) + spacePaddedString;
console.log("|" + spacePaddingReplaced + "|");//prints |  ART| 

//String.substr(start, length)
//returns a new string lenght characters long and starting at start (inclusive) 
const substrStringOneArg = stringOne.substr(3);
console.log(substrStringOneArg);//prints "lo String One
const substrStringTwoArg = stringOne.substr(3, 2);
console.log(substrStringTwoArg);//prints "lo"

//String.substring(start, end)
//returns a new string starting at start (inclusive) and ending at end (exclusive)
const substringStringOneArg = stringOne.substring(3);
console.log(substringStringOneArg);//prints "lo String One
const substringStringTwoArg = stringOne.substr(0, stringOne.indexOf(" "));
console.log(substringStringTwoArg);//prints "Hello"

//String.slice(start, end)
//works like substring except that the end parameter can be negative to specify a position from the end of the string
const sliceStringOneArg = stringOne.slice(3);
console.log(sliceStringOneArg);//prints "lo String One
const sliceStringTwoArg = stringOne.slice(3, -4);
console.log(sliceStringTwoArg);//prints "lo String"