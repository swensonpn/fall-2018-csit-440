//using the new operator
let arrayOne = new Array();
let arrayTwo = new Array(false, 1, "two");
//using short hand notation
let arrayThree = [];
let arrayFour = [false, 1, "two"];
//inspecting the array
console.log(arrayTwo);
console.log(arrayFour);

//get a value
let valueOne = arrayFour[1];//notice this is not the first item
console.log(valueOne);
//when a non-existing value is requested there is no error.
console.log(arrayFour[5]);

//set a value
arrayFour[5] = new Array("hello", "world");//notice this changes the size
console.log(arrayFour);

for(let i = 0; i < arrayFour.length; i++) {
  console.log(i + " is " + arrayFour[i]);
}

console.log(arrayOne.length);//returns 0
console.log(arrayTwo.length);//returns 3
console.log(arrayFour.length);//returns 6

let commaDelimitedString = arrayFour.join();
console.log(commaDelimitedString);

let customDelimitedString = ["valueOne","valueTwo","valueThree"].join("<-->");
console.log(customDelimitedString);

console.log(commaDelimitedString.split());
console.log(customDelimitedString.split("<-->"));

//clone the whole array
let newArrayOne = arrayFour.slice();
console.log(newArrayOne);//returns a deep copy of arrayFour
//create a new array starting at an indexed
let newArrayTwo = arrayFour.slice(2);
console.log(newArrayTwo);//returns array with items 2 - 5 of arrayFour
//create a new array starting at an index (inclusive) and ending at an index (exclusive)
let newArrayThree = arrayFour.slice(1,3);
console.log(newArrayThree);//returns array with items 1 - 2 of arrayFour

//casting array like objects to array
//Array.prototype.slice(arguments);

let spliceArrayOne = arrayFour.splice(3, 2);
console.log(spliceArrayOne);
console.log(arrayFour);

//add items into array
let spliceArrayTwo = arrayFour.splice(1, 0, "Hello", "World");
console.log(spliceArrayTwo);
console.log(arrayFour);

//remove and add items at the same time
let spliceArrayThree = arrayFour.splice(1, 2, "good", "bye");
console.log(spliceArrayThree);
console.log(arrayFour);

let arrayFive = [0,1,2,3,4,5];
//take 5 off the end of the array and log that item
console.log(arrayFive.pop());
console.log(arrayFive);
//add a new item to the end
arrayFive.push("new item five");
console.log(arrayFive);
//take item 0 off the beginning of the array and log it
console.log(arrayFive.shift());
console.log(arrayFive);
//add a new item to the beginning
arrayFive.unshift("new item zero");
console.log(arrayFive);

//reverse just flips the array
let arraySix = [0, 1, 2, 3];
arraySix.reverse();
console.log(arraySix);

//sort called without a parameter
arraySix.sort();
console.log(arraySix);

//sort can be called with a function as an argument for complex sorts
arraySix.sort(function(a, b) {
    return a%2 < b%2;
});
console.log(arraySix);//even items before odd ones

let arraySeven = [0,1,2,"three",4,5];
function isString(item) {
  return typeof item === "string";//return true if match false if not
}

//indexOf searches for the index of an item in an array
console.log(arraySeven.indexOf("three"));//returns 3
console.log(arraySeven.indexOf("four"));//returns -1 if item is not in the array
console.log(arraySeven.indexOf("four") > -1);

//find accepts a function as an argument and returns the first matched item in the array
let arrayFindMatchOne = arraySeven.find(isString);
console.log(arrayFindMatchOne);
//find returns undefined if no match is found
let arrayFindMatchTwo = [0,1,2].find(isString);
console.log(arrayFindMatchTwo);

function isNumber(item) {
  return typeof item === "number";//return a boolean
}

//pass a filtering function as an argument
let arrayFilterMatchesOne = arraySeven.filter(isNumber);
console.log(arrayFilterMatchesOne);

//returns an empty array if no matches are found
let arrayFilterMatchTwo = ["hello","world"].filter(isNumber);
console.log(arrayFilterMatchTwo);

let arrayEight = [5, 10, 15, 20];
//argument function receives three parameters. item(the current array item)
//index(the index of item) array(the array itself)
arrayEight.forEach(function(item, index, array) {
  if(index%2 === 0) {
    array[index] = item + " px";
  }
});
console.log(arrayEight);

let arrayMapResult = arrayEight.map(function(item, index, array) {
  return "index " + index + " is " + item;
});
console.log(arrayMapResult);//resulting array
console.log(arrayEight);//original array