//using the new operator
let objectOne = new Object();

//using the short hand notation
let objectTwo = {};
let objectThree = {propertyOne:"valueOne", propertyTwo: "valueTwo"};

console.log(objectTwo);
console.log(objectThree);


//get a value using dot notation
console.log(objectThree.propertyOne);

//get a value using square bracket notation with property name
console.log(objectThree["propertyOne"]);

//get a value using square bracket notation with variable
let propertyName = "propertyTwo";
console.log(objectThree[propertyName]);

//set a value using dot notation
objectOne.createdProperty = "properties don't have to exist to be set";
console.log(objectOne);

//set a value using square bracket notation with a string
objectOne["any property"] = false;

//set a value using square bracket notation with a variable
let propertyOne = "objectone";
objectOne[propertyOne] = propertyOne;

console.log(objectOne);

for(let n in objectOne) {
    console.log(n + ": " + objectOne[n]);
}

//convert to string using JSON.stringify()
let objectOneString = JSON.stringify(objectOne);
console.log(objectOneString);

//convert back to object using JSON.parse()
console.log(JSON.parse(objectOneString));

//parsing invalid JSON throws an exception
try {
    console.log(JSON.parse('{"property":"value"'));
} catch(err) {
    console.error(err);
}

//works with arrays too
let arrayOrig = [0,1,2,{color:"red"}];
let stringifiedArrayOrig = JSON.stringify(arrayOrig);
console.log(stringifiedArrayOrig);

let arrayRestored = JSON.parse(stringifiedArrayOrig);
console.log(arrayRestored);