let fs = require("fs");

function fixedToTwoDimensionalArray(fileData = "") {
    let lines = fileData.split("\n");
    
    for(let i = 0; i < lines.length; i++) {
        if(typeof lines[i] === "string") {
            let arr = [];
            arr.push(lines[i].substr(0, 39).trim());
            arr.push(lines[i].substr(39, 7).trim());
            arr.push(lines[i].substr(46, 9).trim());
            arr.push(lines[i].substr(55, 13).trim());
            arr.push(lines[i].substr(68, 11).trim());
            arr.push(lines[i].substr(79, 7).trim());
            arr.push(lines[i].substr(86, 13).trim());
            arr.push(lines[i].substr(99, 6).trim());
            arr.push(lines[i].substr(105, 7).trim());
            
            lines[i] = arr;
        }
    }
    
    return lines;
}

fs.readFile("cars.dat", "UTF-8", (err, data) => {
  if(err) throw new Error("Couldn't read data");
    data = fixedToTwoDimensionalArray(data);
    
    
    console.log(data);
});
