function coalesce(value) {
  return value || "default value";
}
console.log(coalesce(12));//returns 12
console.log(coalesce());//returns default value

function ternary(condition) {
  return (condition) ? "red" : "blue";
}
console.log(ternary(true));//returns red
console.log(ternary(false));//returns blue

const SKY_COLOR = "blue";
if(SKY_COLOR === "blue" ) {
  console.log("The sky is blue");
}

//basic if else
const trueOrFalse = (Math.random()  < .5) ? true : false;
if(trueOrFalse) {
  console.log("trueOrFalse was true");
} else {
  console.log("trueOrFalse was false");  
}

//multiple if statements.  Only one will execute
const favorite = Math.floor(Math.random() * 6);
if(favorite === 0) {
  console.log("red");
} else if(favorite === 1) {
  console.log("blue");  
} else if(favorite === 2) {
  console.log("yellow");  
} else if(favorite === 3) {
  console.log("green");  
} else if(favorite === 4) {
  console.log("purple");  
} else {
  console.log("orange");  
}

const colorsArray = ["red","blue","yellow","green","purple","orange"];
const color = colorsArray[Math.floor(Math.random() * 6)];
switch(color) {
  case "red":
    console.log(color + " cherry");
    break;
  case "blue":
    console.log(color + " berry");  
    break;
  case "yellow":
    console.log(color + " banana");  
    break;
  case "green":
    console.log(color + " apple");  
    break;
  case "purple":
    console.log(color + " plum");  
    break;
  case "orange":
    console.log(color + " orange");  
    break;
  default:
    console.log("can't find fruit");
}