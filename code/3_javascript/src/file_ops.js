let fs = require("fs");

let tempData = [
  {
    propertyOne:"valueOne"
  }
];

fs.readFile("cars.csv", "UTF-8", (err, data) => {
  if(err) throw new Error("Couldn't read data");

  tempData = JSON.stringify(tempData);

  fs.writeFile("cars.json", tempData, "UTF-8", (err) => {
    if(err) throw new Error("Couldn't write data");
  });
});
