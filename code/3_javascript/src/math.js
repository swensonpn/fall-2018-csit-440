console.log(Math.PI);

console.log(Math.abs(-1));//prints 1

console.log(Math.round(1.5));//prints 2

console.log(Math.ceil(1.1));

console.log(Math.floor(1.1));

//basic use
for(let i = 0; i < 10; i++) {
    console.log(Math.random());
}

//coin flip
for(let i = 0; i < 5; i++) {
    let side = (Math.floor(Math.random() * 2)) ? "heads" : "tails";
    console.log(side);
}

//roll the dice
for(let i = 0; i < 5; i++) {
    let diceOne = Math.ceil(Math.random() * 6);
    let diceTwo = Math.ceil(Math.random() * 6);
    
    console.log("one:" + diceOne + ", two:" + diceTwo + ", total:" + (diceOne + diceTwo));
}

//shuffle cards
let diamonds = ["2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "10d", "Jd", "Qd", "Kd", "Ad"];
let hearts = ["2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "Jh", "Qh", "Kh", "Ah"];
let spades = ["2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "Js", "Qs", "Ks", "As"];
let clubs = ["2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "10c", "Jc", "Qc", "Kc", "Ac"];

function dealNewHand(numPlayers = 2, cards = 7) {
    let deck = diamonds.concat(hearts, spades, clubs);
    let players = [];
  
    for(let i = 0; i < cards; i++) {
        for(let j = 0; j < numPlayers; j++) {
            let cardIndex = (Math.floor(Math.random() * deck.length));
            
            if(players[j] === undefined) {
                players[j] = [];
            }
            
            players[j].push(deck.splice(cardIndex, 1)[0]);
        }
    }
    
    return {deck: deck, hands: players};
}

let {deck, hands} = dealNewHand();
console.log(deck);
console.log(hands[0]);
console.log(hands[1]);