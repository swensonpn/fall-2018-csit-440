let whileKeepGoing = true;
while (whileKeepGoing) {
    whileKeepGoing = (Math.random()  < .5) ? true : false;
    
    console.log("while loop whileKeepGoing: " + whileKeepGoing);
}

let doWhileKeepGoing = false;
do {
    console.log("doWhileKeepGoing is false but executed once anyway");
} while(doWhileKeepGoing);

//the for loop has 3 statements
// statement 1: declare/set an index
// statement 2: the expression to test (like while loop)
// statement 3: increment/decrement the index
for(let i = 0; i < 5; i++) {
    console.log("for loop index: " + i);
}

let person = {
  name:"Roger",
  age:17,
  favoriteFood:"Pizza"
};

for(let n in person) {
    console.log("for in person " + n + ": " + person[n]);
}

let breakArray = ["bill","bob","brown"];

for(let i = 0; i < breakArray.length; i++) {
    if(breakArray[i] === "bob") {
        console.log("found bob");
        break;
    } else {
        console.log("did not find bob");
    }
}

let continueArray = ["Sally","sherri","Sissy"];
for(let i = 0; i < continueArray.length; i++) {
    if(continueArray[i].charAt(0) === continueArray[i].charAt(0).toUpperCase()) {
        continue;
    }
    
    console.log(continueArray[i] + " needs to be capitilized")
}