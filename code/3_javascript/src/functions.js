//global variable
let global = "I am global";

//outer most function
function outer() {
    let localToOuter = "I am local to outer";

    function inner() {
        let localToInner = "I am local to inner";
        console.log(global);
        console.log(localToOuter);
        console.log(localToInner);
    }

    console.log(inner());//only accessible inside outers closure
}
try {
    inner();//throws error
} catch(err) {console.error("can't call inner from global scope");
outer();//its okay to call outer, which can call inner
}


//calling a declaritive function
function addTwoNumbers(a, b) {
    return a + b;
}
console.log(addTwoNumbers(5, 10));//prints 15

//calling an expression function
const remainderOfTwoNumbers = function(a, b) {
    return a % b;
};
console.log(remainderOfTwoNumbers(2, 1));//prints 0

//calling an expression function passed into a function
function doCallback(callbackFunction) {
    callbackFunction();
}
console.log(doCallback(function(){console.log("Hello");}));//prints "Hello"

//calling a function at the same time its declared
const resultOfAMinusB = ((a, b) => {return a - b})(3, 2);
console.log(resultOfAMinusB);//prints 1

//no return value
function noReturn() {}
console.log(noReturn());//prints undefined

//return with no value
function undefinedReturn(){
  return;
}
console.log(undefinedReturn());//prints undefined

//return a value
function simpleReturn(){
  return "simple";
}
simpleReturn();
console.log(simpleReturn());//prints "simple"

//more than one possible return
function oneReturnOrAnother(){
    if(Math.random() < .5) {
        return "less than";
    } else {
        return "not less than";
    }
}
console.log(oneReturnOrAnother());//prints "less than" or "not less than"

//returning more than one value from a function
function multipleValuesInAReturn() {
    return {color:"red", shape:"sphere"};
}

//capture the object and use its values
let objectOne = multipleValuesInAReturn();
console.log(objectOne);//prints {color:"red", shape:"sphere"}
console.log(objectOne.color);//prints "red"
console.log(objectOne.shape);//prints "sphere"

//get as two variables color and shape
let { color, shape } = multipleValuesInAReturn();
console.log(color);//prints "red"
console.log(shape);//prints "sphere"

//stop execution by return
function setAttribute(key, value) {//key is to be a required string
    if(typeof key !== "string") return;//if key is not passed it will be undefined
    //code to execute if arguments meet validation
}

//stop execution by throwing an error
function requiredCallback(callbackFunction) {
    if(typeof callbackFunction !== "function") throw new Error("argument zero must be a function");
    //code to execute if arguments meet validation
}


//Use coalesce to set a default value (good when type test not needed)
function defaulByCoalesce(optionalParameter) {
    optionalParameter = optionalParameter || "default value";
}


//Use ternary conditional to set a default value (good when type test is needed)
function defaulByTernary(optionalParameter) {
    optionalParameter = (typeof optionalParameter === "string") ? optionalParameter : "default value";
}

//Use ES5 parameter declartion defaults (good when type test not needed)
function defaulByTernary(optionalParameter = "default value") {
    //optionalParameter is "default value" when an argument is not provided
}


//common technique for accepting optional configuration
function setOptions(options = {}) {
    const configuration = {
      debugMode:false,
      maxHeight:50,
      minHeight:10
    };

    for(let n in configuration) {
        if(options[n] !== undefined) {
            configuration[n] = options[n];
        }
    }
}
