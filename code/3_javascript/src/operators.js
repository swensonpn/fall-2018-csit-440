console.log("addition: " + (1+1));//addition: 2
console.log("concatonation: " + ("Hello" + " " + "World"));//concatonation: 2

console.log("subtraction: " + (1-1));//subtraction: 0

console.log("multiplication: " + (1*1));//multiplication: 1

console.log("division: " + (2/1));//division: 2

console.log("modulus: " + (2%1));//modulus: 0

let positiveValue = 1
console.log("Negated: " + (- positiveValue));//negated: -1

let incrementedValue = 1
console.log("incremented: " + (incrementedValue++));//incremented: 2

let decrementedValue = 1
console.log("decremented: " + (decrementedValue--));//decremented: 0


const assignedValue = 10;
console.log("assigned: " + assignedValue);//assigned: 10

let addToAssigned = 10;
addToAssigned += 10;
console.log("add to value: " + addToAssigned);//assigned: 20

addToAssigned += " more days";
console.log("concatonate to value: " + addToAssigned);//concatonate: 20 more days

let subtractFromAssigned = 10;
subtractFromAssigned -= 10;
console.log("subtract from value: " + subtractFromAssigned);//subtract from value: 0

let multiplyByAssigned = 10;
multiplyByAssigned *= 10;
console.log("multiply by value: " + multiplyByAssigned);//multiply by value: 100

let divideByAssigned = 10;
multiplyByAssigned /= 10;
console.log("divide by value: " + divideByAssigned);//divide by value: 1

let remainderDivideByAssigned = 10;
remainderDivideByAssigned /= 10;
console.log("remainder divide by value: " + remainderDivideByAssigned);//remainder divide by value: 0

console.log("1 is greater than 0: " + (1>0));

console.log("1 is less than 2: " + (1<2));

console.log("1 is greater than or equal to 1: " + (1>=1));

console.log("1 is less than or equal to 1: " + (1<=1));

console.log("null is equal in value to undefined: " + (null == undefined));

console.log("null is not equal in value and type to undefined: " + (null === undefined));

console.log("\"true\" is equal to true in value: " + ("true" != true));

console.log("\"true\" is equal to true in value and type: " + ("true" !== true));

console.log("1 is equal to 1 and 2 is equal to 2: " + (1===1 && 2===2));

console.log("1 is equal to 5 or 2 is equal to 2: " + (1===5 || 2===2));

console.log((1===2 || 2===2) && 3===3);