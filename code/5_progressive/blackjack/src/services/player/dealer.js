import { Player } from "./player";

export class BlackjackDealer extends Player {
    constructor() {
        super("Dealer");
    }
    
    hit() {
        return this.hand.score() < 17;
    }
    
    isDealer() {
        return true;
    }
}