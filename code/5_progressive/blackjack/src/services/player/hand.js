import { Card } from "../cards";

export class Hand {
    constructor(cards = []) {
        if(!cards instanceof Array) throw new Error("argument zero must be an Array");
        for(let i = 0; i < cards.length; i++) {
            if(!cards[i] instanceof Card) {
                throw new Error("argument zero must contain only Card objects");
            }
        }
        this._cards = cards;
    }
    
    get cards() {
        return this._cards.slice();
    }
    
    drawCard(card) {
        if(card instanceof Card) {
            this._cards.push(card);
        }
    }
    
    playCard(card) {
        if(card) {
            for(let i = 0; i < this._cards.length; i++) {
                if(card === this._cards[i]) {
                    return this._cards.splice(i, 1)[0];
                }
            }
        }
        
        return null;
    }
    
    score() {
        return 0;
    }
}