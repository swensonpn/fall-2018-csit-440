import { Hand } from "./hand";

export class BlackjackHand extends Hand {
    constructor(cards = []) {
        super(cards);
        if(cards.length !== 2) throw new Error("hand must start with 2 cards");
    }
    
    isBlackjack() {
        return this.score() === 21;
    }
    
    isBust() {
        return this.score() > 21;
    }
    
    score() {
        const cards = this.cards;
     
        let totalScore = 0;
        let numAces = 0;
        
        for(let i = 0; i < cards.length; i++) {
             if(cards[i].value > 10) {
                totalScore += 10;
            } else if(cards[i].value === 1) {
            
                numAces += 1;
                totalScore += 11;
            } else {
                totalScore += cards[i].value;
            }
        }
        
        for(let i = 0; i < numAces.length; i++) {
            if(totalScore > 21) {
                totalScore -= 10;
            }
        }
    
        return totalScore;
    }

}