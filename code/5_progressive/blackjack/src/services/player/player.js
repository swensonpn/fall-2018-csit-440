import { Hand } from "./hand";

export class Player {
    constructor(name = ("player " + Date.now())) {
        this._name = name;
        this._hand = new Hand();
    }
    
    get name() {
        return this._name;
    }
    
    set name(newName = ("player " + Date.now())) {
        this._name = newName;
    }
    
    get hand() {
        return this._hand;
    }
    
    set hand(newHand = new Hand()) {
        if(newHand instanceof Hand) {
            this._hand = newHand;
        }
    }

    isDealer() {
        return false;    
    }
    
    toString() {
        return `${this._name}: ${this._hand.cards.map((card) => {return card.toString();})} = ${this._hand.score()}`;
    }
}