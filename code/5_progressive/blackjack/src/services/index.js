import { Deck } from "./cards";
import { Player, BlackjackDealer, BlackjackHand } from "./player";

export { Card } from "./cards";
export const deckService = new Deck();
export const players = (function(deck){
    deckService.shuffle();
    
    function dealPlayer(deck, player) {
        const cards = [deck.draw(), deck.draw()];
        player.hand = new BlackjackHand(cards);
    }

    
    const players = [
        new BlackjackDealer("Dealer"), 
        new Player("Player 1"), 
        new Player("Player 2"), 
        new Player("Player 3")
    ];
    
    for(let i = 0; i < players.length; i++) {
        dealPlayer(deckService, players[i]);
    }
    
    return players;
})(deckService);
