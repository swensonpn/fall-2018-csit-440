import React from 'react';
import './App.css';
import { Player } from "./components/players";
import { deckService, players } from "./services";

class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      currentPlayer: 1,
      winners:[]
    };
    
    this.handleHit = this.handleHit.bind(this);
    this.handleStay = this.handleStay.bind(this);
  }


  handleStay() {
    const newPlayer = (this.state.currentPlayer < 3) ? this.state.currentPlayer + 1 : 0;
    const winners = [];
    
    if(newPlayer === 0) {
      while(players[0].hit()) {
        players[0].hand.drawCard(deckService.draw());
      }
      
      let dealerScore = players[0].hand.score();
      for(let i = 1; i < 4; i++) {
        if(!players[i].hand.isBust() 
            && (players[i].hand.isBlackjack() 
            || players[i].hand.score() > dealerScore)) {
          
          winners[i] = true;
        } else {
          winners[i] = false;
        }
      }
    }
    
    this.setState({
      currentPlayer: newPlayer,
      winners: winners
    });
    
    
  }
  
  handleHit(player) {
    player.hand.drawCard(deckService.draw());
    this.forceUpdate();
  }


  render() {
    
    return (
      <div id="blackjack" className="w3-container w3-green">
        <h1>Blackjack</h1>
        
        <div id="game_board">
            <div>{/* empty player space */}</div>
            <div>
              <Player player={players[0]} 
                      isMyTurn={ this.state.currentPlayer === 0 }
                      isWinner={ false }
                      handleHit={ this.handleHit }
                      handleStay={ this.handleStay }/>
            </div>
            <div >{/* empty player space */}</div>
            <div>
              <Player player={players[1]} 
                      isMyTurn={ this.state.currentPlayer === 1 }
                      isWinner={ this.state.winners[1] }
                      handleHit={ this.handleHit }
                      handleStay={ this.handleStay }/>
            </div>
            <div>
              <Player player={players[2]} 
                      isMyTurn={ this.state.currentPlayer === 2 }
                      isWinner={ this.state.winners[2] }
                      handleHit={ this.handleHit }
                      handleStay={ this.handleStay }/>
            </div>
            <div>
              <Player player={players[3]}
                      isMyTurn={ this.state.currentPlayer === 3 }
                      isWinner={ this.state.winners[3] }
                      handleHit={ this.handleHit }
                      handleStay={ this.handleStay }/>
            </div>
        </div>
      </div>
    );
  }
}

export default App;
