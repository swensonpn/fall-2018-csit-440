import back from "./BACK.svg";
import c1 from "./C1.svg";
import c2 from "./C2.svg";
import c3 from "./C3.svg";
import c4 from "./C4.svg";
import c5 from "./C5.svg";
import c6 from "./C6.svg";
import c7 from "./C7.svg";
import c8 from "./C8.svg";
import c9 from "./C9.svg";
import c10 from "./C10.svg";
import c11 from "./C11.svg";
import c12 from "./C12.svg";
import c13 from "./C13.svg";
import d1 from "./D1.svg";
import d2 from "./D2.svg";
import d3 from "./D3.svg";
import d4 from "./D4.svg";
import d5 from "./D5.svg";
import d6 from "./D6.svg";
import d7 from "./D7.svg";
import d8 from "./D8.svg";
import d9 from "./D9.svg";
import d10 from "./D10.svg";
import d11 from "./D11.svg";
import d12 from "./D12.svg";
import d13 from "./D13.svg";
import h1 from "./H1.svg";
import h2 from "./H2.svg";
import h3 from "./H3.svg";
import h4 from "./H4.svg";
import h5 from "./H5.svg";
import h6 from "./H6.svg";
import h7 from "./H7.svg";
import h8 from "./H8.svg";
import h9 from "./H9.svg";
import h10 from "./H10.svg";
import h11 from "./H11.svg";
import h12 from "./H12.svg";
import h13 from "./H13.svg";
import s1 from "./S1.svg";
import s2 from "./S2.svg";
import s3 from "./S3.svg";
import s4 from "./S4.svg";
import s5 from "./S5.svg";
import s6 from "./S6.svg";
import s7 from "./S7.svg";
import s8 from "./S8.svg";
import s9 from "./S9.svg";
import s10 from "./S10.svg";
import s11 from "./S11.svg";
import s12 from "./S12.svg";
import s13 from "./S13.svg";
import j1 from "./J1.svg";
import j2 from "./J2.svg";

export class CardImage {
    
    static get map() {
        return {
            back:back,
            c1:c1,
            c2:c2,
            c3:c3,
            c4:c4,
            c5:c5,
            c6:c6,
            c7:c7,
            c8:c8,
            c9:c9,
            c10:c10,
            c11:c11,
            c12:c12,
            c13:c13,
            d1:d1,
            d2:d2,
            d3:d3,
            d4:d4,
            d5:d5,
            d6:d6,
            d7:d7,
            d8:d8,
            d9:d9,
            d10:d10,
            d11:d11,
            d12:d12,
            d13:d13,
            h1:h1,
            h2:h2,
            h3:h3,
            h4:h4,
            h5:h5,
            h6:h6,
            h7:h7,
            h8:h8,
            h9:h9,
            h10:h10,
            h11:h11,
            h12:h12,
            h13:h13,
            s1:s1,
            s2:s2,
            s3:s3,
            s4:s4,
            s5:s5,
            s6:s6,
            s7:s7,
            s8:s8,
            s9:s9,
            s10:s10,
            s11:s11,
            s12:s12,
            s13:s13,
            j1:j1,
            j2:j2
        };
    }
}