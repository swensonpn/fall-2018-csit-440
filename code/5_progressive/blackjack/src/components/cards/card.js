import React from "react";
import {CardImage} from "./img";

function getCardPath(suit, value, isFaceUp) {
    
    if(isFaceUp) {
        
        switch(suit) {
            case 0:
                return CardImage.map["c"+value]
            case 1:
                return CardImage.map["d"+value]
            case 2:
                return CardImage.map["h"+value]
            default:
                return CardImage.map["s"+value]
        }
    } else {
        return CardImage.map["back"];
    }
}

export class Card extends React.Component {

    render() {
        
        return (
            <img src={  getCardPath(this.props.cardObj.suit, this.props.cardObj.value, this.props.faceUp) } 
                alt={ this.props.cardObj.toString() }
                style={{width:"20%"}}/>
        );
    }
}
