/*
contentEditable={true}
suppressContentEditableWarning={true}
*/
import React from 'react';
import { Card } from "../cards";

function getWinnerLoserMessage(isWinner) {
    if(isWinner === undefined) {
        return "";
    } else if(isWinner) {
        return "Winner";
    } else {
        return "Loser";
    }
}

export class Player extends React.Component {
    constructor(props) {
        super(props);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleNameChange(e) {
        const name = e.target.innerHTML;
        
        if(name !== "") {
            this.props.player.name = name;
            this.forceUpdate();
        }
    }
    
    handleClick(e) {
        switch(e.target.innerHTML.toLocaleLowerCase()) {
            case "hit":
                this.props.handleHit(this.props.player);
                break;
            case "stay":
                this.props.handleStay(this.props.player);
                break;
            default:
                throw new Error("Only stay and hit are valid options");
        }
    }
    
    render() { 
        const hand = this.props.player.hand.cards.map((card, index) => {
            const cardUp = (index === 1 
            || !this.props.player.isDealer() 
            || this.props.isMyTurn) ? true : false;
            
            return <Card key={ index } cardObj={ card } faceUp={ cardUp }/>;
        });
        
        const buttons = (this.props.isMyTurn && !this.props.player.isDealer()) ?
            <div className="w3-container w3-right">
                <button className="w3-button w3-ripple w3-white w3-margin-bottom"
                        onClick={ this.handleClick }>Stay</button>
                
                <button className="w3-button w3-ripple w3-white w3-margin-bottom w3-margin-left"
                        onClick={ this.handleClick }>Hit</button>
            </div>
            : 
            "";
    
        return (
            <div>
                <h2 contentEditable={true}
                    suppressContentEditableWarning={true}
                    onBlur={ this.handleNameChange }>{ this.props.player.name }</h2>
                <div className="w3-margin-bottom">
                  { hand }
                </div>
                <div className="w3-left" style={{padding:"8px 16px"}}>Score:&#160; 
                    {
                        (function(player, isMyTurn, isWinner){
                            let score = "";
                            
                            if(player.isDealer() && !isMyTurn) {
                                score = "";
                            } else if (player.hand.isBust()) {
                                score = player.hand.score();
                            } else if(player.hand.isBlackjack()) {
                                score = " Blackjack";
                            } else {
                                score = player.hand.score();
                            }
                            
                            if(!player.isDealer()) {
                                score += " " + getWinnerLoserMessage(isWinner);
                            }
                            
                            return score;
                        })(this.props.player, this.props.isMyTurn, this.props.isWinner)
                    }
                </div>
                { buttons }
            </div>
        );
    }
}