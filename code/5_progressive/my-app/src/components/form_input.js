import React from "react";

export class FormInput extends React.Component {
    constructor(props) {
        super(props);
        
        //set intial state and/or bind event handlers
        this.state = {
            hasFocus:false  
        };
    }
    
    
    
    
    render() {
        const backgroundColorValue = (this.state.hasFocus) ? "lightgray" : "transparent";
        
        const isRequired = (this.props.required === true) ? true : false;
        
        return (
            <div style={{backgroundColor: backgroundColorValue}}>
                <label for={ this.props.name }>
                    { this.props.label }
                    { (isRequired) ? "*" : "" }
                </label>
                <input id={ this.props.name } name={ this.props.name } required={isRequired}/>
            </div>
        );
    }
}