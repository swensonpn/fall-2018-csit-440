import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { FormInput } from "./components";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Howdy Partner<br/>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        <main>
          <form style={{marginBottom:"300px"}}>
            <FormInput name="firstname" label="First Name" required={true}/>
            <FormInput name="middlename" label="Middle Name"/>
            <FormInput name="lastname" label="Last Name" required={true}/>
            <FormInput name="color" label="Favorite Color"/>
            <FormInput name="age" label="Age" required={true}/>
            <input type="submit" value="submit"/>
          </form>
        </main>
      </div>
    );
  }
}

export default App;
