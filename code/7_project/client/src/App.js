import React, { Component } from 'react';
import './App.css';
import { deleteTask, getTask, getTasks, saveTask } from './data';
import { groupTasks, moveTask, removeTask, replaceTask } from './functions';
import { Board, LaneGroup, Lane, Card } from 'react-sticky-notes';
import { Modal, Form } from './components';

const date = Date("2018-06-30");

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tasks: groupTasks()
    };
    
    this.handleCardAdd = this.handleCardAdd.bind(this);
    this.handleCardClose = this.handleCardClose.bind(this);
    this.handleCardDelete = this.handleCardDelete.bind(this);
    this.handleCardMove = this.handleCardMove.bind(this);
    this.handleCardOpen = this.handleCardOpen.bind(this);
    this.handleCardSave = this.handleCardSave.bind(this);
  }

  componentWillMount() {                                                        
    getTasks(date, (err, json) => {
      if(err) {
        this.setState({
          tasks: groupTasks([])
        });
        console.log("using local data");
      } else {
        this.setState({
          tasks: groupTasks(json)
        });
      }
    });
  }

  handleCardAdd(laneId) {
    this.setState({
      editing:{
        type:"backlog",
        description:"",
        due_date:""
      },
      mode:"create"
    });
  }

  handleCardClose() {
    this.setState({
      editing:undefined,
      mode:""
    });
  }
  
  handleCardDelete(card) {
    deleteTask(card, (err, card) => {
      if(err) {
        console.error(err);
      } else {
        this.setState({
          tasks: removeTask(card, this.state.tasks)
        });
      }
    });
    this.handleCardClose();
  }

  handleCardMove(evt) {
    if(typeof evt === "object") {console.log(evt);
      this.setState({
        items: moveTask(this.state.tasks, evt.destination.laneId, evt.destination.index, evt.source.laneId, evt.source.index)
      });
      
      const card = getTask(evt.cardId);
      
      card.type = evt.destination.laneId;
      
      saveTask(card, (err, card) => {
        if(err) {
          console.error(err);
        }
      });
    }
  }

  handleCardOpen(cardId) {
    const task = getTask(cardId);

    if(task) {
      this.setState({
        editing:task,
        mode:"edit"
      });
    }
  }
  
  handleCardSave(card) {
    saveTask(card, (err, card) => {
      if(err) {
        console.error(err);
      } else {
        console.log(this.state.tasks);
        console.log(replaceTask(card, this.state.tasks))
        this.setState({
          tasks: this.state.tasks
        });
      }
    });
    this.handleCardClose();
  }

  render() {                                                                    
    const tasks = this.state.tasks,
          isModalOpen = typeof this.state.editing === "object",
          form = (isModalOpen) ? 
            <Form mode={ this.state.mode } 
                  card={ this.state.editing }
                  handleDelete={ this.handleCardDelete }
                  handleSave={ this.handleCardSave }/> 
            : 
            "";
          
    return (
      <div>
        <Modal open={ isModalOpen } handleClose={ this.handleCardClose }>
          { form }
        </Modal>
        <Board label="Personal Kanban" onDragEnd={ this.handleCardMove } listPropertyName="type">
          <LaneGroup>
            <Lane label="Backlog" laneId="backlog"  onAddCard={ this.handleCardAdd }>
              {
                tasks.backlog.map((task, index) => {
                  return <Card key={ task._id } draggableId={ task._id } index={ index } handleClick={ this.handleCardOpen }>{ task.description }</Card>;
                })
              }
            </Lane>
            <LaneGroup label="Doing" width={ 3/4 }>
              <Lane label="Ready" laneId="ready">
                {
                  tasks.ready.map((task, index) => {
                    return <Card key={ task._id } draggableId={ task._id } index={ index } handleClick={ this.handleCardOpen }>{ task.description }</Card>;
                  })
                }
              </Lane>
              <Lane label="Working" laneId="working">
                {
                  tasks.working.map((task, index) => {
                    return <Card key={ task._id } draggableId={ task._id } index={ index } handleClick={ this.handleCardOpen }>{ task.description }</Card>;
                  })
                }
              </Lane>
              <Lane label="Done" laneId="complete">
                {
                  tasks.complete.map((task, index) => {
                    return <Card key={ task._id } draggableId={ task._id } index={ index } handleClick={ this.handleCardOpen }>{ task.description }</Card>;
                  })
                }
              </Lane>
            </LaneGroup>
          </LaneGroup>
        </Board>
      </div>
    );
  }
}

export default App;