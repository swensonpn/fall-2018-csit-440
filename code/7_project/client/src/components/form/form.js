import React, { Component } from 'react';
import './form.css';

export class Form extends Component {
  constructor(props) {
    super(props);
    
    this.handleChange = this.handleChange.bind(this);
    
    this.state = {card: this.props.card };
  }
  
  componentWillMount() {
    this._card = this.props.card || {};
  }
    
  handleChange(e) {
    this._card[e.target.name] = e.target.value;
    this.forceUpdate();
  }
  
  handleDelete(e) {
    if(typeof this.props.handleDelete === "function") {
      this.props.handleDelete(this._card);
    }
  }
  
  handleSubmit(e) {
    e.preventDefault();
    
    if(typeof this.props.handleSave === "function") {
      this.props.handleSave(this._card);
    }
  }

  render() {
    const isCreateMode = this.props.mode === "create",
          label = (isCreateMode) ? "New Card" : "Edit Card",
          deleteButton = (!isCreateMode) ? 
            <button className="card-form-buttons-delete" 
                    form="card-form" 
                    type="button" 
                    onClick={ this.handleDelete.bind(this) }>Delete</button> 
            : 
            "";
          
    const due =  (this._card.due) ? this._card.due.split("T")[0] : "";
    
    return(
      <form className="card-form" id="card-form" onSubmit={ this.handleSubmit.bind(this) }>
        <fieldset>
            <legend>{ label }</legend>
            <input type="hidden" name="id" value={ this._card._id }/>
            <div>
                <label>Task</label>
                <textarea name="description" value={ this._card.description } onChange={ this.handleChange } required={ true }/>
            </div>
            <div>
                <label>Due Date</label>
                <input type="date" name="due" value={ due } onChange={ this.handleChange } required={ true }/>
            </div>
            <div className="card-form-buttons">
                { deleteButton }
                <button className="card-form-buttons-save" form="card-form" type="submit">Save</button>
            </div>
        </fieldset>
      </form>
    );
  }
}