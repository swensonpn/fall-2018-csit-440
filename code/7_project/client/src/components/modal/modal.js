import React, { Component } from 'react';
import './modal.css';

export class Modal extends Component {

  render() {

    return(
      <div className={ `modal${(this.props.open === true) ? " open" : ""} ` }>
        <div className="modal-content">
            <button className="close" onClick={ this.props.handleClose }>&times;</button>
          <div>

            { this.props.children }
          </div>
        </div>
      </div>
    );
  }
}
