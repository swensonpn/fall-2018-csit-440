

export function groupTasks(taskList = []) {
  const groupedTasks = {
    backlog:[],
    ready:[],
    working:[],
    complete:[]
  };

  for(let i=0; i<taskList.length; i++){
    if(groupedTasks[taskList[i].type]) {
      groupedTasks[taskList[i].type].push(taskList[i]);
    }
  }

  return groupedTasks;
}

export function moveTask(tasks, destId, destIndex, srcId, scrIndex) {
  const srcList = Array.from(tasks[srcId]),
        destList = (srcId === destId) ? srcList : Array.from(tasks[destId]);

  const [task] = srcList.splice(scrIndex, 1);

  task.type = destId;
  destList.splice(destIndex, 0, task);


  tasks[destId] = destList;
  tasks[srcId] = srcList;

  return tasks;
}

export function removeTask(card, groupedTasks) {
  if(groupedTasks[card.type]) {
    for(let i = 0; i < groupedTasks[card.type].length; i++) {
      if(groupedTasks[card.type][i]._id === card._id) {
        groupedTasks[card.type].splice(i, 1);
        return groupedTasks;
      }
    }
  }
  
  return groupedTasks;
}

export function replaceTask(card, groupedTasks) {
  if(groupedTasks[card.type]) {
    for(let i = 0; i < groupedTasks[card.type].length; i++) {
      if(groupedTasks[card.type][i]._id === card._id) {
        groupedTasks[card.type][i] = card;
        return groupedTasks;
      }
    }
    groupedTasks[card.type].push(card);
  }
  
  return groupedTasks;
}
