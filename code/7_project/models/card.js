const mongoose = require("mongoose"),
      Schema = mongoose.Schema;
      
const cardSchema = new Schema({
  type: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  due: {
    type: Date,
    required: true
  },
  complete: {
    type: Date
  }
},
{
  collection: "cards"
});

module.exports = mongoose.model('Card', cardSchema);