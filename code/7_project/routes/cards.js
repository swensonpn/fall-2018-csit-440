const CardModel = require("../models/card");

exports.index = function(request, response) {               console.log("index")
    CardModel
        .find({complete:null})
        .exec(function(err, cards) {
             if (err) {
                 console.error(err);
                 response.writeHead(500);
                 response.end("error");
            } else {
                response.send(cards);
             }
         });
};

exports.save = function(request, response) {                    console.log("save")
    const id = request.body._id;
    
    if(id) {
        CardModel.findById(id, function(err, card){
            if(err) {
                console.error(err);
                response.writeHead(404);
                response.end("not found");
            } else {
                card.type = request.body.type;
                card.description = request.body.description;
                card.due = request.body.due;
                if(request.body.type === "complete") {
                    card.complete = new Date();
                }
                card.save(function() {
                    response.writeHead(200);
                    response.end(JSON.stringify(card));
                });
            }
        });
    } else {
        CardModel.create({
          type: request.body.type,
          description: request.body.description,
          due: request.body.due
        }, function(err, card) {
            if(err) {
                console.error(err);
                response.writeHead(500);
                response.end("error");
            } else {
                response.writeHead(201);
                response.end(JSON.stringify(card));
            }
        });
    }
};    

exports.delete = function(request, response) {                          console.log("delete")
    const id = request.body._id;
    
    if(id) {
        CardModel.findById(id, function(err, card){
            if(err) {
                response.writeHead(404);
                response.end("not found");
            } else {
                CardModel.remove({_id:id},function(err) {
                    if(err) {
                        console.error(err);
                        response.writeHead(500);
                        response.end("error");
                    } else {
                        response.writeHead(202);
                        response.end(JSON.stringify(card));
                    }
                });
            }
        });
    } else {
        response.writeHead(404);
        response.end("not found");
    }
};