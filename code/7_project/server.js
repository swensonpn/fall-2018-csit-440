const express = require('express');
const path = require('path'),
      bodyParser = require('body-parser'),
      mongoose = require('mongoose'),
      cardRoutes = require("./routes/cards");



const app = express();
const port = process.env.PORT || 5000;
const connectString = "mongodb://localhost:27017/test";

/*
 * Setting up and running MongoDB in Cloud9
 * 1. mkdir data
 * 2. echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
 * 3. chmod a+x mongod
 * START
 * ./mongod
 * STOP
 * CNTRL + C
 **/
// Mongoose API http://mongoosejs.com/docs/api.html
mongoose
  .connect(connectString, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch((err) => {console.log('Mongo Error:'); console.log(err);});
  

process.on('SIGINT',function(){
  mongoose.connection.close(function(){
    console.log('MongoDB disconnected due to Application Exit');
    process.exit(0);
  });
});

// Parse json body
app.use(bodyParser.json())

// Serve any static files from react
app.use(express.static(path.join(__dirname, 'client/build')));

// Assign api routes
app.get("/api", cardRoutes.index);
app.post("/api", cardRoutes.save);
app.delete("/api", cardRoutes.delete);

// Assign client app routes
app.get('*', function(req, res) {
 res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});



app.listen(port, () => console.log(`Listening on port ${port}`));

