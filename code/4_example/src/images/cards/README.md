Cards from
https://commons.wikimedia.org/wiki/Category:OpenClipart_simple_playing_cards

Description	
This playing card is part of a complete deck.

Creative Commons icon
CC-Zero

This file is from the Open Clip Art Library, which released it explicitly into the public domain (see here). To the uploader: Please provide as parameter the link to the page where this image appears.
বাংলা | Čeština | Deutsch | Ελληνικά | English | Español | Français | Italiano | 日本語 | Македонски | Malti | Nederlands | Polski | Português | Português do Brasil | Română | Русский | Svenska | Türkçe | Українська | 中文（简体）‎ | 中文（繁體）‎ | +/−	Open Clip Art Library logo
Creative Commons CC-Zero	This file is made available under the Creative Commons CC0 1.0 Universal Public Domain Dedication.
The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law. You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission.
Date	
Source	http://openclipart.org/cgi-bin/navigate/recreation/games/cards/simple
Author	Nicu Buculei
