import { Player, Out, PlayerOut, Deck } from "../card_game";
import { BlackjackHand } from "./hand";
import { BlackjackDealer } from "./dealer";

function dealPlayer(deck, player) {
    const cards = [deck.draw(), deck.draw()];
    player.hand = new BlackjackHand(cards);
}

 
export class BlackjackGame {
    constructor(options = {}) {
        if(typeof options === "object") {
            this._players = (options.players instanceof Array) ? options.players : [new Player("player 1")];
            this._dialogOut = (options.dialogOut instanceof Out) ? options.dialogOut : new Out();
            this._playerOut = (options.playerOut instanceof PlayerOut) ? options.playerOut : new PlayerOut();
            
            for(let i = 0; i < this._players.length; i++) {
                if(!this._players[i] instanceof Player) throw new Error("all players must be of type Player");
            }
            
            this._players.push(new BlackjackDealer());
        }
    }
    
    start() {
        const deck = new Deck();
        deck.shuffle();
        
        //deal cards
        for(let i = 0; i < this._players.length; i++) {
            dealPlayer(deck, this._players[i]);
        }
        
        
        //play
        for(let i = 0; i < this._players.length; i++) {
            let turnOver = true;
            
            do {
                this._playerOut.write(this._players[i]);
                
                if(this._players[i].hand.isBlackjack()) {
                    this._dialogOut.write(this._players[i].name + ": blackjack");
                    turnOver = true;
                } else if(this._players[i].hand.isBust()) {
                    this._dialogOut.write(this._players[i].name + ": busted");
                    turnOver = true;
                } else if(this._players[i] instanceof BlackjackDealer) {
                    if(this._players[i].hit()) {
                        this._dialogOut.write(this._players[i].name + ": hits");
                        this._players[i].hand.drawCard(deck.draw());
                        turnOver = false;
                    } else {
                        this._dialogOut.write(this._players[i].name + ": stays");
                        turnOver = true;
                    }
                } else if(confirm(this._players[i].name + " hit?")) {
                    this._dialogOut.write(this._players[i].name + ": hits");
                    this._players[i].hand.drawCard(deck.draw());
                    turnOver = false;
                } else {
                    this._dialogOut.write(this._players[i].name + ": stays");
                    turnOver = true;
                }
            } while(!turnOver);
        }
        
    }
    
}