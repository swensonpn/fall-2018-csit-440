import { Player } from "../card_game";

export class BlackjackDealer extends Player {
    constructor() {
        super("Dealer");
    }
    
    hit() {
        return this.hand.score() < 17;
    }
}