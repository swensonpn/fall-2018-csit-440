import { Out } from "./out";
import { Player } from "../players";

export class PlayerOut extends Out {

    write(player) {
        if(player instanceof Player) {
            console.log(player.toString());
        }
    }
}