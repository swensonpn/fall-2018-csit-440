import { Card } from "./card";

function generateDeckNoJokers() {
    let deck = [];
    
    for(let i = 0; i < 4; i++) {//loop through four suits
        for(let j = 1; j < 14; j++) {
            const card = new Card(i, j);//created a new card instance
        
            deck.push(card);
        }
    }
    
    return deck;
}

export class Deck {
    constructor(options = {}) {
        
        switch(options.type) {
            
        default:
            this._deck = generateDeckNoJokers();
        }
    }
    
    get deck() {
        return this._deck.slice();
    }
    
    draw() {
        return this._deck.pop();
    }
    
    shuffle() {
        let shuffledDeck = [];
        
        while(this._deck.length > 0) {
            const index = Math.floor(Math.random() * this._deck.length);
            shuffledDeck.push(this._deck.splice(index,1)[0]);
        }
        
        this._deck = shuffledDeck;
    }
}