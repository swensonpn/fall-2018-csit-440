const names = ["♧", "♢", "♡", "♤"];


export class Card {
    constructor(suit, value) {
        //validate suit
        suit = parseInt(suit, 10);
        if(suit < 0 || suit > 3) throw new Error("Suit must be an integer between 0 and 3");
        
        //validate value
        value = parseInt(value, 10);
        if(value < 1 || value > 13) throw new Error("Value must be an integer between 1 and 13");
        
        this._suit = suit;
        this._value = value;
    } 
    
    get suit() {
        return this._suit;
    }
    
    get value() {
        return this._value;
    }
    
    static get CLUB() {
        return 0;
    }
    
    static get DIAMOND() {
        return 1;
    }
    
    static get HEART() {
        return 2;
    }
    
    static get SPADE() {
        return 3;
    }
    
    getSuitName() {
        return names[this._suit];
    }
    
    getValueDescription() {
        switch(this._value) {
            case 1:
                return "ace";
            case 11:
                return "jack";
            case 12:
                return "queen";
            case 13:
                return "king";
            default:
                return this.value;
        }
    }
    
    toString() {
        return `${this.getValueDescription()} ${this.getSuitName()}`;
    }
}