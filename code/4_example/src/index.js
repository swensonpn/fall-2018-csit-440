// import style from "./index.css";
// import { Player } from "./card_game";
// import { BlackjackGame } from "./blackjack";

// const game = new BlackjackGame({
//     players: [
//         new Player("Bob"),
//         new Player("Sally"),
//         new Player("Ronald")
//     ]
// });

// game.start();

import style from "./index.css";
import { BlackjackGame } from "./blackjack";
import { Player } from "./card_game";

const game = new BlackjackGame({
    players: [
        new Player("Player 1"),
        new Player("Player 2"),
        new Player("Player 3"),
        new Player("Player 4"),
        new Player("Player 5"),
    ]
});

game.start();