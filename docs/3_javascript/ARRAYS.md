# Arrays
Arrays are a special data type that are used to hold multiple values.  In JavaScript
arrays do not have to be a single data type and the length is not fixed.  Arrays
are indexed from zero to array length minus one.

## References
- [MDN - Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)
- [W3 Schools - Array](https://www.w3schools.com/js/js_arrays.asp)

## Declaring
Arrays can be declared empty or with values. 
```javascript
//using the new operator
let arrayOne = new Array();
let arrayTwo = new Array(false, 1, "two");

//using short hand notation
let arrayThree = [];
let arrayFour = [false, 1, "two"];

//inspecting the array
console.log(arrayTwo);
console.log(arrayFour);
```

## Getting and Setting Values
To get or set a value from an array square bracket notation is used.
```javascript
//get a value
let valueOne = arrayFour[1];//notice this is not the first item
console.log(valueOne);

//when a non-existing value is requested there is no error.
console.log(arrayFour[5]);

//set a value
arrayFour[5] = new Array("hello", "world");//notice this changes the size
console.log(arrayFour);
```

## Iterating Through an Array
The for loop is the best loop to use for arrays
```javascript
for(let i = 0; i < arrayFour.length; i++) {
  console.log(i + " is " + arrayFour[i]);
}
```

## Array Properties
> ### Length
> Returns the number of elements in the array.  Note that gaps in the array are
  treated as values.
  ```javascript
  console.log(arrayOne.length);//returns 0
  console.log(arrayTwo.length);//returns 3
  console.log(arrayFour.length);//returns 6
  ```

## Array Methods
> ### Join and Split
> Returns the array as a delimited string.  To get an array from a string we use split
  ```javascript
  let commaDelimitedString = arrayFour.join();
  console.log(commaDelimitedString);
  
  let customDelimitedString = ["valueOne","valueTwo","valueThree"].join("<-->");
  console.log(customDelimitedString);
  
  console.log(commaDelimitedString.split());
  console.log(customDelimitedString.split("<-->"));
  ```

> ### Slice
> Return a new array from part or all of an existing array
  ```javascript
  //clone the whole array
  let newArrayOne = arrayFour.slice();
  console.log(newArrayOne);//returns a deep copy of arrayFour
  
  //create a new array starting at an indexed
  let newArrayTwo = arrayFour.slice(2);
  console.log(newArrayTwo);//returns array with items 2 - 5 of arrayFour
  
  //create a new array starting at an index (inclusive) and ending at an index (exclusive)
  let newArrayThree = arrayFour.slice(1,3);
  console.log(newArrayThree);//returns array with items 1 - 2 of arrayFour
  ```
  
> ### Splice  
> Extract and/or insert values into an array.  The first argument position is 
  required, but all others are optional.
  ```javascript
  //array.splice(position, numberOfItemsToRemove, newItemOne, newItemTwo, newItemN);
  
  //remove items from an array
  let spliceArrayOne = arrayFour.splice(3, 2);
  console.log(spliceArrayOne);
  console.log(arrayFour);
  
  //add items into array
  let spliceArrayTwo = arrayFour.splice(1, 0, "Hello", "World");
  console.log(spliceArrayTwo);
  console.log(arrayFour);
  
  //remove and add items at the same time
  let spliceArrayThree = arrayFour.splice(1, 2, "good", "bye");
  console.log(spliceArrayThree);
  console.log(arrayFour);
  ```
  
> ### Push, Pop, Shift, and Unshift
> These methods are used to add and remove items from either end of an array.
  If we imagine  an array is a stack of plates at the cafeteria, then to pop or
  push would be to take a plate from the top or place the plate back on the stack.
  To shift or unshift would be to remove a plate from the bottom of the stack or
  slide a new plate into the bottom.
  ```javascript
  let arrayFive = [0,1,2,3,4,5];
  
  //take 5 off the end of the array and log that item
  console.log(arrayFive.pop());
  console.log(arrayFive);
  
  //add a new item to the end
  arrayFive.push("new item five");
  console.log(arrayFive);
  
  //take item 0 off the beginning of the array and log it
  console.log(arrayFive.shift());
  console.log(arrayFive);
  
  //add a new item to the beginning
  arrayFive.unshift("new item zero");
  console.log(arrayFive);
  ```
  
> ### Reverse and Sort
> These methods change the order of items in an array.
  ```javascript
  //reverse just flips the array
  let arraySix = [0, 1, 2, 3];
  arraySix.reverse();
  console.log(arraySix);
  
  //sort called without a parameter
  arraySix.sort();
  console.log(arraySix);
  
  //sort can be called with a function as an argument for complex sorts
  arraySix.sort(function(a, b) {
      return a%2 > b%b;
  });
  console.log(arraySix);//even items before odd ones
  ```
  
> ### IndexOf and Find
> Both of these methods search for a single item in an array.  IndexOf is faster
  and easier to use, but find is more powerful
  ```javascript
  let arraySeven = [0,1,2,"three",4,5];
  
  function isString(item) {
    return typeof item === "string";//return true if match false if not
  }
  
  //indexOf searches for the index of an item in an array
  console.log(arraySeven.indexOf("three"));//returns 3
  console.log(arraySeven.indexOf("four"));//returns -1 if item is not in the array
  
  //find accepts a function as an argument and returns the first matched item in the array
  let arrayFindMatchOne = arraySeven.find(isString);
  console.log(arrayFindMatchOne);
  
  //find returns undefined if no match is found
  let arrayFindMatchTwo = [0,1,2].find(isString);
  console.log(arrayFindMatchTwo);
  ```

> ### Filter
  Filter is like find except that instead of returning a single match it returns
  all matches
  ```javascript
  function isNumber(item) {
    return typeof item === "number";//return a boolean
  }
  
  //pass a filtering function as an argument
  let arrayFilterMatchesOne = arraySeven.filter(isNumber);
  console.log(arrayFilterMatchesOne);
  
  //returns an empty array if no matches are found
  let arrayFilterMatchTwo = ["hello","world"].filter(isNumber);
  console.log(arrayFilterMatchTwo);
  ```
  
> ### ForEach and Map
> These two methods iterate through the array executing a function for each item.
  The difference between them is that map will create a new array based on return
  values from the function;
  ```javascript
  let arrayEight = [5, 10, 15, 20];
  
  //argument function receives three parameters. item(the current array item)
  //index(the index of item) array(the array itself)
  arrayEight.forEach(function(item, index, array) {
    if(index%2 === 0) {
      array[index] = item + " px";
    }
  });
  console.log(arrayEight);
  
  let arrayMapResult = arrayEight.map(function(item, index, array) {
    return "index " + index + " is " + item;
  });
  console.log(arrayMapResult);//resulting array
  console.log(arrayEight);//original array
  ```