# Data Types

## Simple Data Types
JavaScript has only three primitive data types.

> ### Boolean
> true or false.  
  ```javascript
  let myBoolean = false;
  console.log(typeof myBoolean === "boolean");
  myBoolean = true;
  console.log(typeof myBoolean === "boolean");
  ```
> ### Number
> Any numeric value.  Can be an integer or decimal value.
  ```javascript
  const myInteger = 5;
  console.log(typeof myInteger === "number");
  const myFloat = 3.14;
  console.log(typeof myFloat === "number");
  ```
  ```javascript
  //special number values
  const infinity = Infinity;//number too big to represent
  const notANumber = NaN;//value is not a number
  ```
> ### String
> String literals are enclosed in single quotes, double quotes, or backticks.
  ```javascript
  const myDoubleQuoteString = "Hello World. I'm a string";
  console.log(typeof myDoubleQuoteString === "string");
  ```
  ```javascript
  const mySingleQuoteString = 'Quote "Hello World"';
  console.log(typeof mySingleQuoteString === "string");
  ```
  ```javascript
  //strings enclosed in backticks can be expressed on multiple lines.
  const myBacktickString = `
    Hello
        World.
    How are
        You?
  `;
  console.log(typeof myBacktickString === "string");
  ```
  ```javascript
  //When using the enclosing character in the text it must be escaped with a backslash
  const myEscapeDoubleQuote = "Quote \"Hello World\"";
  const myEscapeSingleQuote = 'Hello World. I\'m a string';
  const myEscapeBacktick = `Hello World\``;
  ```
  
## ```null``` and ```undefined```
JavaScript has two special values to indicate no value.
> ### ```undefined```
> Indicates that a variable has been declare, but no value has been assigned.
  ```javascript
  let myUndefinedValue;
  console.log(myUndefinedValue);
  myUndefinedValue = 12;
  console.log(myUndefinedValue);
  ```
> ### ```null```
> Value has been explicitly set to no value.
  ```javascript
  let myNullValue = 12;
  console.log(myNullValue);
  myNullValue = null;
  console.log(myNullValue);
  ```
  
## Implicit Data Type Conversion
JavaScript is a [loosly typed](https://en.wikipedia.org/wiki/Strong_and_weak_typing)
programming language.  This means that a declared variable can change from one
data type to another.  This can often be a convenient feature of the language, 
but can lead to some interesting edge cases.
> ### Conversion to String
> - A number passed to a function that accepts string, the number will become a
    string.
> - Connecting a number to a string with '+' will convert the number to a string.
> - Infinity is converted to "Infinity".
> - NaN is converted to "NaN".
> - null is converted to "null".
> - undefined = is converted to "undefined".
> - Booleans are converted to "true" or "false".
> ### Conversion to Boolean
> - A number that has a value of 0 or NaN will convert to false, all other values become true.
> - An empty string will be converted to false, all other values become true.
> - null is false.
> - undefined is false.
> ### Conversion to Number
> - Boolean value true converts to 1 and false converts to 0.
> - Strings must be explicitly converted using ```parseInt``` or ```parseFloat```.
```javascript
const myParsedInteger = parseInt("19.2 inch");
console.log(myParsedInteger);//returns 19
```
```javascript
const myParsedFloat = parseFloat("19.2 inch");
console.log(myParsedFloat);//returns 19.2
```
```javascript
const myParsedNotANumber = parseInt("inch 19.2");
console.log(myParsedNotANumber);//returns NaN
```