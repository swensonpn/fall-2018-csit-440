# Conditional Statements
A [conditional statement](https://en.wikipedia.org/wiki/Conditional_(computer_programming))
performs different actions depending on whether some expression specified by the 
programmer evaluates to true or false.

## Coalesce Statement
Use of the or operator can be used to select the first non-null/non-undefined value.
Good for setting a default value if an argument is not provided.
```javascript
function coalesce(value) {
  return value || "default value";
}
console.log(coalesce(12));//returns 12
console.log(coalesce());//returns default value
```
 
## Ternary Statements
Performs one of two actions based on an expression.  Useful when assigning one
value or another based on some condition.
```javascript
function ternary(condition) {
  return (condition) ? "red" : "blue";
}
console.log(ternary(true));//returns red
console.log(ternary(false));//returns blue
```
## If Statement
Execute some code if the provided expression is true.
```javascript
const SKY_COLOR = "blue";
if(SKY_COLOR === "blue" ) {
  console.log("The sky is blue");
}
```
## If-Else Statement
The same as the if statement, but allows the developer to execute different code
if the expression evaluated to false.  This conditional statement also introduces
```else if``` which allows the statement to evaluate multiple if checks.
```javascript
//basic if else
const trueOrFalse = (Math.random()  < .5) ? true : false;
if(trueOrFalse) {
  console.log("trueOrFalse was true");
} else {
  console.log("trueOrFalse was false");  
}
```
```javascript
//multiple if statements.  Only one will execute
const favorite = Math.floor(Math.random() * 6);

if(favorite === 0) {
  console.log("red");
} else if(favorite === 1) {
  console.log("blue");  
} else if(favorite === 2) {
  console.log("yellow");  
} else if(favorite === 3) {
  console.log("green");  
} else if(favorite === 4) {
  console.log("purple");  
} else {
  console.log("orange");  
}
```

> ### Switch Statement
> Switch statements are similar to if-else statements, but evaluate a single
  expression executing the case that matches that value.
  ```javascript
  const colorsArray = ["red","blue","yellow","green","purple","orange"];
  const color = colorsArray[Math.floor(Math.random() * 6)];
  
  switch(color) {
    case "red":
      console.log(color + " cherry");
      break;
    case "blue":
      console.log(color + " berry");  
      break;
    case "yellow":
      console.log(color + " banana");  
      break;
    case "green":
      console.log(color + " apple");  
      break;
    case "purple":
      console.log(color + " plum");  
      break;
    case "orange":
      console.log(color + " orange");  
      break;
    default:
      console.log("can't find fruit");
  }
  ```
  