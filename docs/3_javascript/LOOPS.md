# Loops
In computer programming, a loop is a sequence of instruction s that is continually 
repeated until a certain condition is reached.[WhatIs.com](https://whatis.techtarget.com/definition/loop)

## While Loop
```While``` loops execute a block of instruction until a condition is no longer met.
```javascript
let whileKeepGoing = true;
while (whileKeepGoing) {
    let whileKeepGoing = (Math.random()  < .5) ? true : false;
    
    console.log("while loop whileKeepGoing: " + whileKeepGoing);
}
```

## Do While Loop
The ```do while``` loop behaves the same as the while loop except that the code block
is always executed at least once.
```javascript
let doWhileKeepGoing = false;
do {
    console.log("doWhileKeepGoing is false but executed once anyway");
} while(doWhileKeepGoing);
```
## For Loop
A ```for``` loop executes the block of instructions a fixed number of times.  This type
of loop is good for iterating through arrays.
```javascript
//the for loop has 3 statements
// statement 1: declare/set an index
// statement 2: the expression to test (like while loop)
// statement 3: increment/decrement the index
for(let i = 0; i < 5; i++) {
    console.log("for loop index: " + i);
}
```

## For In Loop
The ```for in``` loop is best used for iterating through the properties of an
object.
```javascript
let person = {
  name:"Roger",
  age:17,
  favoriteFood:"Pizza"
};

for(let n in person) {
    console.log("for in person " + n + ": " + person[n]);
}
```

## Break Statement
A ```break``` statement allows the programmer to exit a loop before the condition is
no longer met.
```javascript
let breakArray = ["bill","bob","brown"];

for(let i = 0; i < breakArray.length; i++) {
    if(breakArray[i] === "bob") {
        console.log("found bob");
        break;
    } else {
        console.log("did not find bob");
    }
}
```

## Continue Statement
The ```break``` statement allows the programmer to cease execution of the block
and continue to the next iteration of the loop.
```javascript
let continueArray = ["Sally","sherri","Sissy"];
for(let i = 0; i < continueArray.length; i++) {
    if(continueArray[i].charAt(0) === continueArray[i].charAt(0).toUpperCase()) {
        continue;
    }
    
    console.log(continueArray[i] + " needs to be capitilized")
}
```