# Activity 2
Write a program that reads the data in a csv file and writes that data to a json
file.  The source file is called cars.csv and is found in the class workspace
resources folder.  Each row in the file represents one car, and each value separated
by a comma is one property of that car.  The first row in the file is the property
names and the second row is the data type that column should be assigned to when
processing.

## Due Thursday September 20th, 2018 (end of class)

## Requirements
1. Project Setup
    - Create a folder called "activity2" in your private workspace.
    - Copy the cars.csv file from the class workspace into the activity2 folder.
    - Create a new NPM project
        - name: "activity2"
        - author: your name
        - scripts: "activity" - runs the file src/index.js with node
    - Create a folder called "src" with one file "index.js". Write your
      code in this file.
2. Project Code
    - **Functions:**
        - **csvToTwoDimensionalArray**: transform the file data from a string to
          a two dimensional array.
            - argument[0]: the contents of the cars.csv file as a string
            - return: a two dimensional array where the first dimension is the
              rows in the file, and the second dimension is the string values between
              the commas.
       
        - **arrayOfArraysOfStringToArrayOfObject**: change the data so that the 
          data representation of each car is an object instead of an array.
            - argument[0]: the two dimensional array produced by csvToTwoDimensionalArray.
            - return: a new array of car objects.
        
        - **castPropertyValue**: casts a single property to the correct data type. 
          This function should be called inside arrayOfArraysOfStringToArrayOfObject.
            - argument[0]: the data type from the second row in the cars.csv file.
            - argument[1]: the value to be converted.
            - return: argument[1] cast to the data type specified by argument[0].
    - **Main Program:** This program should read the cars.csv file using fs.readFile(). 
      Once the file has been read an array of car objects should be produced using
      the functions above.  Save the result into a new file called cars.json using
      fs.writeFile().
      
## Acceptance Criteria
- No incomplete/empty car objects should be in the cars.json file.
- The two heading rows should not be in the cars.json file.
- Each property of a car should be cast as follows:
    - STRING: String
    - DOUBLE: Number parsed as a float
    - INT: Number parsed as an integer
- NaN, null, and undefined must not appear in cars.json
- Car property names must be lower case with no spaces.
- Each of the functions described above tests and/or defaults argument values to
  ensure the program will not break expectantly.
- The cars.json file produced by the program must pass validation using [JSONLint](https://jsonlint.com/)