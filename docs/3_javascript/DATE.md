# Dates

## References
- [W3 Schools - Dates](https://www.w3schools.com/jsref/jsref_obj_date.asp)

## Declare Dates
The date object in JavaScript must be declared with the ```new``` keyword.
Be aware when refering to hours, minutes, seconds, milliseconds, months, and days
value start from zero not one. Also, if the date is simply logged to the console 
the output will be adjusted by timezone.


```javascript
//create date object for the current time
let currentDate = new Date();
console.log(currentDate);

//create from ISO 8601 (YYYY-MM-DD)
let mayOne2018 = new Date("2018-05-01");
console.log(mayOne2018);//prints Mon Apr 30 2018 19:00:00 GMT-0500 (Central Daylight Time)

//create passing in values
let julyFour2015 = new Date(2015, 06, 03, 0);
console.log(julyFour2015);//prints Fri Jul 03 2015 00:00:00 GMT-0500 (Central Daylight Time)
```

## Date Methods
> ### now
> The now method returns the number of milliseconds since midnight Jan 1 1970. 
  This method is good when a pseudo random number is needed or when trying to 
  measure how long something took in the code.

```javascript
const start = Date.now();
console.log(start);//prints an integer

setTimeout(function() {
    console.log(Date.now() - start);//prints the number of milliseconds since start
}, 2000);
```
> ### toISOString()
> Returns a valid ISO 8601 date string from the date object.  This is recommended
  format for sending dates over the internet.
  
```javascript
let originalISODate = "2018-06-15";
let parsedDate = new Date(originalISODate);
console.log(parsedDate);//prints Thu Jun 14 2018 19:00:00 GMT-0500 (Central Daylight Time)

let newISODate = parsedDate.toISOString();
console.log(newISODate);//prints 2018-06-15T00:00:00.000Z
```