# Strings
All three primitive data types have a corresponding object wrappers that provide
useful methods and properties.  The String object is particularly useful.

## References
- [W3 Schools - Strings](https://www.w3schools.com/jsref/jsref_obj_string.asp)
 
## Declaring Strings
Strings can be created as primitives or with the ```new``` keyword
```javascript
//declare as a primitive
let stringTwo = "Hello String Two";
let stringThree = 'Hello String Three';
let stringFour = `Hello String Four`;

//create with the new keyword
let stringOne = new String("Hello String One");
```

## String Literals

```javascript

```

## String Properties
> ### length
> returns the number of characters in a string
```javascript
const stringOneLength = stringOne.length;
console.log(stringOneLength);
```

## String Methods
> ### Transformational Methods
> The list of methods for transforming text is declining.  Methods such as bold, 
  italics, big, small, etc are either no longer supported or deprecated due to 
  their presentational purpose.  Developers should prefer CSS to alter the look 
  of text in a web page, and use HTML when the primary concern is to convey meaning. 

```javascript
//upper case and lower case
const upperCase = stringOne.toUpperCase();
console.log(upperCase);//prints HELLO STRING ONE
const lowerCase = stringOne.toLowerCase();
console.log(lowerCase);//prints hello string one
```

> ### Searching for One String in Another Methods
There are many methods/techniques for determining if one string contains another.

```javascript
//String.indexOf(searchString)
//returns the position of the first occurrence on searchString or -1 if not found
const stringHasString = stringOne.indexOf("One");
console.log("position: " + stringHasString);//prints position: 13
const stringDoesNotHaveString = stringOne.indexOf("Two");
console.log("position: " + stringDoesNotHaveString);//prints position: -1

//test if subject string contains needle
if("subject string").indexOf("string") > -1) {
    console.log("subject string contains search string");
} else {
    console.log("subject string does not contain search string");
}
```
```javascript
//String.lastIndexOf(searchString)
//returns the position of the last occurance of searchString or -1 if not found
let url = "https://www.unk.edu/departments/csit/contact.html";
let fileNameStarts = url.lastIndexOf("/");
console.log(url.substr(fileNameStarts + 1));//prints "contact.html"
```
```javascript
//String.includes(searchString)
//returns true if search string is contained in the subject or false if it is not
const includesSubjectString = "Hello World";
console.log("Includes: " + stringOne.includes("Hello"));//prints true
```
```javascript
//String.startsWith(searchString)
//returns true if string starts with searchString
const startString = "Hello World";
console.log(startString.startsWith("He"));//prints true
```
```javascript
//String.endsWith(searchString)
//returns true if string ends with searchString
const endString = "Hello World";
console.log(endString.endsWith("Wor"));//prints false
```

> ### Change String Methods
> Methods/techniques for changing the characters in a string.

```javascript
//String.replace(findCharSequence, replaceCharSequence)
//finds occurances of findCharSequence and replaces them with replaceCharSequence
let originalReplaceString = "Bobby pulls his dog red around in a red wagon";
console.log(originalReplaceString.replace("red","blue"));//"Bobby pulls his dog blue around in a blue wagon"
console.log(originalReplaceString);//original string did not change
```
```javascript
//String.trim()
//removes whitespace from either end of a string
const spacePaddedString = "  ART";
console.log("|" + spacePaddedString + "|");//prints |  ART| 
const spacePaddedRemovedString = spacePaddedString.trim();
console.log("|" + spacePaddedRemovedString + "|");//prints |ART|
//there is no option to trim non-white space characters
const zeroPaddedString = "000252";
console.log(zeroPaddedString.replace(/^0*/g, ''));

```
```javascript
//there is no method for padding a string either
const spacePaddingReplaced = " ".repeat(1) + spacePaddedString;
console.log("|" + spacePaddingReplaced + "|");//prints |  ART| 
```

> ### Extracting Values
> JavaScript has three methods for getting a substring.

```javascript
//String.substr(start, length)
//returns a new string lenght characters long and starting at start (inclusive) 
const substrStringOneArg = stringOne.substr(3);
console.log(substrStringOneArg);//prints "lo String One
const substrStringTwoArg = stringOne.substr(3, 2);
console.log(substrStringTwoArg);//prints "lo"
```
```javascript
//String.substring(start, end)
//returns a new string starting at start (inclusive) and ending at end (exclusive)
const substringStringOneArg = stringOne.substring(3);
console.log(substringStringOneArg);//prints "lo String One
const substringStringTwoArg = stringOne.substr(0, stringOne.indexOf(" "));
console.log(substringStringTwoArg);//prints "Hello"
```
```javascript
//String.slice(start, end)
//works like substring except that the end parameter can be negative to specify a position from the end of the string
const sliceStringOneArg = stringOne.slice(3);
console.log(sliceStringOneArg);//prints "lo String One
const sliceStringTwoArg = stringOne.slice(3, -4);
console.log(sliceStringTwoArg);//prints "lo String"
```