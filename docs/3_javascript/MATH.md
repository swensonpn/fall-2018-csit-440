# Math
The Math object provides many static methods for for manipulating numbers.

## References
- [W3 Schools - Math Object](https://www.w3schools.com/js/js_math.asp)
- [MDN - Math](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math)

## Math Properties
> ### PI
> The ratio of the circumference of a circle to its diameter, approximately 3.14159.

```javascript
console.log(Math.PI);
```

## Math Methods
> ### Math.abs(x)
  Returns the absolute value of x
  
```javascript
console.log(Math.abs(-1));//prints 1
```

> ### Math.round(x)
  Returns the value of x rounded to the nearest integer
  
```javascript
console.log(Math.round(1.5));//prints 2
```

> ### Math.ceil(x)
  Returns the value of x rounded up to the nearest integer
  
```javascript
console.log(Math.ceil(1.1));
```

> ### Math.floor(x)
  Returns the value of x rounded down to the nearest integer
  
```javascript
console.log(Math.floor(1.1));
```

> ### Math.random()
  Returns a value between 0 (inclusive) and 1 (exclusive)
  
```javascript
//basic use
for(let i = 0; i < 10; i++) {
    console.log(Math.random());
}
```
```javascript
//coin flip
for(let i = 0; i < 5; i++) {
    let side = (Math.floor(Math.random() * 2)) ? "heads" : "tails";
    console.log(side);
}
```
```javascript
//roll the dice
for(let i = 0; i < 5; i++) {
    let diceOne = Math.ceil(Math.random() * 6);
    let diceTwo = Math.ceil(Math.random() * 6);
    
    console.log("one:" + diceOne + ", two:" + diceTwo + ", total:" + (diceOne + diceTwo));
}
```
```javascript
//shuffle cards
let diamonds = ["2d", "3d", "4d", "5d", "6d", "7d", "8d", "9d", "10d", "Jd", "Qd", "Kd", "Ad"];
let hearts = ["2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "Jh", "Qh", "Kh", "Ah"];
let spades = ["2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "Js", "Qs", "Ks", "As"];
let clubs = ["2c", "3c", "4c", "5c", "6c", "7c", "8c", "9c", "10c", "Jc", "Qc", "Kc", "Ac"];

function dealNewHand(numPlayers = 2, cards = 7) {
    let deck = diamonds.concat(hearts, spades, clubs);
    let players = [];
  
    for(let i = 0; i < cards; i++) {
        for(let j = 0; j < numPlayers; j++) {
            let cardIndex = (Math.floor(Math.random() * deck.length));
            
            if(players[j] === undefined) {
                players[j] = [];
            }
            
            players[j].push(deck.splice(cardIndex, 1)[0]);
        }
    }
    
    return {deck: deck, hands: players};
}
let {deck, hands} = dealNewHand();
console.log(deck);
console.log(hands[0]);
console.log(hands[1]);
```

