# Functions
A function is a set of statements that perform a specific task.  Extracting code 
into a function allows the programmer to reuse the logic over and over.  

## References
- [W3 Schools - JavaScript Functions](https://www.w3schools.com/js/js_functions.asp)
- [MDN - Functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions)

## Terminology
- ```function``` is the keyword used in the declaration of a function
- arguments are the values passed to a function when called.
- parameters are placeholders in a function declaration for the arguments which 
  are expected to be passed when the function is called.
- ```return``` is the optional keyword used to pass values back from a function.
- annonymous is a function which is not named.
- how arguments are passed into a function
    - pass by value means that a variable is not passed onto a function, but instead
      the value that variable holds is.  Primitive data types are passed by value.
    - pass by reference means that a variables position in memory is passed into
      the function.  As a result any change to that variable in the function will
      also change the variable outside of the function.  Non primitive data types
      are passed by reference.


## Declaring a Function
Functions can be declared or created as an expresssion.

> ### Declaritive Functions
> The most basic way of creating a function is by using a fuction declaration.
  Declarations start with the keyword ```function``` followed by the name.  After the name a 
  set of parentheses holding any number of parameters separated by commas.  Finally,
  a set of curly braces which contains the statements which will be executed when
  called.
```javascript
function myFunctionNameOne(parameterOne, parameterTwo, parameterN) {
    //code to be executed
    return parameterOne + parameterTwo;//return statement is optional
}
```

> ### Functions as Expressions
> Functions can also be created as expressions.  In this case the function is typically
  assigned to a variable or declared inside the call to another function.  This
  type of function is often referred to as annonymous since they are usually not
  named.
```javascript
//assigning to a variable
const myFunctionTwo = function(parameterOne) {
    console.log("do something annonymous");
};
```
```javascript
//assigning to a variable using arrow function notation (ES6)
const myFunctionThree = (a, b) => {
    return a + b;
};
```
```javascript
//declaring in a call to another function.
myFunctionTwo(function() {
    console.log("executed somewhere in myFunctionTwo");
});
```

## Nesting Functions
In the variable scope section the concept of hoisting was raised.  In JavaScript
it is common to have functions inside functions creating different levels of scope.
Remember that inner functions can access the variables of outer functions, but not
the other way around.
```javascript
//global variable
let global = "I am global";

//outer most function
function outer() {
    let localToOuter = "I am local to outer";
    
    function inner() {
        let localToInner = "I am local to inner";
        console.log(global);
        console.log(localToOuter);
        console.log(localToInner);
    }
    
    console.log(inner());//only accessible inside outers closure
}
try {
    inner();//throws error
} catch(err) {console.error("can't call inner from global scope");
outer();//its okay to call outer, which can call inner
```

## Calling a Function
Functions are called the same way regardless of whether they are declaritive or
expressions. A function can also be called at the same time its declared.  This 
is known as a self-executing function.
```javascript
//calling a declaritive function
function addTwoNumbers(a, b) {
    return a + b;
}
console.log(addTwoNumbers(5, 10);//prints 15
```
```javascript
//calling an expression function
const remainderOfTwoNumbers = function(a, b) {
    return a % b;  
};
console.log(remainderOfTwoNumbers(2, 1));//prints 0
```
```javascript
//calling an expression function passed into a function
function doCallback(callbackFunction) {
    callbackFunction();
}
console.log(doCallback(function(){console.log("Hello");}));//prints "Hello"
```
```javascript
//calling a function at the same time its declared
const resultOfAMinusB = ((a, b) => {return a - b})(3, 2);
console.log(resultOfAMinusB);//prints 1
```

## Handling Returns
Returning values from a function is optional.  Its also, optional to save returned
values into a variable.  When a function returns no value undefined is the return
value.
```javascript
//no return value
function noReturn() {}
console.log(noReturn());//prints undefined
```
```javascript
//return with no value
function undefinedReturn(){return;}
console.log(undefinedReturn());//prints undefined
```
```javascript
//return a value
function simpleReturn(){return "simple";}
console.log(simpleReturn());//prints "simple"
```
```javascript
//more than one possible return
function oneReturnOrAnother(){
    if(Math.random() < .5) {
        return "less than";
    } else {
        return "not less than";
    }
}
console.log(oneReturnOrAnother);//prints "less than" or "not less than"
```
```javascript
//returning more than one value from a function
function multipleValuesInAReturn() {
    return {color:"red", shape:"sphere"};
}

//get as two variables color and shape
let { color, shape } = multipleValuesInAReturn();
console.log(color);//prints "red"
console.log(shape);//prints "sphere"

//capture the object and use its values
let objectOne = multipleValuesInAReturn();
console.log(objectOne);//prints {color:"red", shape:"sphere"}
console.log(objectOne.color);//prints "red"
console.log(objectOne.shape);//prints "sphere"
```
```javascript
//ignoring a return value is also allowed
multipleValuesInAReturn();
```

## Required and Optional Parameters
One of the challenges to programming in JavaScript is that its not possible to 
declaratively require parameters.  A function can be declared with zero to n
parameters, but the caller can pass in any number of arguments they want.  As a 
result the function writer is responsible for checking the value of arguments
acually passed in.

> ### Required Parameter
> To make a parameter required the function needs to test the value passed meets
  the requirements to execute the function.  If it does not the function should
  stop execution.
```javascript
//stop execution by return
function setAttribute(key, value) {//key is to be a required string
    if(typeof key !== "string") return;//if key is not passed it will be undefined
    //code to execute if arguments meet validation    
}
```
```javascript
//stop execution by throwing an error
function requiredCallback(callbackFunction) {
    if(typeof callbackFunction !== "function") throw new Error("argument zero must be a function");
    //code to execute if arguments meet validation    
}
```

> ### Optional Parameters
> Some parameters may be optional.  In these cases the function should either used
  a default value or simply ignore missing values.
```javascript
//Use coalesce to set a default value (good when type test not needed)
function defaulByCoalesce(optionalParameter) {
    optionalParameter = optionalParameter || "default value";
}
```
```javascript
//Use ternary conditional to set a default value (good when type test is needed)
function defaulByTernary(optionalParameter) {
    optionalParameter = (typeof optionalParameter === "string") ? optionalParameter : "default value";
}
```
```javascript
//Use ES5 parameter declartion defaults (good when type test not needed)
function defaulByTernary(optionalParameter = "default value") {
    //optionalParameter is "default value" when an argument is not provided    
}
```
```javascript
//common technique for accepting optional configuration
function setOptions(options = {}) {
    const configuration = {
      debugMode:false,
      maxHeight:50,
      minHeight:10
    };
    
    for(let n in configuration) {
        if(options[n] !== undefined) {
            configuration[n] = options[n];
        }
    }
}
```