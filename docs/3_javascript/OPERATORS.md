# Operators

## Arithmetic Operators
- ```+```: Addition 
```javascript 
console.log("addition: " + (1+1));//addition: 2
console.log("concatonation: " + ("Hello" + " " + "World"));//concatonation: 2
```
- ```-```: Subtraction
```javascript 
console.log("subtraction: " + (1-1));//subtraction: 0
```
- ```*```: Multiplication
```javascript 
console.log("multiplication: " + (1*1));//multiplication: 1
```
- ```/```: Division
```javascript 
console.log("division: " + (2/1));//division: 2
```
- ```%```: Modulus (remainder)
```javascript 
console.log("modulus: " + (2%1));//modulus: 0
```

## Unary Operators
- ```-```: Negation eqivilant to -1 * value
```javascript 
let positiveValue = 1
console.log("Negated: " + (- positiveValue));//negated: -1
```
- ```++```: Increment equivilant to value + 1
```javascript 
let incrementedValue = 1
console.log("incremented: " + (incrementedValue++));//incremented: 2
```
- ```--```: Decrement eqivilant to value - 1
```javascript 
let decrementedValue = 1
console.log("decremented: " + (decrementedValue--));//decremented: 0
```

## Assignment Operators
- ```=```: Set value equal to
 ```javascript 
const assignedValue = 10;
console.log("assigned: " + assignedValue);//assigned: 10
```
- ```+=```: Add value to current value
 ```javascript 
let addToAssigned = 10;
addToAssigned += 10;
console.log("add to value: " + addToAssigned);//assigned: 20
addToAssigned += " more days";
console.log("concatonate to value: " + addToAssigned);//concatonate: 20 more days
```
- ```-=```: Subtract value from current value
 ```javascript 
let subtractFromAssigned = 10;
subtractFromAssigned -= 10;
console.log("subtract from value: " + subtractFromAssigned);//subtract from value: 0
```
- ```*=```: Multiply current value by new value
 ```javascript 
let multiplyByAssigned = 10;
multiplyByAssigned *= 10;
console.log("multiply by value: " + multiplyByAssigned);//multiply by value: 100
```
- ```/=```: Divide value by new value
 ```javascript 
let divideByAssigned = 10;
multiplyByAssigned /= 10;
console.log("divide by value: " + divideByAssigned);//divide by value: 1
```
- ```%=```: Remainder of divide value by new value
 ```javascript 
let remainderDivideByAssigned = 10;
remainderDivideByAssigned /= 10;
console.log("remainder divide by value: " + remainderDivideByAssigned);//remainder divide by value: 0
```

## Relational Operators
- ```>```: Greater than
```javascript
console.log("1 is greater than 0: " + (1>0));
```
- ```<```: Less than
```javascript
console.log("1 is less than 2: " + (1<2));
```
- ```>=```: Greater than or equal to
```javascript
console.log("1 is greater than or equal to 1: " + (1>=1));
```
- ```<=```: Less than or equal to
```javascript
console.log("1 is less than or equal to 1: " + (1<=1));
```
- ```==```: Equal in value
```javascript
console.log("null is equal in value to undefined: " + (null == undefined));
```
- ```===```: Equal in value and equal in type
```javascript
console.log("null is not equal in value and type to undefined: " + (null === undefined));
```
- ```!=```: Not equal in value
```javascript
console.log("\"true\" is equal to true in value: " + ("true" != true));
```
- ```!==```: Not equal in value and type
```javascript
console.log("\"true\" is equal to true in value and type: " + ("true" !== true));
```

## Logical Operators
- ```&&```: And
```javascript
console.log("1 is equal to 1 and 2 is equal to 2: " + (1===1 && 2===2));
```
- ```||```: Or
```javascript
console.log("1 is equal to 5 or 2 is equal to 2: " + (1===5 || 2===2));
```