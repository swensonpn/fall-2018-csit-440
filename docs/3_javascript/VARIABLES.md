# Variables

## Naming Variables
- Must begin with a character, dollar sign, or underscore
- Case Sensitive
- Cannot be a [reserve word](https://www.w3schools.com/js/js_reserved.asp)
- It is recommended that variable names be camel case (i.e. camelCase).

## Variable Scope
[Scope](https://en.wikipedia.org/wiki/Scope_(computer_science)) referes to the
part of the program where the variable is valid.  

> ### Global Scope
> A variable is said to have global scope if it can be accessed everywhere in
  the program.  In a web page global variables are assigned to an object called 
  window.  Use of global variables should be limited to prevent [name collisions](https://en.wikipedia.org/wiki/Name_collision).
  ```javascript
  //variables declared without a preceding keyword are global
  myGlobalVariableOne = "I am global";
  ```
  ```javascript
  //variables directly assigned to the window are global.
  window.myGlobalVariableTwo = "I am also global";
  ```
  ```javascript
  //variables declared with a keyword not in a closure are global
  var myGlobalVariableThree = "Also global";
  ```
> ### Local Scope
> In client-side JS any variable not assigned to the window object is locally scoped.
  Local variables can only be accessed from the current or lower scope.

> #### Block Variables
> Variables declared with the keyword ```let``` are scoped to the current block.
  A block can be defined as any set of curly braces.
  ```javascript
  //example of block scoped variable
  function blockExample(value) {
    if(value === 5) {//block
        let newValue = value + 5;
        console.log(newValue);
    }
    console.log(newValue);//always undefined
  }
  blockExample(10);
  ```
> #### Closure Scoped
> Variables declared with the keyword ```var``` are scoped to the current [closure](https://www.w3schools.com/js/js_function_closures.asp)(function)
  regardless of where they're declared. This behavior is called hoisting, and 
  differs from most programming languages.
  ```javascript
  //Example of a closure
  function doSomething() {
      //code inside the curly braces of a function is in a closure
  }
  ```
  ```javascript
  //example of hoisting
  function hoistingExample(value) {//closure
    if(value === 5) {//block
        var newValue = value + 5;
        console.log(newValue);
    }  
    console.log(newValue);
  }
  hoistingExample(10);
  ```
> ### Constants
> Variables declared with the keyword ```const``` are [immutable](https://www.merriam-webster.com/dictionary/immutable).
  This means that once declared they cannot be changed.  When declaring a constant
  the value must be set at the same time the variable is declared.  
  ```javascript
  /* 
    constants declared in all caps are typically meant to indicate a variables
    that should be re-used.
  */
  const ERROR_MESSAGE = "Oops.  Something went terribly wrong";
  ```
  ```javascript
  //they can also be used to make code less brittle.
  const floor = Math.floor(3.14);
  try {
      floor = 5;
  } catch(err) {
      console.error(err);
  }
  ```
## Declaration Best Practices
- Avoid global variables unless absolutely necessary
- Prefer ```let```  over ```var```
- Use ```const``` anytime a variable value shouldn't change.
- Declare settings and messages as constants in all caps at the top of each module.
  
  