# JavaScript
[JavaScript](https://en.wikipedia.org/wiki/JavaScript)(JS) enables dynamic interactions
with websites.  Once used only in web browsers, JS is now used for server-side
web applications, native desktop and mobile applications, and can be embedded
in many other types of software.

## History
- Introduced by [Netscape](https://en.wikipedia.org/wiki/Netscape) in 1995, and originally called LiveScript.
  While later renamed to JavaScript it has nothing to do with the [Java](https://en.wikipedia.org/wiki/Java_(programming_language)) programming language. 
- In 1996 Microsoft released its own client-side scripting language called VBScript, and its own version of JavaScript-
  like language called JScript.  
- During the early days of JS support was inconsistent.  To address this Netscape
  submitted JS to [ECMA International](https://en.wikipedia.org/wiki/Ecma_International)
  to provide a standard specification.  This standard released in 1997 is known 
  as [ECMAScript](https://en.wikipedia.org/wiki/ECMAScript)(ES).  In practice consistency
  remains an issue even today.
- Today versions of ES are referred to by year(ECMAScript 2016) which is sometimes 
  abrevieated ES 16.

## Notes
1. [Data Types and Variables](./DATA_TYPES.md)
2. [Data Types](./VARIABLES.md)
3. [Operators](./OPERATORS.md)
4. [Conditional Statements](./CONDITIONAL_STATEMENTS.md) 
5. [Loops](./LOOPS.md)
6. [Arrays](./ARRAYS.md)
7. [Object Literal](./OBJECT.md)
8. [Strings](./STRINGS.md)
9. [Dates](./DATE.md)
10. [Math](./MATH.md)
11. [Functions](./FUNCTIONS.md)
12. [Activity 2](./ACTIVITY2.md)
13. [Homework 2](./HOMEWORK.md)