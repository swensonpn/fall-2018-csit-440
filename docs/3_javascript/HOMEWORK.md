# Homework 2
Write a program that reads the data in a fixed length dat file and writes that data 
to a json file.  The source file is called cars.dat and is found in the class workspace
resources folder.  Each row in the file represents one car, and each property of
that car occupies a fixed number of character spaces.  The first row in the file 
is the property names and the second row is the data type that column should be 
assigned to when processing.  Additional processing of the file data will be 
required.

## Due Thursday September 27th, 2018

## Requirements
1. Project Setup
    - Create a folder called "homework2" in your private workspace.
    - Copy the cars.dat file from the class workspace into the homework2 folder.
    - Create a new NPM project
        - name: "homework2"
        - author: your name
        - scripts: "homework" - runs the file src/index.js with node
    - Create a folder called "src" with one file "index.js". Write your
      code in this file.
2. Project Code
    - **Functions:**
        - **csvToTwoDimensionalArray**: transform the file data from a string to
          a two dimensional array.
            - argument[0]: the contents of the cars.csv file as a string
            - return: a two dimensional array where the first dimension is the
              rows in the file, and the second dimension is the string values between
              the commas.
       
        - **arrayOfArraysOfStringToArrayOfObject**: change the data so that the 
          data representation of each car is an object instead of an array. 
            - argument[0]: the two dimensional array produced by csvToTwoDimensionalArray.
            - return: a new array of car objects.
        
        - **castPropertyValue**: casts a single property to the correct data type. 
          This function should be called inside arrayOfArraysOfStringToArrayOfObject.
            - argument[0]: the data type from the second row in the cars.csv file.
            - argument[1]: the value to be converted.
            - return: argument[1] cast to the data type specified by argument[0].
        - **carFieldToMakeModel**: Assuming that all car fields start with the make 
          and end with the model split the car field into two fields make and model.
            - argument[0]: a string value of the field "car" in the data.
            - return: an object with fields "make" and "model".
        - **calculatePowerToWeight**: calculate the power to weight ratio of each car. 
          Assign that value as a new property to car.
            - argument[0]: a car object.
            - return: none.
        - **removeForeignCars**: remove all cars that do not have an origin equal to "US"
            - argument[0]: the array of car objects.
            - return: an array of US only car objects.
        - **sortByPowerToWeight**: change the order of cars in the data so that
          cars with the greatest power to weight ratio appear first in the array
          of cars.
            - argument[0]: the array of car objects.
            - return: an array of car objects sorted by power to weight ratio.
    - **Main Program:** This program should read the cars.dat file using fs.readFile(). 
      Transform the raw data into an array of car objects using the following functions:
        - csvToTwoDimensionalArray()
        - arrayOfArraysOfStringToArrayOfObject()
        - castPropertyValue()
        - carFieldToMakeModel()
        - calculatePowerToWeight()
      Then filter, and sort the data using:
        - removeForeignCars()
        - sortByPowerToWeight()
      Finally, save that processed data into a new file called cars.json using
      fs.writeFile().
      
## Acceptance Criteria
- No incomplete/empty car objects should be in the cars.json file.
- The two heading rows should not be in the cars.json file.
- The field model in the file should be renamed to "year".
- No padding should be left on property values.
- Each property of a car should be cast as follows:
    - STRING: String
    - DOUBLE: Number parsed as a float
    - INT: Number parsed as an integer
- NaN, null, and undefined must not appear in cars.json
- Car property names must be lower case with no spaces.
- Each of the functions described above tests and/or defaults argument values to
  ensure the program will not break expectantly.
- There should be no "car" property in cars.json.  "make" and "model" should appear
  instead with the correct split of the data.
- A new field powertoweight should appear in each car object with correct result 
  for power divided by weight.
- No cars with an origin other than "US" appear in cars.json.
- The array of car objects in cars.json are sorted such that cars with the greatest
  power to weight ratio appear first.
- The cars.json file produced by the program must pass validation using [JSONLint](https://jsonlint.com/)