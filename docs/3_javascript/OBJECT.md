# Literal Object
Object literals are complex objects that structure data as name value pairs. 
Values can be any data type including arrays, objects, and functions.  This
structure is similar to a HashMap&lt;String, Object&gt; in Java.

## References
- [W3 Schools - Object](https://www.w3schools.com/js/js_objects.asp)
- [JSONLint](https://jsonlint.com/)

## Declaring
Objects can be declared empty or with values.
```javascript
//using the new operator
let objectOne = new Object();

//using the short hand notation
let objectTwo = {};
let objectThree = {propertyOne:"valueOne", propertyTwo: "valueTwo"};

console.log(objectTwo);
console.log(objectThree);
```

## Getting and Setting Properties
Values can be retrieved/set using either dot or square bracket notation.
```javascript
//get a value using dot notation
console.log(objectThree.propertyOne);

//get a value using square bracket notation with property name
console.log(objectThree["propertyOne"]);

//get a value using square bracket notation with variable
let propertyName = "propertyTwo";
console.log(objectThree[propertyName]);

//set a value using dot notation
objectOne.createdProperty = "properties don't have to exist to be set";
console.log(objectOne);

//set a value using square bracket notation with a string
objectOne["any property"] = false;

//set a value using square bracket notation with a variable
objectOne[propertyOne] = propertyOne;
```

## Iterating through an object literals
The best type of loop for iterating through an object is the for in loop.
```javascript
for(let n in objectOne) {
    console.log(n + ": " + objectOne[n]);
}
```

## Serialize and Deserialize 
Objects can be converted to a JSON string and back using a built in object.
```javascript
//convert to string using JSON.stringify()
console.log(JSON.stringify(objectOne));

//convert back to object using JSON.parse()
console.log(JSON.parse(objectOne));

//parsing invalid JSON throws an exception
try {
    console.log(JSON.parse('{"property":"value"'));
} catch(err) {
    console.error(err);
}

//works with arrays too
let arrayOrig = [0,1,2,{color:"red"}];
let stringifiedArrayOrig = JSON.stringify(arrayOrig);
console.log(stringifiedArrayOrig);

let arrayRestored = JSON.parse(stringifiedArrayOrig);
console.log(arrayRestored);
```