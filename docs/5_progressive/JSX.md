# JSX
JSX stands for JavaScript XML.  It is a proposed extension to ECMAScript that 
uses syntax similar to XML.  While JSX is not XML a basic understanding of XML 
can be helpful when learning JSX. 

## Resources
* [Draft: JSX Specification](https://facebook.github.io/jsx/)
* [Introduction to XML](https://www.w3schools.com/xml/xml_whatis.asp)
* [Introducing JSX](https://reactjs.org/docs/introducing-jsx.html)

## Security
A major risk of generating HTML in the browser has been [cross-site scripting (XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting).
At its most basic a &lt;script&gt; is submitted to the server perhaps in a comments
field.  When the web application displays that comment back to another user the
script executes as part of the page.  This type of attack is easy to stop by validating
input coming in as part of a a form and/or by encoding HTML reserve characters (&lt; &gt; " ' &amp; /)
as they are returned from the server.  However, if the user submitted data is used in
JavaScript the &lt;script&gt; tag is no longer required making this type of attack
harder stop.  

JSX helps the developer by encoding all values embedded in a JSX fragment.  This
protects the application in most situations, but developers should still take care.
* Avoid using JSX to create &lt;script&gt; and &lt;style&gt; tags
* Only display back trusted sources of data.
* Only disable HTML encoding when the source data is absolutely trusted.

## XML 
XML stands for extensible markup language.  It is not an actual language.  
Instead, it is a metalanguage that allows users to define thier own markup languages.
As an example scalable vector graphics (SVG) is a language defined in XML that is
used to describe vector graphics. 
```xml
&lt;!-- https://www.w3schools.com/graphics/tryit.asp?filename=trysvg_ellipse3 --&gt;
&lt;svg height="100" width="500"&gt;
  &lt;ellipse cx="240" cy="50" rx="220" ry="30" style="fill:yellow" /&gt;
  &lt;ellipse cx="220" cy="50" rx="190" ry="20" style="fill:white" /&gt;
&lt;/svg&gt;
```

XML has also been used to describe HTML.  Before HTML5 became popular XHTML was the
preferred language for marking up web pages.  The primary difference between the 
HTML we use today and XHTML is that XML enforces strict markup langauge syntax rules.

### XML Syntax Rules
* All documents have one root element
* Case sensitive element names
* Attribute values should be in quotes
* Properly nested elements

### Elements and Attributes
* **Elements**: An element consists of an opening &lt;tag&gt;, a closing &lt;/tag&gt;,.
  and everything in between.  Some elements may be self-closing &lt;tag/&gt;.
* **Attributes**: Attributes provide additional information about HTML elements.

```xml
&lt;!-- svg is an element --&gt;
&lt;svg height="100" width="500"&gt;&lt;!-- heigh and width are attributes --&gt;
  &lt;!-- ellipse is a self closing element --&gt;
  &lt;ellipse cx="240" cy="50" rx="220" ry="30" style="fill:yellow" /&gt;
  &lt;ellipse cx="220" cy="50" rx="190" ry="20" style="fill:white" /&gt;
&lt;/svg&gt;
```

## JSX Elements, Fragments and Components
* **Element**: Just like an XML element in JSX an element is one &lt;tag&gt; including
  all of its contents.
```
let greeting = &lt;h1&gt;Hello World&lt;/h1&gt;;
```
* **Fragment**: A group of elements declared together.
```
return (
    &lt;div&gt;
        &lt;h1&gt;Hello World&lt;/h1&gt;
    &lt;/div&gt;
);
```
* **Component**: An element that stands in for another component class.
```
    &lt;form&gt;
        &lt;FormInput/&gt;;&lt;!-- component --&gt;
    &lt;/form&gt;
```

## JSX in JavaScript
The basics of JSX are very similar to standard HTML except for the stricter enforcement
of syntax rules.  There are however some differences.
* JSX is JavaScript so it is not necessary to declare elements in a String.
* Attribute values that are variables/expressions do not require quotes.
* When inserting a list of elements into a fragment the compiler may complain
  if a special attribute ```key``` is not used to uniquely identify each item
  in the list.
* When declaring HTML attributes the JavaScript property for the attribute name
  must be used in place of the HTML attribute name.
```
&lt;p className="intro-text"&gt;Some text&lt;/p&gt;
```

## JSX Expressions
In JSX dynamic values can be inserted anywhere in the fragment using curly braces.
A value can be a variable or any single JavaScript expresssion.  An expression is
any JavaScript that produces a single value.
```xml
//using a variable expression
let greeting = "Hello World";

&lt;p title={ greeting }&gt;{ greeting }&lt;/p&gt;
```
```xml
//terenary condition
```xml
return (
    &lt;p&gt;Hello { (this.props.name !== undefined) ? this.props.name : "World" }&lt;
);
```
```xml
//jsx itself is an expression
let greeting = (this.props.name !== undefined) ? &lt;p&gt;Hello {this.props.name}&lt;/p&gt; : "";

return (
    &lt;div&gt;
        { greeting }
    &lt;/div&gt;
);
```
```xml
//calling a function
function doCalculation(a, b) { return a + b; }

return (
    &lt;span&gt;{ doCalculation(this.props.a, this.props.b) }&lt;/span&gt;
);
```
```xml
//self executing arrow function
return (
    &lt;div&gt;
        {
            ((color, content) => {
                let style = {backgroundColor:color};
                
                return (
                    &lt;span style={ style }&gt;{ content }&lt;/span&gt;
                );
            })(this.props.color, this.props.content);
        }
    &lt;/div&gt;
);
```
## Attributes
Using HTML attributes in JSX is usually straight forward, but because JSX is an
extension of JavaScript all attribute names must match the JavaScript not HTML.
This usually means changing names to camelCase.
```xml
//setting css classes
return &lt;span className="text-right"&gt;some text&lt;/span&gt;
```
```xml
//setting styles
return &lt;span style={{backgroundColor:"red",marginRight:"5px"}}&gt;some text&lt;/span&gt;
```
There are also React specific attributes that the developer must use in certain
situations.  Key is one such attribute that allows React to uniquely identify JSX
elements in a list.
```xml
//this.props.items is an array of strings ["item 1","item 2", "item 3"]

render() {
    //change this.props.items into an array of JSX elements
    const listItems = this.props.items.map((item, index) => {
    
        //a warning or error will appear if each item does not have a unique key value
        return &lt;li key={ index }&gt;{ item }&lt;/li&gt;
    });
    
    //return an HTML list
    return (
        &lt;ul&gt;
            { listItems }
        &lt;/ul&gt;
    );
}
```


## Children
Often it is necessary to nest components.  In those cases the developer needs to
control where those child components are rendered.  Each component automatically
receives a special property called "children" that is a collection of nested elements.
```xml
//parent component
render() {
    return (
        &lt;FormInput help="Use your NUID from MyBLUE"&gt;
            &lt;label for="username"&gt;Username&lt;/label&gt;
            &lt;input id="username" name="username"/&gt;
        &lt;/FormInput&gt;
    );
}
```
```xml
//FormInput component
render() {
    return (
        &lt;div&gt;
            { this.props.children[0] }
            &lt;br/&gt;
            { this.props.children[1] }
            &lt;br/&gt;
            &lt;span&gt;{ this.props.help }&lt;/span&gt;
        &lt;/div&gt;
    );
}   
