# Progressive Web Applications
The term progressive web application originates from a technique web developers
have long used to take advantage of new HTML, CSS, and JS features in their design
while still supporting older browsers.  That technique is called progressive
enhancement.  The idea is that an application is coded for a base level of features
that are supported in the oldest browser their orginization wishes to support. 
When a user visits the web page the application tests to see if a newer feature
is supported by the user's broweser, and if so provides some enhanced experience.

Wikipedia describes a progressive web applications as web pages that take advantage 
of the newer browser APIs that provide a native mobile type experience.  Things 
like being able to work offline, access the geo location of the user, and client-side 
databases.  In practice the term gets used to describe any web page that provides
an app like experience regardless of whether the web page uses any of the mobile
like APIs.  That's where frameworks like Angular(Google) and React(Facebook) come
in.  These frameworks simplify the creation of rich native app like web pages.

In this course we will use the React framework to build dynamic web applications.

## Resources
[Progressive Web Apps](https://en.wikipedia.org/wiki/Progressive_Web_Apps)
[React](https://reactjs.org/)

## Notes
* [Create React Project](PROJECT.md)
* [Components](COMPONENT.md)
* [JSX](JSX.md)
* [Services](SERVICE.md)
* [Event Handling](EVENTS.md)

