# Components
In web development the word component often has different specific meanings depending
on the technology or framework being discussed.  Many times those specific meanings
are just a way to describe how a generic HTML concept is used in that technology.  

That concept is that HTML elements are often grouped the same way throughout a
web page.  Consider a web form, which is made up of many fields.  Each field typically
requires at least two HTML elements(tags).  In the example below there are three elements
which together make one field component in the form.
```
&lt;div&gt;
    &lt;label for="username"&gt;NUID&lt;/label&gt;
    &lt;input id="username" name="username"/&gt;
&lt;/div&gt;
```
This technique allows the web developer to reuse the same HTML pattern and the 
CSS styling that accompanies in throughout the application.  In React this technique
is taken a step further.

A React component is a JavaScript class that encapsulates the HTML elements and
all the logic that goes with them. 
```javascript
class FormInput extends React.Component {
    render() {
        return (
            &lt;div&gt;
                &lt;label for="username"&gt;NUID&lt;/label&gt;
                &lt;input id="username" name="username"/&gt;
            &lt;/div&gt;
        );
    }
}
```

There are two important concepts to understand about React components.  The first
is state.  State refers to the properties which a component manages internally.  
The second is lifecycle.  In a React application the developer does not directly
call ```new InputField()```.  Instead, the framework looks for references to components
within other components.
```
&lt;form&gt;
    &lt;InputField/&gt;
&lt;/form&gt;
```
When a tag like the one above is encountered an instance of the InputField component
class is created.  From the moment a component is created until it is destroyed
the framework not the developer manages it.  This period of time is the component's
lifecycle.

Between the creation and destruction of a component the framework will call various
pre-defined component class methods as events within the application occur.  These
methods will be referred to as lifecycle methods in these notes.  Its up to the
developer to decide how a component behaves when thes methods get called.  The
developer should not call them directly though.

## References
* [React.Component](https://reactjs.org/docs/react-component.html)

## Constructor
In a React component class the constructor method is optional.  A constructor is
only needed when setting the initial state of a component or binding event handlers
to the component.  State and event handling will be discussed later.  If a constructor
is declared it must call super with the first argument of the constructor.
```javascript
class FormInput extends React.Component {
    constructor(props) {
        super(props);
        
        //set intial state and/or bind event handlers
    }
    
    //code continues
}
```

## Properties
The developer may create as many properties as needed for their component class.
There are two special properties that have special meaning in the React framework.
* **props**: This property has a data type of ```object```, and contains all the
  HTML attributes that the component's parent declared on it.
```javascript
//Parent component using FormInput
render() {
    return (
        &lt;form&gt;
            &lt;FormInput name="username" label="NUID"/&gt;
        &lt;/form&gt;
    )
}
```
```javascript
//FormInput component modified to use props
class FormInput extends React.Component {
    render() {
        return (
            &lt;div&gt;
                &lt;label for={ this.props.name }&gt;{ this.props.label }&lt;/label&gt;
                &lt;input id={ this.props.name } name={ this.props.name }/&gt;
            &lt;/div&gt;
        );
    }
}
```
* **state**: This property is optionally initialized by the developer in the 
  component constructor.  The property must be of type ```object```, and should
  be used to maintain values internal to the component's state.  For example, a
  web developer may wish to make entire background of FormInput gray whenever
  the &lt;input&gt; element gains focus.
```javascript
//FormInput component modified to use state
class FormInput extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            hasFocus:false
        };
    }
    
    render() {
        const backgroundColorValue = (this.state.hasFocus) ? "lightgray" : "transparent";
        
        return (
            &lt;div style={{backgroundColor: backgroundColorValue}}&gt;
                &lt;label for={ this.props.name }&gt;{ this.props.label }&lt;/label&gt;
                &lt;input id={ this.props.name } name={ this.props.name }/&gt;
            &lt;/div&gt;
        );
    }
}
```

## Methods
Just like with properties the web developer may add as many methods as they need.
In addition, each component class inheriets a number of methods from its parent
class ```React.Component```.  

### Lifecycle Methods
This is a list of the most commonly used lifecycle methods.  Refer the references
for more detailed information.

* **render**: Render is the only method a developer is required to declare in a
  component class.  The purpose of render is to tell React what to display in the
  web page by returning a JSX(discussed later) representation of the desired HTML 
  output.
```javascript
render() {
    const backgroundColorValue = (this.state.hasFocus) ? "lightgray" : "transparent";
    
    return (
        &lt;div style={{backgroundColor: backgroundColorValue}}&gt;
            &lt;label for={ this.props.name }&gt;{ this.props.label }&lt;/label&gt;
            &lt;input id={ this.props.name } name={ this.props.name }/&gt;
        &lt;/div&gt;
    );
}
```
* **componentDidMount**: Called after the component has successfully called render
  for the first time.  

* **componentDidUpdate**: This method is triggered when a component is re-rendered.
  <br/>**Important**:  Changing the actual UI is resource intensive.  React will attempt
  to only re-render a component if it changes.

* **componentWillUnmount**: This method is called right before a component is unmounted.

## Other Pre-Defined Methods

* **setState**: The rendering of a component in React is triggered when either its
  props or state changes.  Since a component manages its own state the developer
  needs a way to tell React that the state property has changed.  Calling setState
  does this.  <br/>**Important**: All changes to state after the constructor should be
  made with setState.
```javascript
//the easiest way to use set state is to pass in an object with changes to state
this.setState({
    hasFocus:true
});
```

* **forceUpdate**:  Sometimes the developer will want to re-render a component
  even though its props or state did not change.  forceUpdate allows the developer
  to do this.<br/>
  **Note**: Its generally recommended to avoid using this method when possible.
```javascript
this.forceUpdate();
```