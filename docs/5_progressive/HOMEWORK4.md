# Homework 4
Using activity 4 as a starting point change the application so that the user can
click on any column heading to sort the cars by that property.  Then update the
service class to retrieve the cars data from the internet.

## Due Tuesday November 14th.

## Requirements
1. Project Setup
    - Use the same project created for activity 4.
    - Paste the cars.json file produced homework 2 into public/json/cars.json

2. Project Code
    - **Classes**:
        - **Table**:
        Add an event handler method called handleSort to the class.  This method
        should inspect the event target to determine what field the user clicked
        and then sort the cars by that property.  This handler should be passed
        to the TableHeader component as a property.
        - **TableHeader**:
        Modify this component so that it receives a property called handleSort from
        the Table component.  Setup onClick event listeners for every &lt;th&gt;
        element that calls the handleSort method passed in by Table.
        - **CarService**: 
        Change the CarService to include the function below.  Then modify the
        the getCars method to call this function instead of just calling back 
        with the static array.
        
        ```javascript
        function doAjax (endpoint, method, data, onsuccess, onfailure) {
          const headers = new Headers({
            "x-request-id":Date.now(),
            "x-requested-with":"XMLHttpRequest",
            "Content-Type":"application/json"
          });
        
          const options = {
                  method:method.toUpperCase(),
                  headers: headers,
                  body: data
                };
        
          const request = new Request(endpoint);
        
          fetch(request, options)
            .then((response) => {
              return response.json();
            })
            .then((json) => {
              onsuccess(json);
            })
            .catch((err) => {
              onfailure(err);
            });
        }
        ```
## Acceptance Criteria
* All acceptance criteria from activity 4
* The cars are retrieved by making a network request for the cars.json file.
* When the user clicks on the header for any column in the table the cars are 
  visibly sorted by that column.  If the user keeps clicking on that column the 
  sort should toggle between an ascending sort and a descending sort.