# Create React Project

## References
[Create React App](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app)

## Create React App
Create react app is a command line tool that will generate a runnable project
configured for Webpack and Node Package Manager.  To do this use Node Package 
Managers npx program.  Npx(x for execute) allows the developer to run a package 
directly.
```shell
npx create-react-app my-app
cd my-app
npm start
```

## Built-in Documentation
When a project is created with create-react-app a [README.md](../../code/5_progressive/my-app/README.md)
file is also produced.  Easily overlooked this file is a great reference on how to do things in react.

## Running in Cloud 9
The React documenation is written assuming you're working locally not in the cloud.
These are the steps for running the newly created react app in Cloud 9.
1. In the console
```shell
npm run start
```
2. Select preview from the menu then Preview Running Application.  This will open
   a tab with the application.
3. Copy and paste the preview URL into a new browser tab or click the top second
   from the right icon.