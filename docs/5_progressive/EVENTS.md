# Event Handling
An event is an action that typically originates external to the program, which
causes the program to execute some pre-defined behavior. In this course most events
will be user interactions like mouse clicks and keyboard input.  

## References
* [Handling Events](https://reactjs.org/docs/handling-events.html)
* [SyntheticEvent](https://reactjs.org/docs/events.html)
* [Supported Events](https://reactjs.org/docs/events.html#supported-events)

## Handling Events in React
The implementation of events in React are styled after the World Wide Web 
Consortium ([WC3](https://www.w3.org)) specifications.  This means that much of
what can be found about DOM events is also applicable to React.  There are some
differences though, and developers are encouraged prefer React's event system
over vanilla JavaScript DOM events.

## Basic Wiring
Setting up an event in React will normally consist of three parts.  An method
that will be executed when the event occurs, a statement in the component constructor
binding the handler method to the ```this``` context, and an assignment of the 
handler method to an event listener.

```javascript
class Card extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            faceUp: (props.faceUp === true) ? true : false
        };
        
        this.handleClick = this.handleClick.bind(this);//bind handler method to 'this'
    }
    
    //event handler method that is executed when the event occurs
    handleClick() {
        this.setState({
           faceUp: !this.state.faceUp 
        });
    }
    
    render() {
        
        //event handler assigned to onClick event listener
        return (
            &lt;img src={  getCardPath(this.props.cardObj.suit, this.props.cardObj.value, this.state.faceUp) } 
                onClick={ this.handleClick }
                alt={ this.props.cardObj.toString() }/&gt;
        );
    }
}
```

## Event Object
When working with events the developer often needs to use information about the 
triggered event to shape the resulting behavior.  When triggred an event object
is passed to the event handler method.  In React the actual event object is not
passed, but a similar object is.

```javascript
//handler method
handleClick(e) {//e is the event object
    e.target //the HTML element that caused the event
    
    e.preventDefault()//stop the HTML action from happening 
    
    e.which//what key was pressed
}
```



