# Services
A service is not a React concept.  Services are a concept borrowed from another 
progressive framework called Angular 2 that can be useful in building React applications.
In many organizations UI developers and backend developers work in different teams.
This can make scheduling a new application difficult if the desire is to have both
teams developing at the same time since the UI team may need data from the backend
to test their user interface.

Abstracting the source of data into separate data classes can be a useful way to
easily change where the data comes from without modifying the component code.
```javascript
//The component that needs data from the server
import React from "react";
import { heroService } from "../services";

export class HeroList {
    constructor(props) {
        super(props);
        this.state = { heros:null };
    }
    
    componentDidMount() {
        heroService.getHeros((heros) => {
            this.setState({
                heros:heros
            })
        });
    }
    
    render() {
        if(this.state.heros === null) {
            return &lt;span&gt;Loading&lt;/span&gt;
        } else {
            &lt;ul&gt;
                { 
                    this.state.heros.map((hero, index) => {
                        return &lt;li key={ index }&gt;{ hero }&lt;li&gt;
                    });    
                }
            &lt;/ul&gt;   
        }
    }
```

## Sources of Data Over Time
* **Local Data**: When first getting started on an application it is often useful
  to dummy data somewhere in the application. 
```javascript
//service class with array as data
const heros = [
  "Superman",
  "Wonder Woman",
  "Green Lantern",
  "Spider Man",
  "Bat Man"
];

class HeroService {
    getHeros(callback) {
        callback(heros)
    }  
};

export const heroService = new HeroService();
```
* **Mock Service**: After the UI team and backend team have agreed on the format
  of messages that will be passed back and forth between the browser and server
  mock web services can be created.  A mock will typically contain documentation
  about how to use the service, example messages, and validation schemas to test
  messages against.  There are many tools that can expose the example messages to
  the internet so that the UI team retrieve the example messages programmically.
```javascript
const endpointUrl = "https://mock";

function doAjax (endpoint, method, data, onsuccess) {
  const headers = new Headers({
    "x-request-id":Date.now(),
    "x-requested-with":"XMLHttpRequest",
    "Content-Type":"application/json"
  });

  const options = {
          method:method.toUpperCase(),
          headers: headers,
          body: data
        };

  const request = new Request(endpoint);

  fetch(request, options)
    .then((response) => {
      return response.json();
    })
    .then((json) => {
      onsuccess(json);
    });

}

class HeroService {
    getHeros(callback) {
        doAjax(endpointUrl, "GET", undefined, callback);
    }  
};

export const heroService = new HeroService();
```
* **Production Service**: Once the backend team is ready the UI developer need
  only change the web service URL to point at the actual server.
```javascript
const endpointUrl = "https://prod";
```