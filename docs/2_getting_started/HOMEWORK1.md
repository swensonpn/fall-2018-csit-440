# Homework 1
Building on the "homework1" project built for Activity 1 re-create the 
[Material Design Template](https://www.w3schools.com/w3css/tryit.asp?filename=tryw3css_examples_material) 
featured by [W3.CSS](https://www.w3schools.com/w3css/default.asp).  This is the 
free CSS framework we will be taking examples from for the rest of the semester.

## Due September 6, 2018

## Requirements
The goal is to re-create this sample web page as closely as possible using the
development tools learned in this section.  Start by removing the contents of all
files in the src directory and deleting the UNK.png image from images.
1. **Images** - Download all images from the example, and upload them to the src/images folder.
2. **CSS** - There are four CSS files in the example. Load them as follows:
    1. **w3-theme-teal.css** - Copy the contents of this file into your index.css file.    
    2. **w3.css** - This is the CSS framework, and is already a dependency in your package.json file.  To
        include it in the final css file use the following CSS  ``` @import "~w3-css/w3.css";```
        statement at the top of index.css.
    3. **font-awesome.min.css** - Keep the &lt;link&gt; tag as is, so it downloads from [Cloudflare](https://www.cloudflare.com/).
    4. **css?family=Roboto** -  Keep the &lt;link&gt; tag as is, so it downloads from [Google Fonts](https://fonts.google.com/)
    5. **inline css** - Include CSS inside &lt;style&gt; tags in the index.css file.
3. **JavaScript** - Copy the contents of &lt;script&gt; tags into the index.js file.
4. **HTML** - Copy the example HTML directly from the example making changes as necessary to load resources
    from the homework1 project with the exception of CSS files 3 & 4. 

## Accpetance Criteria
1. When using the run script the page displays the same as the example.
    1. Images display in the page
    2. CSS files load and styles are applied
    3. JavaScript header and menu work.
2. The dist folder can be downloaded to a desktop, and still looks the same in a browser.
     1. Images display in the page
    2. CSS files load and styles are applied
    3. JavaScript header and menu work.
