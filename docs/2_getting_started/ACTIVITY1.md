# Activity 1
Create a project folder called "homework1" in your private workspace.  Configure
this folder to use Node Package Manager and Webpack.

## Due August 30, 2018 (End of Class)

## Requirements
1. package.json
    - The package name should be "homework1"
    - The package author should be your name
    - Dependencies
        -   w3-css
    - Dev Dependencies
        - webpack
        - webpack-cli
        - babel-core 
        - babel-loader 
        - babel-preset-env
        - html-webpack-plugin 
        - html-loader
        - mini-css-extract-plugin 
        - css-loader
        - file-loader
        - webpack-dev-server
    - Scripts
        - dev: Runs webpack in development mode
        - build: Runs webpack in production mode
        - start: Starts the dev server
2. Webpack
    - Configure Loaders
        - babel-loader 
        - html-loader
        - css-loader
        - file-loader
    - Plugins
        -  html-webpack-plugin
        - mini-css-extract-plugin
3. Babel
    - Create the .babelrc to match the one we did in class.  
4. Source Folder - use the files from the class repository.
    - index.html 
    - index.css
    - index.js

## Acceptance Criteria
To be considered complete the homework1 project must do the following:
1. Running the build script produces a folder called "dist" with contents that match
   the class workspace /code/2_getting_started/dist folder.
2. Running the start script starts the dev web server with index.html as the default
   starting page.
3. The css, image, and JavaScript files all correctly load with the index.html page.