# Node Package Manager (npm)
npm is a package(project) manager for the JavaScript programming language. 
It is the default package manager for the JavaScript runtime environment Node.js. 
It consists of a command line client, also called npm, and an online database of 
public and paid-for private packages, called the npm registry. The registry is 
accessed via the client, and the available packages can be browsed and searched 
via the npm website. The package manager and the registry are managed by npm, Inc.
([wikipedia](https://en.wikipedia.org/wiki/Npm_(software)))

## Documention
* [npm Documention](https://docs.npmjs.com/)

## Package File
All projects managed by npm need a file called package.json.  Think of this file
as your project definition.  It defines properties describing your project, 
lists other packages your program will depend on, and defines scrpts which can
be run using npm.  ([Working with package.json](https://docs.npmjs.com/getting-started/using-a-package.json))

> ### JavaScript Object Notation (JSON)
> The package.json file is written in a lightweight data-interchange format 
  called [JavaScript Object Notation (JSON)](https://www.json.org/). This format 
  is supported by many programming languages making it one of the most popular 
  formats available today.  Files written in JSON usually have the file extension 
  .json.
  
> ### Project Properties
> npm boasts that it is the world's largest software registry.  That's no small 
  feat given that JavaScript development tools didn't even exist a decade ago. 
  There are many reasons for the rise in popularity of npm.  One of them is
  how easy they have made it to share projects with the world by submitting them
  to the npm registry.  
  
> Simply pushing code to npm isn't sufficient for it to be useful to other developers.
  They have to be able to find those projects.  That's where the package.json
  properties come in.  They provide much of the data displayed on the npm site.
  
> **Package Properties:**  
> - name (required) - package/project name.  all lower case, no spaces.
> - version (required) - format matters(x.x.x)
> - description - simple project description.
> - main - always index.js, no main method in JavaScript so this tells npm to begin.
> - keywords - helps developers find the project in the npm registry.
> - author - one person. can be just a name or an object with name, email, and url.
> - contributors - array of persons in same format as author.
> - license - default [ISC](https://opensource.org/licenses/ISC)

> ### Dependency Management
> In software development [code reuse](https://en.wikipedia.org/wiki/Code_reuse)
  is an important concept.  Use of loops and functions/methods in a program are
  basic examples of code reuse.  More comprehensive examples are software 
  [frameworks](https://en.wikipedia.org/wiki/Software_framework) and 
  [libraries](https://en.wikipedia.org/wiki/Library_(computing)).  
  
> Managing the examples above in a client-side web application has always been
  pretty straight forward.  Traditional client-side web applications were typically
  small and rarely used more than one or two large scale frameworks/libraries.
  The challenge starts when the web developer starts to include smaller widgets 
  that depend on other libraries, frameworks, or widgets.  
  
> Consider a calendar widget designed to extend [Twitter Bootstrap](https://getbootstrap.com/). 
  This calendar widget will depend on Bootstrap and possibly other tools like 
  [Moment](https://momentjs.com/). In turn, Bootstrap depends on [jQuery](https://jquery.com/).
  Until recently the web developer would need to link all of the JavaScript files 
  for each of these dependencies to their web page. 
  
> Over time the widgets, frameworks, and even browsers change creating the 
  possibility the underlying application will break.  Now someone has to research
  why the application has broken.  Multiply this time by the number of widgets
  included in a modern web page, and dependency management becomes a nightmare.
  
> npm seeks to fix this issue, by managing dependencies for the developer.  When
  dependencies are listed in the package.json npm will download the dependency
  package from the npm registry.  The downloads can be found in a special folder
  called node_modules.  If one of those dependencies has dependencies of its own,
  npm will download the appropriate version of those as well.
  
> **Depenency Properties:**
> - dependencies - packages the application needs to run.
> - devDependencies - developer tools and project building utilities.
  
> ### Scripts
> Like many software development tools is based on the command line.  When working
  with the command line sequences of commands are often repeated.  npm provides
  scripts as a way to execute multiple commands with a single statement. They can
  be as simple as one statement or as complex as executing a series of programs.
  
> **Script Property:**
> - scripts - a list of name value pairs where name is the name of the script and 
  value is the code to execute.
  
## Package Lock File
Certain npm operations will trigger the creation of a file called package-lock.json.
This file is generated when any operation modifies the node_modules folder, 
or package.json file.  It's primary purpose is to describe the dependency tree
in such a way that future installations of this project are identical to the
original project. [package-lock.json](https://docs.npmjs.com/files/package-lock.json)  
 
## Verify Setup
Before starting with npm verify that npm is installed.  
```shell
$ npm -v
6.2.0
```

## Create a New Project
These are the steps to have npm create a new project.
1. Create a folder for your project.  Below is the command, but this can also be
   done in the Cloud9 user interface.
    ```shell
    ~/workspace $ mkdir 2_getting_started
    ```
2. Change directories to the new folder.
    ```shell
    ~/workspace $ cd 2_getting_started
    ~/workspace/2_getting_started $
    ```
3. Run the npm command for initiating a new project and answer questions as prompted.
    ```shell
    ~/workspace/projectname $ npm init
    This utility will walk you through creating a package.json file.
    It only covers the most common items, and tries to guess sensible defaults.
    <br>
    See `npm help json` for definitive documentation on these fields
    and exactly what they do.
    <br>
    Use `npm install <pkg>` afterwards to install a package and
    save it as a dependency in the package.json file.
    <br>
    Press ^C at any time to quit.
    package name: (2_getting_started)
    version: (1.0.0) 
    description: my first project
    entry point: (index.js) 
    test command: node test.js
    git repository: https://bitbucket.org/swensonpn/fall-2018-csit-440/src/master/
    keywords: 
    author: Paul Swenson
    license: (ISC) 
    About to write to /home/ubuntu/workspace/code/2_getting_started/package.json:
    {
        "name": "projectname",
        "version": "1.0.0",
        "description": "my first project",
        "main": "entry point: (index.js) ",
        "scripts": {
            "test": "./test.js"
        },
        "repository": {
            "type": "git",
            "url": "https://bitbucket.org/swensonpn/fall-2018-csit-440/src/master/"
        },
        "author": "Paul Swenson",
        "license": "ISC"
    }
    <br>
    Is this OK? (yes) yes
    ```
4. Verify that a new package.json file appears inside the new project directory.

## Installing Dependencies
Installing dependencies is a two step process. First, the dependency must be 
downloaded into a file called node_modules.  Second, an entry should be made
in the package.json file so that the new dependency downloads when this project
is installed in a new location.
1. Run the following command for a regular dependency
    ```shell
    ~/workspace/2_getting_started $ npm install npm-hello-world --save
    npm notice created a lockfile as package-lock.json. You should commit this file.
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    <br>
    + npm-hello-world@1.0.1
    added 1 package from 1 contributor and audited 1 package in 0.976s
    found 0 vulnerabilities
    ```
2. Run this command to install a development dependency
    ```shell
    ~/workspace/2_getting_started $ npm install npm-hello-world --save-dev
    npm notice save npm-hello-world is being moved from dependencies to devDependencies
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    <br>
    + npm-hello-world@1.0.1
    updated 1 package and audited 1 package in 0.655s
    found 0 vulnerabilities
    ```
3. Verify the new entry was added to the package.json file
4. Verify the package was downloaded to the node_modules directory

## Reinstall This Project
When distributing an application its dependencies should not be included with the
project's source code.  Instead, the person who downloads the application should
run the npm install command.  

This is also useful when the developer hand edits the dependency entries in their
package.json file or if the node_modules folder gets deleted.
1. Run the following command to update/download dependencies in the node_modules folder.
    ```shell
    ~/workspace/2_getting_started $ npm install
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    <br>
    audited 1 package in 0.64s
    found 0 vulnerabilities
    ```
2. Verify the dependencies were downloaded to the node_modules directory

## Running a Script
When the project was created test.js was set as the entry point for a script 
called "test".  This file doesn't exist yet, but in the package.json file there 
is an entry called "scripts".

1. Look in the package.json file
    ```shell
    "scripts": {
        "test": "node test.js"
    },
    ```
2. Create a file called test.js in the same folder as package.json
3. Open the folder and type the following code
    ```javscript
    let hello = require("npm-hello-world");
    <br>
    hello.log();
    ```
4. Run the script
    ```shell
    ~/workspace/2_getting_started $ npm run test
    <br>
    > 2_getting_started@1.0.0 test /home/ubuntu/workspace/code/2_getting_started
    > node test.js
    <br>
    hello world
    ```

