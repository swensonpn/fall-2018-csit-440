# Development Environment Setup
The following tasks will configure a development environment that can be used
to write assigned programs, and ensure that work can be turned in for grading.

## Create an account on Cloud9
Cloud9 is the integrated development environment (IDE) we'll be using to type
code for this course. 
1. Check your email for an email from support@c9.io
2. Click the link in the email, and create your account.
3. Create a new workspace
    - Click the Create a new workspace tile
    - Name your new workspace fall-2018-csit440-"your canvas username"
    - Change the workspace to private
    - Change workspace type from HTML5 to Blank Workspace
    - Click Create workspace
4. Add the .gitignore file from this workspace into your new workspace

## Upgrade/install NodeJS(node) & Node Package Manager(npm)
1. Install node from the bash tab at the bottom of your IDE screen.
    ```shell
    nvm install node
    ```
    The output should look like this
    ```shell
    Downloading https://nodejs.org/dist/v10.8.0/node-v10.8.0-linux-x64.tar.xz...
    ######################################################################## 100.0%
    Now using node v10.8.0 (npm v6.2.0)
    ```
2. Set the new node version as the default
    ```shell
    nvm alias default 10.8.0
    ```

## Create a GIT repository
1. Initialize your project as a Git repository.  
    ```shell 
    git init ./
    ```
2. Add your new index.html file to the repository.
    ```shell
    git add README.md
    ```
8. Commit the work on the new file
    ```shell
    git commit -m "created the index.html file"
    ```

## Create an account at bitbucket.org and link it to Cloud9
1. Create an account at [bitbucket.org](https://bitbucket.org/)
    - Click on get started and complete the registration form
    - At the top left of the screen after logging in click "+" and select repository
        - Give your repository the same name as your cloud9 workspace.
        - Make sure the repository is private
        - Click create repository
    - In your new repository expand the section called 'I have an existing project'
        - Copy each of the three lines of code beginning with git.  You will use them in step 3
2. Go back to Cloud9 and link your new bitbucket account to Cloud9
    - Navigate to your account settings. (gear at the top right of your home page)
    - In the left menu select Connected Services
    - Then select Bitbucket.  A popup window should open asking for your Bitbucket credentials.
    - Log in.  Your accounts are now linked.
3. In Cloud9 add the remote bitbucket repository to your workspace.  you may be prompted to type in yes and hit enter.
    ```shell
    git remote add origin [Your repository location from step 1]
    ```
    ```shell
    git push -u origin --all
    ```
    ```shell
    git push -u origin --tags
    ```
4. Go to bitbucket and verify your README.md file is visible on the repository page.
5. Share your bitbucket repository with the instructor.
    - On the source page of your new bitbucket repository click the "..." icon and select share repository
    - Search for "swensonpn@gmail.edu".
    - Leave permissions as "read" and click the add button.
    - Verify with the instructor your account is accessible.

## Submitting homework.
All course work must be submitted to you bitbucket repository to receive a grade.
1. Stage any new files to be committed. Hint: homework1/* would stage all files contained in a folder called homework1
    ```shell
    git add homework1/*
    ```
2. Commit all your changes to the repository
    ```shell
    git commit -m "Submitting Homework 1"
    ```
3. Push your changes to Bitbucket
    ```shell
    git push origin master
    ```