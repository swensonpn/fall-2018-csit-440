# Webpack
webpack is a module bundler. Its main purpose is to bundle JavaScript files for 
usage in a browser, yet it is also capable of transforming, bundling, or packaging 
just about any resource or asset. [Webpack](https://webpack.js.org/)

## Documention
* [Webpack Documention](https://webpack.js.org/concepts/)
* [Webpack 4 Tutorial](https://www.valentinog.com/blog/webpack-tutorial/)

## Benefits of Bundling
npm makes managing complex dependencies much easier, but it was designed for
JavaScript.  Specifically, ([NodeJS](https://nodejs.org/en/)) which runs on a
server.  Web pages rely on many types of files not just JavaScript.  In the calendar
widget example the developer needed to link as many as four JavaScript files to
the web page.  In fact, they would also need to link two or more stylesheets, plus
fonts, and possibly image files.  Keeping track of all these resources can be 
overwhelming.  In addition, the web page in question will downloading many files.
Each of these files contain not only the required code, but hundreds perhaps thousands
of lines of code that isn't needed for the calendar to function.

Tools like Webpack seek make managing assets, building projects, and deploying 
code easier.  Here are a few things Webpack can do:
* Bundle all used application code including dependencies into a single JavaScript file.
* Optimize and compress JavaScript code.
* Transpile the newest JavaScript language features into JavaScript that can be
  run by older browsers.
* Bundle CSS, Sass, and Less files into single compressed CSS files.
* Manage image and other resources.

## Wepack Configuration File
The file used for configuring Webpack is called webpack.config.js.  This file
is optional as of version 4 as long as no changes are needed to the default 
configuration. There are some basic [concepts](https://webpack.js.org/concepts/) 
that are important to understand before undertaking Webpack configuration.

* Entry - Is the file (default ./src/index.js) where Webpack starts to build a
  bundle.
* Output - The directory (default ./dist) where the finished project will be saved. 
* Loaders - Tells Webpack how to process a file.  There may be many loaders in a
  typical build.  Webpack uses pattern matching rules on the filename to determine
  which loaders to use.  Each rule will have two or more properties.
    * test (required) - The pattern used to match a file name.  
    * use (required) - A list of loaders that should be applied.
    * exclude - File patterns to exclude from processing.
* Plugins - Provide additional functionality beyond just how to process a file.
  The HTML Webpack Plugin for example can replace static values in an HTML file with
  dynamic values based on the build.  

## Getting Webpack
Use npm to install webpack and webpack command line in your project.
1. Get Webpack
    ```shell
    ~/workspace/code/2_getting_started $ npm install webpack --save-dev
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    <br>
    + webpack@4.16.5
    added 319 packages from 277 contributors and audited 3404 packages in 13.085s
    found 0 vulnerabilities
    ```
2. Get Webpack command line tool
    ```shell
    ~/workspace/code/2_getting_started $ npm install webpack-cli --save-dev
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    <br>
    + webpack-cli@3.1.0
    added 81 packages from 37 contributors and audited 3548 packages in 9.12s
    found 0 vulnerabilities
    ```

## Run Webpack
1. Create an entry point file in the Cloud9 user interface or command line.
    ```shell
    ~/workspace/code/2_getting_started $ mkdir src
    ~/workspace/code/2_getting_started $ touch src/index.js
    ```
2. Open the file and add the following
    ```javascript
    let greeting = "Hello Webpack";
    console.log(greeting);
    ```
3. Create an npm script for running webpack. Modify the scripts section in package.json to look like this.
    ```shell
    "scripts": {
        "test": "node test.js",
        "dev": "webpack --mode development",
        "build": "webpack --mode production"
    },
    ```
4. Save and close package.json
5. Run the new build script.  Pay attention to the output.  Success or failure will be reported
    ```shell
    ~/workspace/code/2_getting_started $ npm run build
    > 2_getting_started@1.0.0 build /home/ubuntu/workspace/code/2_getting_started
    > webpack --mode production
    <br>
    Hash: bf0519a1a9fae53e2a62
    Version: webpack 4.16.5
    Time: 402ms
    Built at: 08/19/2018 7:56:02 PM
      Asset       Size  Chunks             Chunk Names
    main.js  958 bytes       0  [emitted]  main
    Entrypoint main = main.js
    [0] ./src/index.js 29 bytes {0} [built]
    ```
6. Look in a directory called "dist" for the bundled application.
7. Open the file called "main.js" using the run button in Cloud9 execute the bundle.
   Hello Webpack should be in the new run configuration tab at the bottom of the screen.

## Creating Webpack Configuration
To use any functionality beyond basic Webpack a wepack.config.js file is needed.
In this example [Babel](https://babeljs.io/) will be configured as a step in the build. 
1. Install Babel
    ```shell
    ~/workspace/code/2_getting_started $ npm install babel-loader@7 babel-core babel-preset-env --save-dev
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    <br>
    + babel-loader@7.1.5
    + babel-preset-env@1.7.0
    + babel-core@6.26.3added 91 packages from 18 contributors and audited 5541 packages in 8.88s
    found 0 vulnerabilities
    ```
3. Create a new webpack.config.js file in the project folder using the UI or command line.
    ```shell
    ~/workspace/code/2_getting_started $ touch webpack.config.js
    ```
4. Copy the following code into the new file.
    ```javascript
    module.exports = {
        module: {
            rules: [
              {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                  loader: "babel-loader",
                  options: {
                    presets: ['env']
                  }
                }
              },
            ]
        }
    };
    ```
    The script above says that any file with a file extension .js should be processed
    by the babel-loader unless its located in the node_modules directory.
5. Run the build script
    ```shell
    ~/workspace/code/2_getting_started $ npm run dev
    > 2_getting_started@1.0.0 dev /home/ubuntu/workspace/code/2_getting_started
    > webpack --mode development
    <br>
    Hash: 1db2839122caeaf1a31a
    Version: webpack 4.16.5
    Time: 567ms
    Built at: 08/19/2018 8:52:14 PM
      Asset      Size  Chunks             Chunk Names
    main.js  3.86 KiB    main  [emitted]  main
    Entrypoint main = main.js
    [./src/index.js] 69 bytes {main} [built]
    ```
6. Go look at the code in main.js at the bottom of the file "let" should appear as "var"

## Using the HTML Webpack Plugin
The [HtmlWebpackPlugin](https://webpack.js.org/plugins/html-webpack-plugin/) 
simplifies injection of bundled resources into an html page for serving an application.
1. Create a file called index.html in the src folder using Cloud9 ui or command line
    ```shell
    ~/workspace/code/2_getting_started $ touch src/index.html
    ```
2. Copy the fillowing contents into the index.html file
    ```
    &lt;!DOCTYPE html&gt;
    &lt;html lang="en"&gt;
        &lt;head&gt;
            &lt;title&gt;Greeting&lt;/title&gt;
        &lt;/head&gt;
        &lt;body&gt;
            &lt;h1&gt;Greeting&lt;/h1&gt;
            &lt;p id="greeting"&gt;&lt;/p&gt;
        &lt;/body&gt;
    &lt;/html&gt;
    ```
3. Update the index.js file to match this.
    ```javascript
    let greeting = "Hello Webpack";
    let element = document.getElementById("greeting");
    
    if(element !== null)  {
        element.innerHTML = greeting;
    }
    ```
4. Install the html loader and plugin using npm
    ```shell
    ~/workspace/code/2_getting_started $ npm install html-webpack-plugin html-loader --save-dev
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    <br>
    + html-loader@0.5.5
    + html-webpack-plugin@3.2.0
    added 59 packages from 69 contributors and audited 5643 packages in 8.608s
    found 0 vulnerabilities
    ```
5. Modify the webpack.config.js file to look like this.
    ```javascript
    const HtmlWebPackPlugin = require("html-webpack-plugin");
    module.exports = {
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ['env']
              }
            }
          },
          {
            test: /\.html$/,
            use: [
              {
                loader: "html-loader",
                options: { minimize: false }
              }
            ]
          }
        ]
      },
      plugins: [
        new HtmlWebPackPlugin({
          template: "./src/index.html",
          filename: "./index.html"
        })
      ]
    };
    ```
6. Run the build
    ```shell
    ~/workspace/code/2_getting_started $ npm run build
    > 2_getting_started@1.0.0 build /home/ubuntu/workspace/code/2_getting_started
    > webpack --mode production
    <br>
    Hash: 58eab63c240edd025096
    Version: webpack 4.16.5
    Time: 1129ms
    Built at: 08/19/2018 9:17:59 PM
      Asset       Size  Chunks             Chunk Names
    main.js  973 bytes       0  [emitted]  main
    ./index.html  184 bytes          [emitted]  
    Entrypoint main = main.js
    [0] ./src/index.js 69 bytes {0} [built]
    Child html-webpack-plugin for "index.html":
      1 asset
      Entrypoint undefined = ./index.html
      [0] ./node_modules/html-webpack-plugin/lib/loader.js!./src/index.html 150 bytes {0} [built]
    ```
7. Look in the folder called "dist" for index.html.  This file should now have a script tag pointing at the main.js file

## Using the Mini Css Extractor Plugin
The [MiniCssExtractPlugin](MiniCssExtractPlugin) allows css to be bundled in
a similar way to JavaScript.
1. Create a file called "index.css" using the command line or Cloud9 ui.
    ```shell
    ~/workspace/code/2_getting_started $ touch src/index.css
    ```
2. Paste the following CSS into the new file.
    ```css
    html {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 16px;
    }
    
    body {
        margin:0;
        padding:60px;
    }
    ```
3. Change the src/index.js file to look like this
    ```javascript
    import style from "./index.css";

    let greeting = "Hello Webpack";
    let element = document.getElementById("greeting"); 
    
    if(element !== null)  {
      element.innerHTML = greeting;
    }
    ```
4. Install the plugin and loader
    ```shell
    ~/workspace/code/2_getting_started (master) $ npm install mini-css-extract-plugin css-loader --save-dev
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    <br>
    + mini-css-extract-plugin@0.4.1
    + css-loader@1.0.0
    added 28 packages from 57 contributors and audited 5817 packages in 7.738s
    found 0 vulnerabilities
    ```
5. Change the webpack.config.js file to match this
    ```javascript
    const HtmlWebPackPlugin = require("html-webpack-plugin");
    const MiniCssExtractPlugin = require("mini-css-extract-plugin");
    
    module.exports = {
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ['env']
              }
            }
          },
          {
            test: /\.html$/,
            use: [
              {
                loader: "html-loader",
                options: { minimize: true }
              }
            ]
          },
          {
            test: /\.css$/,
            use: [MiniCssExtractPlugin.loader, "css-loader"]
          }
        ]
      },
      plugins: [
        new HtmlWebPackPlugin({
          template: "./src/index.html",
          filename: "./index.html"
        }),
        new MiniCssExtractPlugin({
          filename: "[name].css",
          chunkFilename: "[id].css"
        })
      ]
    };
    ```
6. Build the project
    ```shell
    ~/workspace/code/2_getting_started $ npm run build
    > 2_getting_started@1.0.0 build /home/ubuntu/workspace/code/2_getting_started
    > webpack --mode production
    
    Hash: 808311b5a0bbf38ec641
    Version: webpack 4.16.5
    Time: 1173ms
    Built at: 08/19/2018 9:44:24 PM
      Asset       Size  Chunks             Chunk Names
    main.css  134 bytes       0  [emitted]  main
    main.js      1 KiB       0  [emitted]  main
    ./index.html  267 bytes          [emitted]  
    Entrypoint main = main.css main.js
    [0] ./src/index.js 251 bytes {0} [built]
    [1] ./src/index.css 39 bytes {0} [built]
        + 1 hidden module
    Child html-webpack-plugin for "index.html":
        1 asset
        Entrypoint undefined = ./index.html
        [0] ./node_modules/html-webpack-plugin/lib/loader.js!./src/index.html 207 bytes {0} [built]
    Child mini-css-extract-plugin node_modules/css-loader/index.js!src/index.css:
        Entrypoint mini-css-extract-plugin = *
        [0] ./node_modules/css-loader!./src/index.css 301 bytes {0} [built]
            + 1 hidden module
    ```
7. Check the folder called "dist" for a new file called main.css.
8. Look at the dist/index.html file for a link to the new stylesheet.

## Loading Images
1. Download unk.png from the class workspace.
2. Save the file into a folder src/images.
3. Change the src/index.html file to look like the following.
    ```
    &lt;!DOCTYPE html&gt;
    &lt;html lang="en"&gt;
        &lt;head&gt;
            &lt;title&gt;Greeting&lt;/title&gt;
        &lt;/head&gt;
        &lt;body&gt;
            &lt;h1&gt;Greeting&lt;/h1&gt;
            &lt;p id="greeting"&gt;&lt;/p&gt;
            &lt;img src="./images/unk.png" alt="UNK"/&gt;
        &lt;/body&gt;
    &lt;/html&gt;
    ```
4. Install the file loader
    ``` shell
    ~/workspace/code/2_getting_started $ npm install file-loader --save-dev
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    <br>
    + file-loader@1.1.11
    added 1 package from 1 contributor and audited 5830 packages in 6.449s
    found 0 vulnerabilities
    ```
5. Change the webpack.config.js file to match this
    ```javascript
    const HtmlWebPackPlugin = require("html-webpack-plugin");
    const MiniCssExtractPlugin = require("mini-css-extract-plugin");

    module.exports = {
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ['env']
              }
            }
          },
          {
            test: /\.html$/,
            use: [
              {
                loader: "html-loader",
                options: { minimize: false }
              }
            ]
          },
          {
            test: /\.css$/,
            use: [MiniCssExtractPlugin.loader, "css-loader"]
          },
          {
            test: /\.(jpe?g|png|gif|svg)$/i,
            use: [{
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'images/',    
                publicPath: 'images'   
              }
            }]
          },
        ]
      },
      plugins: [
        new HtmlWebPackPlugin({
          template: "./src/index.html",
          filename: "./index.html"
        }),
        new MiniCssExtractPlugin({
          filename: "[name].css",
          chunkFilename: "[id].css"
        })
      ]
    };
    ```
5. Build the project
    ```shell
    ~/workspace/code/2_getting_started (master) $ npm run build
    > 2_getting_started@1.0.0 build /home/ubuntu/workspace/code/2_getting_started
    > webpack --mode production
    
    Hash: d036af30244d93f17018
    Version: webpack 4.16.5
    Time: 911ms
    Built at: 08/19/2018 10:06:58 PM
      Asset       Size  Chunks             Chunk Names
      images/unk.png   11.9 KiB          [emitted]  
      main.css  107 bytes       0  [emitted]  main
      main.js      1 KiB       0  [emitted]  main
      ./index.html  314 bytes          [emitted]  
    Entrypoint main = main.css main.js
    [0] ./src/index.js 251 bytes {0} [built]
    [1] ./src/index.css 39 bytes {0} [built]
      + 1 hidden module
    Child html-webpack-plugin for "index.html":
      Asset      Size  Chunks             Chunk Names
      images/unk.png  11.9 KiB          [emitted]  
      + 1 hidden asset
      Entrypoint undefined = ./index.html
      [0] ./node_modules/html-webpack-plugin/lib/loader.js!./src/index.html 279 bytes {0} [built]
      [1] ./src/images/unk.png 35 bytes {0} [built]
    Child mini-css-extract-plugin node_modules/css-loader/index.js!src/index.css:
      Entrypoint mini-css-extract-plugin = *
      [0] ./node_modules/css-loader!./src/index.css 273 bytes {0} [built]
        + 1 hidden module
    ```

## Installing Webpack Dev Server

1. Install the server
    ```shell
    ~/workspace/code/2_getting_started $ npm install webpack-dev-server --save-dev
    npm WARN 2_getting_started@1.0.0 No description
    npm WARN 2_getting_started@1.0.0 No repository field.
    npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.4 (node_modules/fsevents):
    npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.4: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
    <br>
    + webpack-dev-server@3.1.5
    added 171 packages from 119 contributors and audited 8490 packages in 14.906s
    found 0 vulnerabilities
    ```
2. Change the scripts section of package.json to match 
    ```
    "scripts": {
        "test": "node test.js",
        "dev": "webpack --mode development",
        "build": "webpack --mode production",
        "start": "webpack-dev-server --mode development --open"
    },
    ```
3. Change the webpack.config.js file to match this
    ```javascript
    const HtmlWebPackPlugin = require("html-webpack-plugin");
    const MiniCssExtractPlugin = require("mini-css-extract-plugin");

    module.exports = {
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ['env']
              }
            }
          },
          {
            test: /\.html$/,
            use: [
              {
                loader: "html-loader",
                options: { minimize: false }
              }
            ]
          },
          {
            test: /\.css$/,
            use: [MiniCssExtractPlugin.loader, "css-loader"]
          },
          {
            test: /\.(jpe?g|png|gif|svg)$/i,
            use: [{
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'images/',    
                publicPath: 'images'   
              }
            }]
          },
        ]
      },
      devServer: {
        host: process.env.IP,
        port: process.env.PORT,
        public:"fall-2018-csit-440-swensonpn.c9users.io",
        contentBase: './src'
      },
      plugins: [
        new HtmlWebPackPlugin({
          template: "./src/index.html",
          filename: "./index.html"
        }),
        new MiniCssExtractPlugin({
          filename: "css/[name].css",
          chunkFilename: "[id].css"
        })
      ]
    };
    ```
4. Start the server
    ```shell
    ~/workspace/code/2_getting_started $ npm run start
    ```
5. Use Preview > Preview Running Application to view page
