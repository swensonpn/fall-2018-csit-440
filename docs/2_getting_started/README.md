# Getting Started
In this section we will be setting up development environments, learning about
build tools, and creating a static web page.

## Notes
1. [Development Environment Setup](./SETUP.md)
2. [Node Package Manager](./NPM.md)
3. [Webpack](./WEBPACK.md)
4. [Activity 1](./ACTIVITY1.md)
5. [Homework 1](./HOMEWORK1.md)