# Object Oriented Programming
Object-oriented programming (OOP) is a programming paradigm based on the concept of "objects", 
which may contain data, in the form of fields, often known as attributes; and code, 
in the form of procedures, often known as methods. [Wikipedia](https://en.wikipedia.org/wiki/Object-oriented_programming)

As programs grow they become more difficult to write, enhance, and maintain.
Learning to model certain aspects of a program as objects rewards the developer 
by letting them concentrate on one small part of the overall program at a time. 
It also, encourages better code organization and easier reuse of code.  The cost
for this simplification is a little more time up front thinking about the project.

## References
- [Wikipedia - Object-Oriented Programming](https://en.wikipedia.org/wiki/Object-oriented_programming#Class-based_vs_prototype-based)
- [How to explain object-oriented programming concepts to a 6-year-old](https://medium.freecodecamp.org/object-oriented-programming-concepts-21bb035f7260)

## Terminology
- **object**: The representation of some application concept in code.  Consider
  the a game of cards.  Possible objects may be Card, CardDeck, and Player. 
- **field, property, attribute**: All are terms used to describe some variable
  that belongs to an object.  For example, a playing card object may have properties
  suit, value, and image.
- **method**: A function that belongs to an object and performs some procedure
  consistent with the objects purpose.  For example, an object representing a
  deck of cards may have a method called dealHands or shuffle.
- **state**: A generic term used refer to the values of all object properties as a group. 
- **instance**: The realization of an object. A deck of playing cards would have
  52 instances of the Card object each with its own unique state (suit, value, and image).  
- **class**: A declaration that defines the properties and methods of one object type.
- **prototype**: An object that serves as a template for creating object instances.
- **private**: A property or method belonging to an object that cannot be accessed
  outside the object itself.
- **public**: A property or method that has been exposed so it may be called or
  manipulated outside the object itself.
- **constructor**: Code that is called when an object instantiated.  
- **this,self**: How an object refers to itself internally.
- **singleton**: An object which may have only one instance.

## Principles of Object Oriented Programming
- **Encapsulation**: The idea that an object should manage its own state.  This
  is usually achieved by keeping object properties private, and only allowing
  changes through public methods.  
- **Abstraction**: The key to abstraction is exposing only high level ideas to 
  the rest of the program.  In OOP objects communicate by calling each others 
  methods.  If as the program grows the developer needs to change an object that
  is complicated to use or that isn't generic enough large portions of the application
  may need to be changed.  If on the other hand the interface is simple enough
  the whole object may be replaced without changing a single other line of code.
- **Inheritance**: This principle allows the developer to organize their objects
  in a hierarchical fashion.  This allows objects to share base functionality, 
  while adding new functionality.  For example, in blackjack there are two to many
  players where only one player is the dealer.  All players behave in essentially
  the same way, but the dealer has a few special rules.  By extending the player
  object to create a dealer object the developer only needs to code the properties
  and methods that are unique to the dealer, because all the player properties
  and methods are inherieted.
- **Polymorphism**: Polymorphism allows a developer to use child objects as if they
  were the parent object.  In the blackjack example above there are two or more 
  player objects one being the dealer.  In this program all players are dealt the 
  same number of cards and the player object would have a method addCardToHand(card). 
  Polymorphism allows the developer to put all players including the dealer in a
  single array then looping through that array dealing cards to each player including
  the dealer without writing any special code.


## Notes
1. [JavaScript OOP](JSOOP.md)
2. [ES16 Classes](./CLASSES.md)
3. [Modules](./MODULES.md)
4. [Activity 3](./ACTIVITY3.md)