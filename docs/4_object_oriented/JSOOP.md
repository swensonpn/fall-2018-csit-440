# JavaScript OOP

## References
- [Wikipedia - Object-Oriented Programming](https://en.wikipedia.org/wiki/Object-oriented_programming#Class-based_vs_prototype-based)
- [MDN - Details of the object model](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Details_of_the_Object_Model)


## Prototype Based
JavaScript is a prototyipical programming language.  This means that the rules
for each type of object are based on some template object.  Each object has exactly
one prototype.

```javascript
//view in Firefox developer toolbar
console.log(new String("Hello"));
console.log(new Array("zero","one","two"));
console.log(new Object());
```

## Functions as Objects
Instantionable objects in JavaScript are traditionally built on Functions. The 
function declaration acts as a constructor for the object.  Properties and methods
are added to the object using prototype.

> ### Constructor
```javascript
function Card(suit, value) {
    //validate suit
    suit = parseInt(suit, 10);
    if(suit < 0 || suit > 3) throw new Error("Suit must be an integer between 0 and 3");
    
    //validate value
    value = parseInt(value, 10);
    if(value < 1 || value > 13) throw new Error("Value must be an integer between 1 and 13");
    
    this.suit = suit;//this keyword makes suit a puplic property of the Card instance.
    this.value = value;
}
```

> ### Adding Properties to Prototype
```javascript
Card.prototype.CLUB = 0;
Card.prototype.DIAMOND = 1;
Card.prototype.HEART = 2;
Card.prototype.SPADE = 3;
```
 > ### Adding Methods to Prototype
 ```javascript
Card.prototype.getSuitName = function() {
    const names = ["club", "diamond", "heart", "spade"];
    
    return names[this.suit];
};
```
```javascript
Card.prototype.getValueDescription = function() {
    switch(this.value) {
        case 1:
            return "ace";
        case 11:
            return "jack";
        case 12:
            return "queen";
        case 13:
            return "king";
        default:
            return this.value;
    }
};
```
```javascript
Card.prototype.toString = function() {
    return `${this.getValueDescription()} of ${this.getSuitName()}s`;
};
```

> ### Create and Use Instances
```javascript
let deck = [];

for(let i = 0; i < 4; i++) {//loop through four suits
    for(let j = 1; j < 14; j++) {
        const card = new Card(i, j);//created a new card instance
        console.log(card.toString());//use the toString method
        deck.push(card);
    }
}
console.log(deck);
```

## Private Properties and Methods
The example above has a problem with encapsulation.  All properties and methods
added with either the ```this``` keyword or with ```prototype``` are public.  Consider
the following:

```javascript
//the dealer is dealt these cards
const hand = [
    new Card(0, 10),
    new Card(2, 6)
];
console.log(hand[0].toString() + " and " + hand[1].toString());

//the dealer cheats
hand[1].value = 1;

console.log(hand[0].toString() + " and " + hand[1].toString() + " blackjack!!!");
```

The suit and value properties of Card need to be immutable (not allowed to change). 
Fixing this requires private properties are needed.  JavaScript allows us to do 
this by declaring variables inside the constructor.  The catch is that the rules 
of variable scope would prevent access to those members from our prototype methods.  
Getting a truly encapsulated object require the developer to expose suit and value
without allowing them to change.

```javascript
function EncapsulatedCard(suit, value) {
    //validate suit
    suit = parseInt(suit, 10);
    if(suit < 0 || suit > 3) throw new Error("Suit must be an integer between 0 and 3");
    
    //validate value
    value = parseInt(value, 10);
    if(value < 1 || value > 13) throw new Error("Value must be an integer between 1 and 13");
    
    //suit is private, but we can get to it using the this.getSuit() method
    this.getSuit = function(){
        return suit;
    }
    
    //value is private, but we can get to it using the this.getValue() method
    this.getValue = function() {
        return value;
    }
}
```
```javascript
EncapsulatedCard.prototype.getSuitName = function() {
    const names = ["club", "diamond", "heart", "spade"];
    
    return names[this.getSuit()];
};
```
```javascript
EncapsulatedCard.prototype.getValueDescription = function() {
    switch(this.getValue()) {
        case 1:
            return "ace";
        case 11:
            return "jack";
        case 12:
            return "queen";
        case 13:
            return "king";
        default:
            return this.getValue();
    }
};
```
```javascript
EncapsulatedCard.prototype.toString = function() {
    return `${this.getValueDescription()} of ${this.getSuitName()}s`;
};
```
```javascript
//the dealer is dealt these cards
const encapsulatedHand = [
    new EncapsulatedCard(0, 10),
    new EncapsulatedCard(2, 6)
];
console.log(encapsulatedHand[0].toString() + " and " + encapsulatedHand[1].toString());
//the dealer trys to cheat
encapsulatedHand[1].value = 1;
console.log(encapsulatedHand[0].toString() + " and " + encapsulatedHand[1].toString() + " busted!!!");
```

## Object Inheritance
Objects built on function support inherietance and polymorphism but the techniques 
are not as clean those used in class based programming languages like Java.  Three
steps are required to get it wired up correctly.

> ### Generic Hand
> This is the base object being extended.
```javascript
function Hand(cards = []) {
    if(!cards instanceof Array) throw new Error("argument zero must be an array");
    
    this.getCards = function() {
        return cards.slice();
    };
    
    this.playCard = function(card) {
        for(let i = 0; i < cards.length; i++) {
            if(card === cards[i]) {
                return cards.splice(i, 1)[0];
            }
        }
    };
    
    this.takeCard = function(card) {
        if(card instanceof Card) cards.push(card);
    }
}
```
```javascript
Hand.prototype.score = function() {
  return 0;  
};
```

> ### Call and Apply
> The JavaScript functions call and apply allow one object to inherit the properties
  and methods of another  The only difference between them is the expected parameters.
  Both expect the context(usually the keyword ```this```) to be passed as argument zero.
> - **call**: Pass additional arguments individually to the object being extended.
  This function is good when the number of arguments is fixed.
```javascript
Hand.call(this, cards);
```
> - **apply**: Pass an array of arguments to the object being exended.  This function
  is good when the number of arguments is variable.
```javascript
Hand.apply(this, Array.prototype.slice.call(arguments));
```

> ### Setting up Hierarchy
While call or apply will wire will provide access to the parent's properties and
methods it isn't enough for polymorphism to work.  For that we need to physically 
set the child's prototype and constructor to the parent.
```javascript
BlackjackHand.prototype = Object.create(Hand.prototype);
BlackjackHand.prototype.constructor = BlackjackHand;

```


> ### Blackjack Hand Inherits Hand
```javascript
function BlackjackHand(cards = []) {
    Hand.call(this, cards);//inherit Hand
    if(this.getCards().length !== 2) throw new Error("Blackjack hands must start with two cards");
}
BlackjackHand.prototype = Object.create(Hand.prototype);
BlackjackHand.prototype.constructor = BlackjackHand;
BlackjackHand.prototype.score = function() {
    const cards = this.getCards();
    
    let totalScore = 0;
    let numAces = 0;
    
    for(let i = 0; i < cards.length; i++) {
        if(cards[i].getValue() > 10) {
            totalScore += 10;
        } else if(cards[i].getValue() === 1) {
            numAces += 1;
            totalScore += 11;
        } else {
            totalScore += cards[i].getValue();
        }
    }
    
    for(let i = 0; i < numAces.length; i++) {
        if(totalScore > 21) {
            totalScore -= 10;
        }
    }
    
    return totalScore;
};
```

> ### Testing Inheritance
```javascript
//show genericHand is instanceof Hand but not BlackjackHand
console.log(genericHand instanceof Hand);
console.log(genericHand instanceof BlackjackHand);
//show blackjackHand is an instanceof Hand & BlackjackHand
console.log(blackjackHand instanceof Hand);
console.log(blackjackHand instanceof BlackjackHand);

//show genericHand has an empty score and blackjackHand scores the hand
console.log(genericHand.score());
console.log(blackjackHand.score());
```