# Homework 3
Enhance Activity 3 to support a round of betting before cards are 
printed to the console.

## Due Thursday October 18th, 2018 (due date extended for fall break)

## References
[Poker Terms and Definitions](https://www.pokerstars.com/poker/terms/?no_redirect=1)

## Requirements
1. Project Setup
    - Create a folder called homework3 in your private workspace.
    - Copy the entire contents of activity 3 into this new folder.
    - Modify the package.json file to set the name property to homework3.
2. Project Code
You may create new objects, modify Poker objects, and extend any object
for this homework.
  - ***Betting Example***: In this example each player has 100 units with which
    to bet.  The current bet starts at zero, and the betting round is over when
    all players have contributed the full current bet or have folded.  There are
    five players, and all players have contributed 5 units to the pot (ante).
    1. Player One(95): [cards] current bet 0.  Bets 10.
    2. Player Two(95): [cards] current bet 10. Folds. (removed from play)
    3. Player Three(95): [cards] current bet 10. Bets 15. (current bet + raised bet by 5)
    4. Player Four(95): [cards] current bet 15. Bets 15.
    5. Player Five(95): [cards] current bet 15. Folds.
    6. Player One(85): [cards] current bet 15. Bets 20. (must contribute an additional 10 to the pot)
    7. Player Three(80): [cards] current bet 20. Bets 20. (must contribute an additional 5 to the pot)
    8. Player Four(80): [cards] current bet 20.  Bets 20. (must contribute an additional 5 to the pot)
    9. Player One(75): [cards] current bet 20.  Bets 20. (already bet 20 so no additional money put into the pot)
    10. Player Three(75): [cards] current bet 20. Bets 20. (already bet 20 so no additional money put into the pot)
    11. Player Four(75): [cards] current bet 20. Bets 20. (already bet 20 so no additional money put into the pot)
    12. Betting is over.  All players have either folded, or had an opportunity to
        raise the bet.  All players that did not fold have contributed the bet 
        amount to the pot.

## Acceptance Criteria
- The user of PokerGame must be able to pass in how much money players start
  with, and if that option is not used the default amount should be 100.
- Each player object must be able to track how much money it has to bet with.
- There must be a pot that receives each players ante and all bets.
- At the beginning of the hand all players must ante 5 into the pot.
- Prior to outputing all the players hands and announcing a winner each player
  must have the opportunity to bet.  
  - Use the built-in function prompt("prompt text").  
  - The prompt text should tell the user what cards they have, how much money
    they have left, and the amount of the current bet.
  - The user must be able to type an amount between the current bet and the full
  - amount of money they have left.  They may also fold.
- The program should run using the webpack dev server each time the page is refreshed.
- Code should be split into ES16 modules
- The code should use PlayerOut to display each player's name, cards, and score.
- The code should use Out to display what a player bet, and the winner.
- The program should produce no errors  
   