# ES16 Classes
Classes make object oriented programming in JavaScript more familiar to developers
comming from class based languages, but do not change the prototypical nature of
the language.  A ```class``` is actually a special type of function.  Like a 
function it can be declaritive or an expression.

```javascript
//declaritive
class Player {//...}

//expression
const Player = class {//...}
```

## Constructor
An optional special method for initializing the state of an object.  This method is called 
automatically whenever a new object is instantiated.  
```javascript
class Player {
    constructor(name = "") {// constructor is optional
        console.log("Player's name is " + name);
    }
}
const player1 = new Player("Bob");
```

## Properties
Object properties are usually initialized in the constructor.  They can then be 
accessed the same way we do in function and literal objects.
```javascript
class Player {
    constructor(name = "") {
        this.name = name;
        this.cards = [];
    }

}
const player1 = new Player("Jill");
console.log(player1.name);
```

## Methods
Declare methods by name without using the function keyword.
```javascript
class Player {
    constructor(name = "") {
        this.name = name;
    }

    sayHello() {
        return "Hello, my name is " + this.name;
    }
}
const player1 = new Player("Jill");
console.log(player1.sayHello());
```

## Private Properties and Methods
In ES16 JavaScript still wants to make everything public.  Achieving encapsulation
can be done the same way it was in a function, but its not clean.  The most common
way to address this is through convension.  The properties and methods are allowed
to remain public, but are prefixed with an underscore character.  Developers using
the object should be aware of the convension and not use them directly.

> ### Encapsulation Using Constructor
```javascript
class EncapsulatedPlayer {
    constructor(name = "") {
        this.getName = () => {
            return name;  
        };
    }
}
let ePlayer = new EncapsulatedPlayer("Roger");
console.log(ePlayer.getName());
```
> ### Encapsulation by Convension
```javascript
class ConvensionPlayer {
    constructor(name = "") {
        this._name = name;
    }
    
    getName() {
        return this._name;
    }
}
let cPlayer = new ConvensionPlayer("Susan");
console.log(cPlayer._name);//still public, but wrong
console.log(cPlayer.getName());//correct
```

## Getters an Setters
These are special methods that are access like a property.  As the name implies 
they are typically used get and set properties.  They can also be useful when more
logic is needed.
```javascript
class Player {
    constructor(firstName = "", lastName = "") {
        this._firstName = firstName;
        this._lastName = lastName;
    }
    
    get firstName() {
        return this._firstName;
    }
    
    get lastName() {
        return this._lastName;
    }

    get name() {
        return this._firstName + " " + this._lastName;
    }
    
    set firstName(firstName = "") {
        this._firstName = firstName;
    }
    
    set lastName(lastName = "") {
        this._lastName = lastName;
    }
    
    set name(name = "") {
        const names = name.split(" ");
        this._firstName = names[0];
        this._lastName = (names[1] !== undefined) ? names[1] : "";
    }
}
const player1 = new Player("Jane", "Doe");
console.log(player1.name);
player1.name = "John Doe";
console.log(player1.name);
```

## Static Methods
Adding the ```static``` keyword in front of a method makes if available without
having to create an instance of an object.  The catch is they cannot be called 
through an instance.  Static methods are good for creating utility methods.
```javascript
class Player {
    
    static speak(message) {
        console.log(message);
    }
}
const player1 = new Player("Steve", "Smith");
console.log(Player.speak(`Hi my name is ${player1.name}`));
```

## Object Inheritance
Inheritance using ```class``` is easier to implementa and understand than 
```function``` object inherietance.  Just use the ```extend``` keyword when
declaring the child object.  An object may only have one parent.  If the new child
class is given a constructor the ```super``` method may be called to call the 
parent's constructor.
```javascript
class Dealer extends Player {
    constructor(firstName, lastName, deck) {
        super(firstName, lastName);//call the parent's constructor
        this._deck = deck;
    }
    
    get deck() {
        return this._deck;
    }
    
    deal(numCards = 0, numPlayers = 0) {
        let hands = [];
        
        this._deck.shuffle();
        for(let i = 0; i < numCards; i++) {
            if(! hands[i] instanceof Array) {
                hands[i] = [];
            }
            
            for(let j = 0; j < numPlayers; j++) {
                hands[i].push(this._deck.takeCard());
            }
        }
    }
}

const dealer = new Dealer("Kenny", "Rogers", new Deck());
console.log(dealer instanceof Player);
console.log(dealer instanceof Dealer);
```