# Modules
A module is a reusable piece of code that encapsulates implementation details and 
exposes a public API so it can be easily loaded and used by other code.
([A 10 minute primer to JavaScript modules, module formats, module loaders and module bundlers](https://medium.freecodecamp.org/javascript-modules-a-beginner-s-guide-783f7d7a5fcc))

More specifically a module is a single file that may contain one or more functions,
classes, and/or other variables that serve a single purpose.  Each module has its
own global scope.

## References
- [A 10 minute primer to JavaScript modules, module formats, module loaders and module bundlers](https://medium.freecodecamp.org/javascript-modules-a-beginner-s-guide-783f7d7a5fcc)
- [Authoring CommonJS modules](http://know.cujojs.com/tutorials/modules/authoring-cjs-modules)
- [MDN - export](https://developer.mozilla.org/en-US/docs/web/javascript/reference/statements/export)
- [MDN - import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)

## CommonJS (NodeJS)
This module system is used in NodeJS.  When writing applications for the browser
developers need to be conscious of how calls to the impact the performance of their
programs.  In server-side development modules load from the filesystem which is
much faster.

> ### Terminology
> - **module**: A reference to the modules global scope.  Used much the same as 
  ```this``` in an object.
>  - **exports**: Variables, objects, and classes can be exposed for use outside
    the module.
>  - **require**: Imports a module into the application.

> ### Writing CommonJS Module
```javascript src/modules/commonjs/index.js
// import the file system module for use in this module
let fs = require("fs");

//everything declared in a module is scoped to that module
let tempData = [
  {
    propertyOne:"valueOne"
  }
];

function serialize(object = {}) {
    if(typeof object !== "object") throw new Error("argument zero must be an object");
        return JSON.stringify(object);
}

function deserialize(string = "{}") {
    if(typeof string !== "string") throw new Error("argument zero must be a string");
    return JSON.parse(string);    
}

//expose a public method
module.exports.getFile = function(filepath = "", onfileRetrieved) {
    fs.readFile(filepath, "UTF-8", (err, data) => {
        if(err) {
            onfileRetrieved(err);
        } else {
            try {
                onfileRetrieved(null, deserialize(data));
            } catch(err) {
                onfileRetrieved(err);   
            }
        }
    });
};

//module is optional, because it is the global scope
exports.setFile = function(filepath = "", fileData = "", onFileSaved) {
    fs.writeFile(filepath, serialize(fileData), "UTF-8", onFileSaved);
};
```

> ### Using a CommonJS Module
```javascript src/commonjs.js
// import module
// file and extension are optional if file is named index.js if filename is not index use filename without .js
var fileModule = require("./modules/commonjs");

//use import
fileModule.getFile("cars.json", function(err, data) {
    if(!err) {
        data.push({
            car:"Ford Model T"
        });
 
        fileModule.setFile("cars.json", data, function(err) {
            if(!err) {
                console.log("operation successful"); 
            } else {
                console.log(err);
            }
        });
    } else {
        console.log(err);
    }
});
```

## ES16 Module System
Modules are a JavaScript standard, but are still quite new.  Support is 
experimental in NodeJS.  Recent versions of most browsers support modules, but
importing lots of module files is slow.  By using a bundler such as Webpack modules
can be used in a program without the overhead of having to retrieve each module 
file individually.

> ### Terminology
> - **export**: tells JavaScript this variable, class, or function should be exposed
  outside the module.
> - **default**: A variable, class, or function that has ```export default``` can
  be used in the import without refering to it by name.  Only one module property
  can be ```default```
> - **import**: tells JavaScript a module must be retrieved.
> - **from**: The filepath from the current file to the module file being imported.
>  - **as**: Renames an imported variable, class, or function

> ### Writing ES16 Module
```javascript src/modules/es_module/module1.js
// export a class
export class MyClass {}

// export a function
export function myFunction(){console.log("Hello myFunction");}

//export a variable
export let myVariable = "myVariable value";
```
```javascript src/modules/es_module/module2.js
//use default to indicate that an object, function, or variable can be imported without a name
export default class MyDefaultClass{}

export function someFunction() {}
```

> ### Using ES16 Module
```javascript src/es_module.js
//importing a named value from an object
import { MyClass } from "./modules/es_module/module1";
//importing multiple values
import { MyClass, myFunction, myVariable } from "./modules/es_module/module1";
//renaming value on import
import { MyClass as MyImportedClass, myFunction as myImportedFunction, myVariable as myImportedVariable } from "./modules/es_module/module1";
```
```javascript src/es_module.js
//importing a default value
import anyVariableName from "./modules/es_module/module2";
//importing a default and a named value at the same time
import anyVariableName, { someFunction } from "./modules/es_module/module2";
```

> ### Aggregating Modules
> Creating many small modules is easier to maintain that a few large one.  Import
  and export can be combined in an ```index.js``` file to make it easier for developers
  to use a module while keeping each module file as small as possible.
```javascript src/modules/es_module/index.js
export { * } from "./module1";
export * from "./module2";
```
```javascript src/es_module.js
import { MyClass, someFunction } from "./modules/es_module";
```