# Activity 3
Using the code in 4_example/card_game write a five card stud poker game.  

## Due Thursday October 9th, 2018 (end of class)

## References
- [Poker Hand Rankings](https://www.cardplayer.com/rules-of-poker/hand-rankings)

## Requirements
1. Project Setup
    - Create a folder called "activity3" in your private workspace.
    - Create a new NPM project
        - name: "activity3"
        - author: your name
        - devDependencies:  from 2_getting_started package.json
        - scripts: from 2_getting_started package.json
    - Setup webpack
        -  Use the webpack.config.js file from 2_getting_started
    - Create a folder called "src" 
        - create files index.html, index.css, and index.js in src
        - Copy the card_game folder with its contents from the class workspace 
        into src.
    
2. Project Code
    - ***Classes***
        - **PokerHand**: extends Hand
            - constructor: takes and validates an array of cards
            - method score: returns the name of the best hand a player can make.
            - method highCard: returns the value of the highest card in the players hand
            - method isPair: returns true if the hand contains two cards of the same value
            - method isTwoPair: returns true if the hand contains two cards of the 
            same value and two other cards with another matching value
            - method isThreeOfAKind: returns true if the hand contains three cards of the same value
            - method isStraight: returns true if all cards in the hand have values
            in sequence.
            - method isFlush: returns true if all cards are of the same suit.
            - method isFullHouse: returns true if the hand contains two cards of one value
            and three cards of another value
            - method isFourOfAKind: returns true if the hand contains four cards of the same value
            - method isStraightFlush returns true if the hand is both a straight
            and a flush.
            - method isRoyalFlush: returns true if the hand is a straight flush
            containing cards 10 through ace
        
        - **PokerGame**: contains the logic for playing a single hand of poker
            - constructor: acccepts one argument options.  Evaluates the options
            argument creating any required objects not provided.
            - method start: Play one hand of poker, and declare a winner.  This
            game does not contain any betting so prompting the user will not be
            necessary.
        
    - ***Main Program***
        - Create a new instance of PokerGame passing in three players
        - Call the PokerGame start method.

## Acceptance Criteria
- The program should run using the webpack dev server each time the page is refreshed.
- Code should be split into ES16 modules
- The code should use PlayerOut to display each player's name, cards, and score.
- The code should use Out to display the winner.
- The program should produce no errors