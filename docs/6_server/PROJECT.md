# Creating an Express Project

## Resources
[Express Installing](https://expressjs.com/en/starter/installing.html)
[Express Hello World](https://expressjs.com/en/starter/hello-world.html)

## Create Project
First create an NPM project using the techniques from section two getting started.
```bash
$ mkdir myapp
$ cd myapp
$ npm init
```

## Install Express
```bash
$ npm install express --save
```

## Create an Express Server
Create a file called 'server.js' and paste in the following code.

```javascript
//server.js
const express = require('express');
const app = express();
const port = process.env.PORT || 5000;//cloud9 port is env variable

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
```

## Starting the Server
The new server can be started from the command line as shown below or in Cloud 9.
In Cloud 9 open the server.js file and ensure it is the active tab.  Then
click the Run button from the top menu bar.  

```bash
$ node server.js
```

## Stopping the Server
Use ```cntl + c``` to stop the server from the command line.  If the server was 
started from Cloud 9's run button there will be a new tab at the bottom of the 
screen.  In that tab use the button labeled stop.