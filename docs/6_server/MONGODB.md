# Persistence

## References
* [The key differences between SQL and NoSQL DBs.](http://www.monitis.com/blog/cc-in-review-the-key-differences-between-sql-and-nosql-dbs/)
* [Set Up a Database](https://docs.c9.io/docs/setup-a-database#mongodb)
* [Mongo Shell Quick Reference](https://docs.mongodb.com/manual/reference/mongo-shell/)
* [Mongoose:  Getting Started](https://mongoosejs.com/docs/)
 
## Setup MongoDB on Cloud9
1. Install MongoDB
    1. Create a folder called data in the workspace root directory.  This is where
    the database data will be located.
    2. Run the following command to install MongoDB
    ```bash
    $ sudo apt-get install -y mongodb-org
    ```

2. Create a Start Script
    1. Create a folder called bin in the workspace root directory.
    2. Create a file called startmongo.sh in the bin folder.
    3. Copy the following into your startdb.sh file.
    ```bash
    #!/bin/bash
    
    mongod --bind_ip=$IP --dbpath=/home/ubuntu/workspace/data --nojournal --rest "$@"
    ```
    4. Make startdb.sh executable.
    ```bash
    $ chmod +x bin/startdb.sh
    ```
    5. Test the script
    ```bash
    $ /home/ubuntu/workspace/code/6_server/bin/startdb.sh
    2018-11-12T12:41:07.992+0000 ** WARNING: --rest is specified without --httpinterface,
    2018-11-12T12:41:07.992+0000 **          enabling http interface warning: bind_ip of 0.0.0.0 is unnecessary; listens on all ips by default
    2018-11-12T12:41:07.995+0000 [initandlisten] MongoDB starting : pid=16234 port=27017 dbpath=/home/ubuntu/workspace/data 64-bit host=swensonpn-fall-2018-csit-440-6312834
    2018-11-12T12:41:07.995+0000 [initandlisten] db version v2.6.12
    2018-11-12T12:41:07.995+0000 [initandlisten] git version: d73c92b1c85703828b55c2916a5dd4ad46535f6a
    2018-11-12T12:41:07.995+0000 [initandlisten] build info: Linux build5.ny.cbi.10gen.cc 2.6.32-431.3.1.el6.x86_64 #1 SMP Fri Jan 3 21:39:27 UTC 2014 x86_64 BOOST_LIB_VERSION=1_49
    2018-11-12T12:41:07.995+0000 [initandlisten] allocator: tcmalloc
    2018-11-12T12:41:07.995+0000 [initandlisten] options: { net: { bindIp: "0.0.0.0", http: { RESTInterfaceEnabled: true, enabled: true } }, storage: { dbPath: "/home/ubuntu/workspace/data", journal: { enabled: false } } }
    2018-11-12T12:41:08.003+0000 [initandlisten] waiting for connections on port 27017
    2018-11-12T12:41:08.003+0000 [websvr] admin web console waiting for connections on port 28017
    ```
    6. Shut down the database uscing ```cntrl+c``` in the console.
    ```bash
    ^C2018-11-12T12:43:16.293+0000 [signalProcessingThread] got signal 2 (Interrupt), will terminate after current cmd ends
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] now exiting
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] dbexit: 
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] shutdown: going to close listening sockets...
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] closing listening socket: 5
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] closing listening socket: 6
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] closing listening socket: 8
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] removing socket file: /tmp/mongodb-27017.sock
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] shutdown: going to flush diaglog...
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] shutdown: going to close sockets...
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] shutdown: waiting for fs preallocator...
    2018-11-12T12:43:16.293+0000 [signalProcessingThread] shutdown: closing all files...
    2018-11-12T12:43:16.294+0000 [signalProcessingThread] closeAllFiles() finished
    2018-11-12T12:43:16.294+0000 [signalProcessingThread] shutdown: removing fs lock...
    2018-11-12T12:43:16.294+0000 [signalProcessingThread] dbexit: really exiting now
    ```
    7. Make an easy to remember alias for the start command.
    ```bash
    $ alias startdb="/home/ubuntu/workspace/code/6_server/bin/startdb.sh"
    ```
    8. Test the alias
    ```bash
    $ startdb
    2018-11-12T12:48:35.474+0000 ** WARNING: --rest is specified without --httpinterface,
    2018-11-12T12:48:35.474+0000 **          enabling http interface warning: bind_ip of 0.0.0.0 is unnecessary; listens on all ips by default
    2018-11-12T12:48:35.477+0000 [initandlisten] MongoDB starting : pid=16308 port=27017 dbpath=/home/ubuntu/workspace/data 64-bit host=swensonpn-fall-2018-csit-440-6312834
    2018-11-12T12:48:35.477+0000 [initandlisten] db version v2.6.12
    2018-11-12T12:48:35.477+0000 [initandlisten] git version: d73c92b1c85703828b55c2916a5dd4ad46535f6a
    2018-11-12T12:48:35.477+0000 [initandlisten] build info: Linux build5.ny.cbi.10gen.cc 2.6.32-431.3.1.el6.x86_64 #1 SMP Fri Jan 3 21:39:27 UTC 2014 x86_64 BOOST_LIB_VERSION=1_49
    2018-11-12T12:48:35.477+0000 [initandlisten] allocator: tcmalloc
    2018-11-12T12:48:35.477+0000 [initandlisten] options: { net: { bindIp: "0.0.0.0", http: { RESTInterfaceEnabled: true, enabled: true } }, storage: { dbPath: "/home/ubuntu/workspace/data", journal: { enabled: false } } }
    2018-11-12T12:48:35.486+0000 [initandlisten] waiting for connections on port 27017
    2018-11-12T12:48:35.486+0000 [websvr] admin web console waiting for connections on port 28017
    ```

## MongoDB Command Line
Being able to inspect the data saved in MongoDB is an important part of debugging
an application.  Learning a few simple commands will make the next part of this
section much easier to understand.

### Start/Stop the Mongo Shell
1. From the top menu select Window > New Terminal
2. Type the command "mongo" to start a session
```bash
$ mongo
MongoDB shell version: 2.6.12
connecting to: test
Server has startup warnings: 
2018-11-12T12:55:31.669+0000 ** WARNING: --rest is specified without --httpinterface,
2018-11-12T12:55:31.669+0000 **          enabling http interface
> _
```
3. Use ```cntrl+c``` to end the session
```bash
> ^C
bye
$ 
```

### Selecting a Database
1. Get a list of all databases
```shell
> show dbs
admin  (empty)
local  0.078GB
```
2. Select the database to use
```shell
> use local
switched to db local
```

### Listing Collections
1. Get a list of all collections
```shell
> show collections
startup_log
system.indexes
```

## Query a Collection
1. List all documents in a collection
```shell
> db.startup_log.find({})
{ "_id" : "swensonpn-fall-2018-csit-440-6312834-1542027331680", "hostname" : "swensonpn-fall-2018-csit-440-6312834", "startTime" : ISODate("2018-11-12T12:55:31Z"), "startTimeLocal" : "Mon Nov 12 12:55:31.680", "cmdLine" : { "net" : { "bindIp" : "0.0.0.0", "http" : { "RESTInterfaceEnabled" : true, "enabled" : true } }, "storage" : { "dbPath" : "/home/ubuntu/workspace/data", "journal" : { "enabled" : false } } }, "pid" : NumberLong(16369), "buildinfo" : { "version" : "2.6.12", "gitVersion" : "d73c92b1c85703828b55c2916a5dd4ad46535f6a", "OpenSSLVersion" : "", "sysInfo" : "Linux build5.ny.cbi.10gen.cc 2.6.32-431.3.1.el6.x86_64 #1 SMP Fri Jan 3 21:39:27 UTC 2014 x86_64 BOOST_LIB_VERSION=1_49", "loaderFlags" : "-fPIC -pthread -Wl,-z,now -rdynamic", "compilerFlags" : "-Wnon-virtual-dtor -Woverloaded-virtual -fPIC -fno-strict-aliasing -ggdb -pthread -Wall -Wsign-compare -Wno-unknown-pragmas -Winvalid-pch -pipe -Werror -O3 -Wno-unused-function -Wno-deprecated-declarations -fno-builtin-memcmp", "allocator" : "tcmalloc", "versionArray" : [ 2, 6, 12, 0 ], "javascriptEngine" : "V8", "bits" : 64, "debug" : false, "maxBsonObjectSize" : 16777216 } }
```
2. Find documents that match a criteria
```shell
> db.startup_log.find({hostname:"swensonpn-fall-2018-csit-440-6312834"})
{ "_id" : "swensonpn-fall-2018-csit-440-6312834-1542027331680", "hostname" : "swensonpn-fall-2018-csit-440-6312834", "startTime" : ISODate("2018-11-12T12:55:31Z"), "startTimeLocal" : "Mon Nov 12 12:55:31.680", "cmdLine" : { "net" : { "bindIp" : "0.0.0.0", "http" : { "RESTInterfaceEnabled" : true, "enabled" : true } }, "storage" : { "dbPath" : "/home/ubuntu/workspace/data", "journal" : { "enabled" : false } } }, "pid" : NumberLong(16369), "buildinfo" : { "version" : "2.6.12", "gitVersion" : "d73c92b1c85703828b55c2916a5dd4ad46535f6a", "OpenSSLVersion" : "", "sysInfo" : "Linux build5.ny.cbi.10gen.cc 2.6.32-431.3.1.el6.x86_64 #1 SMP Fri Jan 3 21:39:27 UTC 2014 x86_64 BOOST_LIB_VERSION=1_49", "loaderFlags" : "-fPIC -pthread -Wl,-z,now -rdynamic", "compilerFlags" : "-Wnon-virtual-dtor -Woverloaded-virtual -fPIC -fno-strict-aliasing -ggdb -pthread -Wall -Wsign-compare -Wno-unknown-pragmas -Winvalid-pch -pipe -Werror -O3 -Wno-unused-function -Wno-deprecated-declarations -fno-builtin-memcmp", "allocator" : "tcmalloc", "versionArray" : [ 2, 6, 12, 0 ], "javascriptEngine" : "V8", "bits" : 64, "debug" : false, "maxBsonObjectSize" : 16777216 } }
```

## Remove Documents
When building the schema for a new application its not unusual to end up with several
document structures that don't match causing errors during testing.  To clear all
documents from a collection use the following.  In this case Mongo will not let the
user remove data from the system startup_log collection.
1. Remove all documents from a collection
```shell
> db.startup_log.remove({})
WriteResult({
        "nRemoved" : 0,
        "writeError" : {
                "code" : 10101,
                "errmsg" : "cannot remove from a capped collection: local.startup_log"
        }
})
```