# HTTP Protocol
Hypertext transfer protocol (HTTP) defines the rules for communication across the
internet.  For this course it used for communication between the browser and the
server, but it also used for communications between distrubuted systems.

**Understanding the HTTP is a crucial skill for any web developer.**

## Resources
* [HTTP: The Protocol Every Web Developer Must Know](https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-1--net-31177)
* [Postman](https://www.getpostman.com/)

## Stateless
HTTP is a stateless.  This means each request response cycle is unique.  The 
browser and server do not retain information from request to request.  As a result
developers must use other means if their application requires synchronisation
between the browser and server.

A common example of this need is maintaining a login.  If an application relied 
only on HTTP for security the user would have to pass their credentials with every 
request.

## HTTP Messages
Messages sent between the browser and server are text based.  Each message consists
of a header and a body separated by a blank line.  The header contains meta data
about the request and the body contains the data itself.

> Request Message
```
GET /index.html HTTP/1.1
Host: www.example.com
```

> Response Message
```
HTTP/1.1 200 OK
Date: Mon, 23 May 2005 22:38:34 GMT
Content-Type: text/html; charset=UTF-8
Content-Length: 138
Last-Modified: Wed, 08 Jan 2003 23:11:55 GMT
Server: Apache/1.3.3.7 (Unix) (Red-Hat/Linux)
ETag: "3f80f-1b6-3e1cb03b"
Accept-Ranges: bytes
Connection: close
<br/>
<br/>
&lt;html&gt;
&lt;head>
  &lt;title&gt;An Example Page&lt;/title&gt;
&lt;/head&gt;
&lt;body>
  Hello World, this is a very simple HTML document.
&lt;/body&gt;
&lt;/html&gt;
```

## URLs
This image from [HTTP: The Protocol Every Web Developer Must Know](https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-1--net-31177)
does a great job illustrating the parts of a typical url.
![alt url parts](https://cdn.tutsplus.com/net/authors/jeremymcpeak/http1-url-structure.png)

* **Protocol**: http or https.  Both are the http protocol, but https is used for
  secure communications.
* **Host**: The domain name or ip address of the server.
* **Port**: When using port 80 (typical) this is omitted.
* **Resource Path**: This is used to locate the requested resource on the server.
  When serving static content this is usually the directory path to the desired file,
  but it can also be a value mapped to a function/method that should be run.
* **Query**: Parameters passed to the server process for dynamic content.  The
  query string is sent as name value pairs separated by an ampersand.   
  
## HTTP Verbs  
Verbs are used to tell the server what type of request the browser is making.  
* **GET**: Fetch an existing resource.
* **POST**: Create a new resource.
* **PUT**: Update an existing resource.
* **DELETE**: Delete an existing resource.
* **HEAD**: Fetch the headers for an existing resource. Used to determine
  if a locally cached file should be replaced.
* **TRACE**: Used for debugging a request.
* **OPTIONS**: Used to get server support settings such as if the server allows
  cross origin requests.
  
## Status Codes
Status codes tell the browser whether the request succeded or what to do next.
* **2xx**: Success Messages
    * **200**: OK
* **3xx**: Redirection
    * **301**: Moved Permanently - the browser should make a new request to the 
      indicated url.
* **4xx**: Client Error
    * **401**: Unauthorized
    * **404**: Not Found
* **5xx**: Server Error
    * **500**: Internal Server Error
    
## Postman 
Until now browser based developer tools have been used to debug web applications.
These tools are great for inspecting synchronous requests from the browser.  As 
web applications become more complex other tools can make testing easier.  One such
tool is Postman an application for developing and testing 
[restful web services](https://en.wikipedia.org/wiki/Representational_state_transfer).

