# Mongoose
Mongoose is a data modeling tool built on top of the Mongo database driver.  Tools
like Mongoose are called Object Data Model(ODM) or Object Relational Model(ORM).
These tools represent the data as objects which are then mapped to the underlying
database.  ODM/ORM frameworks are typically a little slower than interacting with 
the database directly, but can provide a lower overall cost for development and 
maintenance due to there ease of use and declaritive nature.

## References 
* [Mongoose](https://mongoosejs.com/)


## Install
```bash
$ npm install mongoose --save
```

## Connecting to the Database
```javascript
//server.js
const mongoose = require('mongoose');

const port = process.env.PORT || 5000;
const connectString = "mongodb://localhost:27017/test";

//connect to the database on server start
mongoose
  .connect(connectString, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch((err) => {console.log('Mongo Error:'); console.log(err);});
  
//disconnect from the database on server stop
process.on('SIGINT',function(){
  mongoose.connection.close(function(){
    console.log('MongoDB disconnected due to Application Exit');
    process.exit(0);
  });
});
```

## Creating a Model
The model for a database collection is typically stored in a JavaScript module.

```javascript
//model/card.js

const mongoose = require("mongoose"),
      Schema = mongoose.Schema;
 
//create a schema      
const cardSchema = new Schema({
  type: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  due: {
    type: Date,
    required: true
  },
  complete: {
    type: Date
  }
},
{
  collection: "cards"
});

//compile schema into a model
module.exports = mongoose.model('Card', cardSchema);
```

## Using a Model
Once the model is created it can be used in a controller.

### Import the Model
```javascript
//routes/cards.js

const CardModel = require("../models/card");
```

### Get All Documents
```javascript
//routes/cards.js

CardModel
        .find({complete:null})
        .exec(function(err, cards) {
             if (err) {
                 console.error(err);
                 response.writeHead(500);
                 response.end("error");
            } else {
                response.send(JSON.stringify(cards));
             }
         });
```         

### Get One Document
```javascript
//routes/cards.js

CardModel.findById(id, function(err, card){
    if(err) {
        console.error(err);
        response.writeHead(404);
        response.end("not found");
    } else {
        response.send(JSON.stringify(card));
    }
});
```

### Create a New Document
```javascript
//routes/cards.js

CardModel.create({
      type: request.body.type,
      description: request.body.description,
      due: request.body.due
    }, function(err, card) {
        if(err) {
            console.error(err);
            response.writeHead(500);
            response.end("error");
        } else {
            response.writeHead(201);
            response.end(JSON.stringify(card));
        }
    });
```

### Update an Existing Document
```javascript
//routes/cards.js

//1. get card by id
CardModel.findById(id, function(err, card){
    if(err) {
        console.error(err);
        response.writeHead(404);
        response.end("not found");
    } else {
        //2. Update properties from the request
        card.type = request.body.type;
        card.description = request.body.description;
        card.due = request.body.due;
        if(request.body.type === "complete") {
            card.complete = new Date();
        }
        
        //3. Save the updated card
        card.save(function() {
            response.writeHead(200);
            response.end(JSON.stringify(card));
        });
    }
});
```

### Delete an Existing Document
```javascript
//routes/cards.js

CardModel.remove({_id:id},function(err) {
        if(err) {
            console.error(err);
            response.writeHead(500);
            response.end("error");
        } else {
            response.writeHead(202);
            response.end(JSON.stringify(card));
        }
    });
```