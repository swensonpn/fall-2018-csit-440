# Model View Controller Pattern (MVC)
Model–view–controller is an architectural pattern commonly used for developing user interfaces that divides an application into three interconnected parts. This is done to separate internal representations of information from the ways information is presented to and accepted from the user. The MVC design pattern decouples these major components allowing for efficient code reuse and parallel development.

Traditionally used for desktop graphical user interfaces (GUIs), this architecture has become popular for designing web applications and even mobile, desktop and other clients. Popular programming languages like Java, C#, Ruby, PHP have MVC frameworks that are used in web application development straight out of the box. [wikipedia](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)

# References
* [Model–view–controller](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)

## MVC in Express

![alt model view controller diagram](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/MVC-Process.svg/500px-MVC-Process.svg.png)

In the diagram above a single http request is broken up into three parts.  

## Controller
The process begins with a request from the user to a path on the server.  The server
will route this request to a function based on a path and method configured by the 
developer. 
```javascript
//code/7_project/server.js

//import module
var cardRoutes = require("./routes/cards");

/* code removed for brevity */


// Assign api routes
app.get("/api", cardRoutes.index);
app.post("/api", cardRoutes.save);
app.delete("/api", cardRoutes.delete);
```

Each of the three functions above is a controller and is resposible for the application
flow.  In this case there are three controller functions all mapped to one 
path (/api), but with different http verbs.  Notice that the verb used describes
the type of action that will be performed.


```javascript 
//code/7_project/routes/cards.js

//import module
const CardModel = require("../models/card");

//controller function for GET request
exports.index = function(request, response) {               
    CardModel
        .find({complete:null})
        .exec(function(err, cards) {
             if (err) {
                 console.error(err);
                 response.writeHead(500);
                 response.end("error");
            } else {
                response.send(cards);
             }
         });
};

//controller function for POST request
exports.save = function(request, response) {                
    const id = request.body._id;
    
    if(id) {
        CardModel.findById(id, function(err, card){
            if(err) {
                console.error(err);
                response.writeHead(404);
                response.end("not found");
            } else {
                card.type = request.body.type;
                card.description = request.body.description;
                card.due = request.body.due;
                if(request.body.type === "complete") {
                    card.complete = new Date();
                }
                card.save(function() {
                    response.writeHead(200);
                    response.end(JSON.stringify(card));
                });
            }
        });
    } else {
        CardModel.create({
          type: request.body.type,
          description: request.body.description,
          due: request.body.due
        }, function(err, card) {
            if(err) {
                console.error(err);
                response.writeHead(500);
                response.end("error");
            } else {
                response.writeHead(201);
                response.end(JSON.stringify(card));
            }
        });
    }
};    

//controller function for DELETE request
exports.delete = function(request, response) {                          
    const id = request.body._id;
    
    if(id) {
        CardModel.findById(id, function(err, card){
            if(err) {
                response.writeHead(404);
                response.end("not found");
            } else {
                CardModel.remove({_id:id},function(err) {
                    if(err) {
                        console.error(err);
                        response.writeHead(500);
                        response.end("error");
                    } else {
                        response.writeHead(202);
                        response.end(JSON.stringify(card));
                    }
                });
            }
        });
    } else {
        response.writeHead(404);
        response.end("not found");
    }
};
```

## Model
In the code above notice the import of ```CardModel```.  This module is the model.
In MVC models are representations of the data itself, and would often be the how
the application interacts with the database.  The model is resposible for the data
rules.  Things like what data type is the field and is that field required.

```javascript
//code/7_project/models/card.js

//import modules
const mongoose = require("mongoose"),
      Schema = mongoose.Schema;
      
//the schema sets the rules for a valid card.      
const cardSchema = new Schema({
  type: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  due: {
    type: Date,
    required: true
  },
  complete: {
    type: Date
  }
},
{
  collection: "cards"
});

module.exports = mongoose.model('Card', cardSchema);
```
In the case of this Express application the model above is using a tool 
called Mongoose to enforce the validation of the data and provide a connection 
to the database.  

## View
In a traditional MVC application whether it be built on Express or some other 
platform the view is usually some server-side templating engine that converts
the model into an HTML web page.  That web page is then what the user sees.
In the case of the example project the view is a React application.  The controller
simply converts data retrieved from the model into a JSON string and returns that
to the browser where React updates what the user sees.

```javascript
//code/7_project/client/src/App.js

componentWillMount() {  
    //call to service
    getTasks(date, (err, json) => {//callback function for ajax complete
      if(err) {
        this.setState({
          tasks: groupTasks([])
        });
        console.log("using local data");
      } else {
        //update the page
        this.setState({
          tasks: groupTasks(json)
        });
      }
    });
  }
```

## Adding Controller Methods to 6_server

### New Route Handling Module
Controller methods are typically stored outside of the server.js file.  Doing this 
makes the application easier to scale and maintain.

```javascript
//routes/cards.js

const cards = [//an array can stand in for the database for now
    //card objects
];

exports.index = function(request, response) {                                   
    console.log("index");
    response.send(JSON.stringify(cards));
};

exports.save = function(request, response) {                                    
    console.log("save")
    const id = request.body.id;
    
    let card = cards.find(function(card) {
        return card.id === id;
    })
    
    if(card) {
        card.type = request.body.type;
        card.description = request.body.description;
        card.due = request.body.due;
        if(card.type !== "complete" && request.body.type === "complete") {
            card.complete = new Date();
        } 
        response.writeHead(200);
        response.end(JSON.stringify(card));
    } else {
        card = {
          type: request.body.type,
          description: request.body.description,
          due: request.body.due
        };
        
        cards.push(card);
        response.writeHead(201);
        response.end(JSON.stringify(card));
    }
};    

exports.delete = function(request, response) {                                  
    console.log("delete");
    const id = request.body.id;
    
    for(let i = 0; i < cards.length; i++) {
        if(cards[i].id === id) {
            let card = cards.slice(i,1)[0];
            response.writeHead(202);
            response.end(JSON.stringify(card));
            return;
        }
    }
    
    response.writeHead(404);
    response.end("not found");
};
```


### Update Server to Use New Module
```javascript

const path = require('path'),
      bodyParser = require('body-parser'),
      cardRoutes = require("./routes/cards");
      
      
// Parse json request body
app.use(bodyParser.json())

// Serve any static files from react
app.use(express.static(path.join(__dirname, 'client/build')));

// Assign api routes
app.get("/api", cardRoutes.index);
app.post("/api", cardRoutes.save);
app.delete("/api", cardRoutes.delete);

// Assign client app routes (everything else)
app.get('*', function(req, res) {
 res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});      
```      