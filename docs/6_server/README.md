# Serving Web Content
Web applications rely on a request response cycle.  The user requests a resource
by typing a url in the address bar or clicking on a link.  The server receives
that request and responds by passing a web page back to the user.  The messages
passing between the browser and server are sent using the http protocol.

## Resources
* [Express](https://expressjs.com/)
* [HTTP: The Protocol Every Web Developer Must Know](https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-1--net-31177)

## Notes
* [HTTP Protocol](./HTTP.md)
* [Creating an Express Project](./PROJECT.md)
* [Model View Controller Pattern](./MVC.md)
* [MongoDB](./MONGODB.md)
* [Mongoose](./MONGOOSE.md)
* [Activity 5](ACTIVITY5.md)