# Activity 5
Create a React application that displays information about cars as an HTML table.

## Due Tuesday November 20thth 2018 end of class

## Requirements
1. Project Setup
    - Using the notes from this section create an Express project in a folder called
      activity5.
    - In the new project create a folder called client.
    - Copy the contents of homework 4 into the new client folder.  
      

2. Project Code
    - **Cars Controller Module**
        - Create a folder called routes in the activity5 folder.  
        - In the new folder create a file called cars.js. 
        - Copy the cars array created for activity 4 into the top of the cars.js file.
        - Export one function called index that writes the entire contents of the cars array.
          as a JSON string to the browser. 
    - **Web Server**
        - Create a file called server.js in the activity5 folder.
        - Using the materiel in docs/6_server and code/6_server as a guide create
        - a node express web server with two routes.
            - **GET /cars**: Calls the index function from cars.js
            - **ALL &ast;**: Passes all other requests to client/build
    - **client/build/src/service/car_service.js**
        * Change the url that is passed to the ajax function to ```/cars```.
        * Build the project.
        ```bash
        $ cd /activity5/client
        $ npm run build
        ```
## Acceptance Criteria
* Server starts without errors
* A request to the server with path ```/``` returns the homework 4 index.html
  file. The main.js and w3.css files should also load.
* An ajax call is automatically made to ```/cars```, which returns a JSON string
  of car objects from routes/cars.index.
* The client/buid React application (homework 4) updates the display to show the
  cars in the table.