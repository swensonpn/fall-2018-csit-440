# Project
Create an original web application.  The application will consist of a client 
application built using react, and a server application using Node Express. You
may choose any topic for the application.  This would include a variation on the
homeworks or example projects, provided the resulting application is substantially
of the student's own design. 

## Due Thursday December 13th, 2018

## Requirements
1. Project Setup
    - ***Client***:
        - Provide package.json with the following requirements
            -  name
            -  author
            -  all dependencies & devDependencies
            -  scripts for building the bundle and running the dev server
        - webpack must be used for bunbling the project and running the web server,
          but it is acceptable to use the default configuration provided by React
          instead of including in the project.
    - ***Server***
        - Provide package.json with the following requirements
            -  name
            -  author
            -  all dependencies
        - Client can be packaged inside the server as done in the project example
          or provided as a separate project folder.  If they are split the client
          build must be installed on the server.
2. Client (React)
    - Create a react application that reads and writes data to the server using
      the ```fetch``` api.
    - The user interface must make use of the w3.css styling framework.
    - There must be at leat two implementations of user interactions with the page.
      Most applications will need more.  
3. Server (Express)
    - Create a web server that exposes a url for the client application and static
      resources.  
    - Expose a minimum of two rest web service endpoints.  At least one of the endpoints
      must use the POST method.
    - Create at least one mongoose data model.  The program must read and write
      data into the Mongo database using this model.

## Acceptance Criteria
- All source code must be provided
- Every dependency must be imported using Node Package Manager.  Do not include
  source code from outside sources in your project.
- Final application must be runnable in two steps:
    - Start MongoDB 
    - Start Node Express web server.